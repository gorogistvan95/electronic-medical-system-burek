import * as dotenv from "dotenv";
import { SnakeNamingStrategy } from "./src/snake-naming.strategy";
import "./src/boilerplate.polyfill";

if (!(<any>module).hot) {
    process.env.NODE_ENV = process.env.NODE_ENV || "development";
}

dotenv.config({
    path: `.${process.env.NODE_ENV}.env`,
});

for (const envName of Object.keys(process.env)) {
    process.env[envName] = process.env[envName].replace(/\\n/g, "\n");
}

module.exports = {
    type: process.env.DB_ENGINE,
    host: process.env.DB_HOST,
    port: +process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    namingStrategy: new SnakeNamingStrategy(),
    entities: ["src/modules/**/*.entity{.ts,.js}"],
    migrations: ["src/migrations/*{.ts,.js}"],
};
