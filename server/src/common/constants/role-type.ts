"use strict";

export enum RoleType {
    USER = "USER",
    RECEPTIONIST = "RECEPTIONIST",
    ADMIN = "ADMIN",
}
