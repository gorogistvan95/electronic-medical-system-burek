import {
    EntitySubscriberInterface,
    EventSubscriber,
    InsertEvent,
    UpdateEvent,
} from "typeorm";

import { UtilsService } from "../../providers/utils.service";
import { AuthEntity } from "../../modules/auth/auth.entity";

@EventSubscriber()
export class UserSubscriber implements EntitySubscriberInterface<AuthEntity> {
    listenTo() {
        return AuthEntity;
    }
    beforeInsert(event: InsertEvent<AuthEntity>) {
        if (event.entity.password) {
            event.entity.password = UtilsService.generateHash(
                event.entity.password
            );
        }
    }
    beforeUpdate(event: UpdateEvent<AuthEntity>) {
        if (event.entity.password !== event.databaseEntity.password) {
            event.entity.password = UtilsService.generateHash(
                event.entity.password
            );
        }
    }
}
