import { Repository } from "typeorm";
import { EntityRepository } from "typeorm/decorator/EntityRepository";
import { TimeTableEntity } from "./timetable.entity";

@EntityRepository(TimeTableEntity)
export class TimeTableRepository extends Repository<TimeTableEntity> { }
