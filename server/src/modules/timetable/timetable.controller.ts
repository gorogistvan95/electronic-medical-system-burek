import {
    Body,
    Controller,
    HttpCode,
    HttpStatus,
    Post,
    UseGuards,
    UseInterceptors,
    HttpException,
    Get,
    Param,
    Put,
    Delete,
    Query,
    ForbiddenException,
} from "@nestjs/common";
import {
    ApiTags,
} from "@nestjs/swagger";

import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "../../modules/auth/auth.service";
import { TimeTableService } from "./timetable.service";
import { CreateTimeTableReqDto } from "./dto/CreateTimeTableReqDto";
import { CreateTimeTableResDto } from "./dto/CreateTimeTableResDto";
import { UpdateTimeTableReqDto } from "./dto/UpdateTimeTableReqDto";
import { ListTimeTableResDto } from "./dto/ListTimeTableResDto";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/timetable")
@ApiTags("api/timetable")
export class TimeTableController {
    constructor(
        public readonly timeTableService: TimeTableService,
        public readonly authService: AuthService
    ) {}

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Post("create-timetable")
    @HttpCode(HttpStatus.OK)
    async createAppointment(
        @Body() createTimeTableReqDto: CreateTimeTableReqDto
    ): Promise<CreateTimeTableResDto> {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const appointment: CreateTimeTableResDto = await this.timeTableService.createAppointment(
                createTimeTableReqDto
            );

            return appointment;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete(":id/delete-appointment")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteAppointment(@Param("id") appointmentId) {
        if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        await this.timeTableService.deleteAppointment(appointmentId);
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("list-appointments-by-patient-id/:patientId")
    @HttpCode(HttpStatus.OK)
    async getAppointmentsByPatientId(
        @Param("patientId") patientId,
        @Query("startDate") startDate: string,
        @Query("endDate") endDate: string
    ): Promise<ListTimeTableResDto[]> {
        if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        const listTimeTableResDto: ListTimeTableResDto[] = await this.timeTableService.getAppointmentsByPatientId(
            patientId
        );
        return listTimeTableResDto;
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("list-appointments-by-doctor-id/:doctorId")
    @HttpCode(HttpStatus.OK)
    async getAppointmentsByDoctorId(
        @Param("doctorId") doctorId: string,
        @Query("startDate") startDate: string,
        @Query("endDate") endDate: string
    ) {
        if (
            AuthService.getAuthUser().role != RoleType.RECEPTIONIST &&
            AuthService.getAuthUser().role != RoleType.USER
        ) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        return await this.timeTableService.getAppointmentsByDoctorId(
            doctorId,
            startDate,
            endDate
        );
    }

    @UseInterceptors(AuthUserInterceptor)
    @Put(":id/update-timetable")
    @UseGuards(AuthGuard("jwt"))
    @HttpCode(HttpStatus.NO_CONTENT)
    async updateTimeTable(
        @Param("id") timeTable,
        @Body() updateTimeTableReqDto: UpdateTimeTableReqDto
    ) {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            await this.timeTableService.updateAppointment(
                timeTable,
                updateTimeTableReqDto
            );
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
}
