import {
    BadRequestException,
    HttpException,
    HttpStatus,
    Injectable,
    NotFoundException,
} from "@nestjs/common";
import { AuthService } from "../../modules/auth/auth.service";
import { ReferralEntity } from "../../modules/referral/referral.entity";
import { ReferralService } from "../../modules/referral/referral.service";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { TimeTableRepository } from "./timetable.repository";
import { CreateTimeTableReqDto } from "./dto/CreateTimeTableReqDto";
import { TimeTableEntity } from "./timetable.entity";
import { CreateTimeTableResDto } from "./dto/CreateTimeTableResDto";
import { UpdateTimeTableReqDto } from "./dto/UpdateTimeTableReqDto";
import { createQueryBuilder, FindConditions, MoreThan } from "typeorm";
import { PatientEntity } from "../../modules/patient/patient.entity";
import { PatientService } from "../../modules/patient/patient.service";
import { ReferralRepository } from "../../modules/referral/referral.repository";
import { ListTimeTableByPatientResDto } from "./dto/ListTimeTableByPatientResDto";
import { ExaminationEntity } from "../../modules/examination/examination.entity";
import { ExaminationService } from "../../modules/examination/examination.service";
import { ClinicClassEntity } from "../../modules/clinicclass/clinicclass.entity";
import { ClinicClassService } from "../../modules/clinicclass/clinicclass.service";
import { ListTimeTableByDoctorResDto } from "./dto/ListTimeTableByDoctorResDto";

@Injectable()
export class TimeTableService {
    constructor(
        public readonly referralService: ReferralService,
        public readonly authService: AuthService,
        public readonly patientService: PatientService,
        public readonly referralRepository: ReferralRepository,
        public readonly timeTableRepository: TimeTableRepository,
        public readonly examinationService: ExaminationService,
        public readonly clinicClassService: ClinicClassService
    ) {}

    async createAppointment(
        createTimeTableReqDto: CreateTimeTableReqDto
    ): Promise<CreateTimeTableResDto> {
        //Check EXIST USER
        const doctorAuthEntity: AuthEntity = await this.authService.findOne({
            id: createTimeTableReqDto.authId,
        });

        await this.referralService.proba();
        const referralEntity: ReferralEntity = await this.referralService.findOne(
            { id: createTimeTableReqDto.referralId }
        );
        await this.timeValidator(
            createTimeTableReqDto.startOfSession,
            createTimeTableReqDto.endOfSession
        );
        if (
            !(await this.uniqAppointmentValidator(
                doctorAuthEntity.id,
                createTimeTableReqDto.startOfSession,
                createTimeTableReqDto.endOfSession
            ))
        ) {
            throw new BadRequestException("Ütközik az időpont");
        }
        if (!doctorAuthEntity && !referralEntity) {
            throw new HttpException(
                "doctor or referral ID doesnt exist",
                HttpStatus.BAD_REQUEST
            );
        }

        const appointment = await this.timeTableRepository.create({
            startOfSession: createTimeTableReqDto.startOfSession,
            endOfSession: createTimeTableReqDto.endOfSession,
            doctor: doctorAuthEntity,
            referral: referralEntity,
        });
        const appointmentEntity: TimeTableEntity = await this.timeTableRepository.save(
            appointment
        );
        return new CreateTimeTableResDto(appointmentEntity);
    }

    async updateAppointment(
        appointmentId: string,
        updateTimeTableReqDto: UpdateTimeTableReqDto
    ) {
        await this.timeValidator(
            updateTimeTableReqDto.startOfSession,
            updateTimeTableReqDto.endOfSession
        );
        const appointment: TimeTableEntity = await this.findOne({
            id: appointmentId,
        });
        if (!appointment) {
            throw new NotFoundException(
                `Appointment : ${appointmentId} not found`
            );
        }
        const updateAppointment: TimeTableEntity = await this.timeTableRepository.create(
            updateTimeTableReqDto
        );
        const updatedEntity = await this.timeTableRepository.save({
            ...appointment,
            ...updateAppointment,
        });
    }
    async timeValidator(startOsSession: Date, endOfSession: Date) {
        if (startOsSession >= endOfSession) {
            throw new BadRequestException(
                "start session smaller or equal than endofsession"
            );
        }
    }

    async findOne(
        findData: FindConditions<TimeTableEntity>
    ): Promise<TimeTableEntity> {
        return this.timeTableRepository.findOne(findData);
    }

    async deleteAppointment(appointmentId: string) {
        const appointment: TimeTableEntity = await this.findOne({
            id: appointmentId,
        });
        if (!appointment) {
            throw new NotFoundException(
                `Appointment ${appointmentId} not found`
            );
        }

        await this.timeTableRepository.remove(appointment);
    }

    async listAppointmentByDoctor(
        doctorAuthId: string
    ): Promise<ListTimeTableByDoctorResDto[]> {
        const doctorAuthEntity: AuthEntity = await this.authService.findOne({
            id: doctorAuthId,
        });
        const appointmentEntity: TimeTableEntity[] = await this.timeTableRepository.find(
            { doctor: doctorAuthEntity }
        );
        const listTimeTableResDto: ListTimeTableByDoctorResDto[] = await Promise.all(
            appointmentEntity.map(
                async (appointmentEntity: TimeTableEntity) => {
                    const referralEntity: ReferralEntity = await this.referralService.findOne(
                        { id: appointmentEntity.referralId }
                    );
                    const examinationEntity: ExaminationEntity = await this.examinationService.findOne(
                        { id: referralEntity.examinationId }
                    );
                    const patientEntity: PatientEntity = await this.patientService.findOne(
                        { id: referralEntity.patientId }
                    );
                    return new ListTimeTableByDoctorResDto(
                        appointmentEntity,
                        patientEntity.name,
                        examinationEntity.name
                    );
                }
            )
        );
        return listTimeTableResDto;
    }

    async getAppointmentsByPatientId(
        patientId: string,
        startDate?: string,
        endDate?: string
    ) {
        try {
            const patientEntity: PatientEntity = await this.patientService.findOne(
                {
                    id: patientId,
                }
            );

            if (patientEntity) {
                const referralEntities: ReferralEntity[] = await this.referralRepository.find(
                    {
                        patient: patientEntity,
                    }
                );
                let resultArray = [];
                for (const referral of referralEntities) {
                    const timeTableEntity: TimeTableEntity[] = await this.timeTableRepository
                        .createQueryBuilder("appointment")
                        .where("referral_id = :referralId", {
                            referralId: referral.id,
                        })
                        .getMany();

                    const examinationEntity: ExaminationEntity = await this.examinationService.findOne(
                        { id: referral.examinationId }
                    );

                    if (!examinationEntity) {
                        throw new NotFoundException("examination doesnt exist");
                    }
                    const clinicClassEntity: ClinicClassEntity = await this.clinicClassService.findOne(
                        { id: examinationEntity.clinicClassId }
                    );

                    await timeTableEntity.map((timeTableEntity) => {
                        resultArray.push(
                            new ListTimeTableByPatientResDto(
                                timeTableEntity,
                                patientEntity.name,
                                examinationEntity.name,
                                clinicClassEntity.name
                            )
                        );
                    });
                }
                return resultArray;
            }
        } catch (e) {
            return [];
        }
    }
    async uniqAppointmentValidator(
        doctorId: string,
        startDates: Date,
        endDates: Date
    ): Promise<Boolean> {
        try {
            let newHours = new Date(startDates).getHours();
            let startDate = new Date(new Date(startDates).setHours(newHours));
            let endDate = new Date(
                new Date(endDates).setHours(new Date(endDates).getHours())
            );
            const appointmentEntity: TimeTableEntity[] = await this.timeTableRepository
                .createQueryBuilder("appointment")
                .where(
                    `(doctor_id = :doctor) AND ((:startDate <= appointment.startOfSession AND
                    :endDate >= appointment.startOfSession ) OR  (:startDate >= appointment.startOfSession AND
                        :startDate <  appointment.endOfSession ) OR (:endDate <= appointment.endOfSession  AND :endDate >= appointment.startOfSession))`,
                    {
                        doctor: doctorId,
                        startDate,
                        endDate,
                    }
                )
                .getMany();
            if (appointmentEntity.length < 1) {
                return true;
            } else return false;
        } catch (err) {
            return false;
        }
    }
    async getAppointmentsByDoctorId(
        doctorId: string,
        startDate: string,
        endDate: string
    ) {
        try {
            const doctorAuthEntity: AuthEntity = await this.authService.findOne(
                {
                    id: doctorId,
                }
            );

            if (doctorAuthEntity) {
                const appointmentEntity: TimeTableEntity[] = await this.timeTableRepository
                    .createQueryBuilder("appointment")
                    .where(
                        "(doctor_id = :doctor) && :startDate < appointment.startOfSession && appointment.startOfSession < :endDate",
                        {
                            doctor: doctorAuthEntity.id,
                            startDate,
                            endDate,
                        }
                    )
                    .getMany();
                const listTimeTableResDto: ListTimeTableByDoctorResDto[] = await Promise.all(
                    appointmentEntity.map(
                        async (appointmentEntity: TimeTableEntity) => {
                            const referralEntity: ReferralEntity = await this.referralService.findOne(
                                { id: appointmentEntity.referralId }
                            );
                            const examinationEntity: ExaminationEntity = await this.examinationService.findOne(
                                { id: referralEntity.examinationId }
                            );
                            const patientEntity: PatientEntity = await this.patientService.findOne(
                                { id: referralEntity.patientId }
                            );
                            return new ListTimeTableByDoctorResDto(
                                appointmentEntity,
                                patientEntity.name,
                                examinationEntity.name
                            );
                        }
                    )
                );

                return listTimeTableResDto;
            } else {
                throw new NotFoundException(
                    `Appointment ${doctorId} not found`
                );
            }
        } catch (e) {
            return [];
        }
    }
}
