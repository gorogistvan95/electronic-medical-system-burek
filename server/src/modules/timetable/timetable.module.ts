import { forwardRef, Module } from "@nestjs/common";
import { AuthModule } from "../../modules/auth/auth.module";
import { TimeTableService } from "./timetable.service";
import { ReferralModule } from "../../modules/referral/referral.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TimeTableRepository } from "./timetable.repository";
import { TimeTableController } from "./timetable.controller";
import { PatientModule } from "../../modules/patient/patient.module";
import { ExaminationModule } from "../../modules/examination/examination.module";
import { ClinicClassModule } from "../../modules/clinicclass/clinicclass.module";

@Module({
    imports: [
        PatientModule,
        AuthModule,
        ReferralModule,
        TypeOrmModule.forFeature([TimeTableRepository]),
        ExaminationModule,
        ClinicClassModule,
    ],
    controllers: [TimeTableController],
    providers: [TimeTableService],
    exports: [
        TimeTableService,
        TypeOrmModule.forFeature([TimeTableRepository]),
    ],
})
export class TimeTableModule {}
