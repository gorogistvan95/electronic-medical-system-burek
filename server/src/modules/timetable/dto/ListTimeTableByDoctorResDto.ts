"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";

import { IsString } from "class-validator";
import { TimeTableEntity } from "../timetable.entity";

export class ListTimeTableByDoctorResDto {
    @ApiPropertyOptional()
    id: string;

    @ApiPropertyOptional()
    startOfSession: string;

    @ApiPropertyOptional()
    endOfSession: string;

    @ApiPropertyOptional()
    doctorId: string;

    @ApiPropertyOptional()
    referralId: string;

    @ApiPropertyOptional()
    patientName: string;

    @ApiPropertyOptional()
    examinationName: string;

    constructor(
        timeTable: TimeTableEntity,
        examinationName: string,
        patientName: string
    ) {
        this.id = timeTable.id;
        this.startOfSession = new Date(timeTable.startOfSession).toISOString();
        this.endOfSession = new Date(timeTable.endOfSession).toISOString();
        this.doctorId = timeTable.doctorId;
        this.referralId = timeTable.referralId;
        this.patientName = patientName;
        this.examinationName = examinationName;
    }
}
