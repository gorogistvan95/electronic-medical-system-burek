"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { TimeTableEntity } from "../timetable.entity";

export class CreateTimeTableResDto {
    @ApiPropertyOptional()
    readonly id: string;

    @ApiPropertyOptional()
    readonly startOfSession: Date;

    @ApiPropertyOptional()
    readonly endOfSession: Date;

    @ApiPropertyOptional()
    readonly doctorId: string;

    @ApiPropertyOptional()
    readonly referralId: string;

    constructor(appointment: TimeTableEntity) {
        this.id = appointment.id;
        this.startOfSession = appointment.startOfSession;
        this.endOfSession = appointment.endOfSession;
        this.doctorId = appointment.doctor.id;
        this.referralId = appointment.referral.id;
    }
}
