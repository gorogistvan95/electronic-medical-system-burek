"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import {
    IsDate,
    IsDateString,
    IsNumber,
    IsOptional,
    IsString,
} from "class-validator";

export class UpdateTimeTableReqDto {
    @IsOptional()
    @IsDateString()
    @ApiPropertyOptional()
    readonly description: string;

    @IsOptional()
    @IsDateString()
    @ApiPropertyOptional({ type: "timestamp" })
    readonly startOfSession: Date;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional({ type: "timestamp" })
    readonly endOfSession: Date;
}
