export interface TimeTableQueryParams {
    startDate: string;
    endDate: string;
}
