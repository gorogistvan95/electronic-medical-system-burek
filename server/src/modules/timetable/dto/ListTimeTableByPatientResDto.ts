"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { TimeTableEntity } from "../timetable.entity";

export class ListTimeTableByPatientResDto {
    @ApiPropertyOptional()
    id: string;

    @ApiPropertyOptional()
    startOfSession: string;

    @ApiPropertyOptional()
    endOfSession: string;

    @ApiPropertyOptional()
    doctorId: string;

    @ApiPropertyOptional()
    referralId: string;

    @ApiPropertyOptional()
    examinationName: string;

    @ApiPropertyOptional()
    patientName: string;

    @ApiPropertyOptional()
    clinicClassName: string;

    constructor(
        timeTable: TimeTableEntity,
        patientName: string,
        examinationName: string,
        clinicClassName: string
    ) {
        this.id = timeTable.id;
        this.startOfSession = new Date(timeTable.startOfSession).toISOString();
        this.endOfSession = new Date(timeTable.endOfSession).toISOString();
        this.doctorId = timeTable.doctorId;
        this.referralId = timeTable.referralId;
        this.patientName = patientName;
        this.clinicClassName = clinicClassName;
        this.examinationName = examinationName;
    }
}
//eztatirnitimetablesre.
