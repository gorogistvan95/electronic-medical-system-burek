"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";

import { TimeTableEntity } from "../timetable.entity";

export class ListTimeTableResDto {
    @ApiPropertyOptional()
    id: string;

    @ApiPropertyOptional()
    startOfSession: string;

    @ApiPropertyOptional()
    endOfSession: string;

    @ApiPropertyOptional()
    doctorId: string;

    @ApiPropertyOptional()
    referralId: string;

    constructor(timeTable: TimeTableEntity) {
        this.id = timeTable.id;
        this.startOfSession = new Date(timeTable.startOfSession).toISOString();
        this.endOfSession = new Date(timeTable.endOfSession).toISOString();
        this.doctorId = timeTable.doctorId;
        this.referralId = timeTable.referralId;
    }
}
//eztatirnitimetablesre.
