"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";

export class ListTimeTableReqDto {
    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly authId: string;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly referralId: string;
}
