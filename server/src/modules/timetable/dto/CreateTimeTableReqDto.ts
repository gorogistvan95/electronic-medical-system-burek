"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsDate, IsDateString, IsNumber, IsString } from "class-validator";

export class CreateTimeTableReqDto {
    @ApiPropertyOptional()
    @IsDateString()
    startOfSession: Date;

    @ApiPropertyOptional()
    @IsDateString()
    endOfSession: Date;

    @ApiPropertyOptional()
    @IsString()
    authId: string;

    @ApiPropertyOptional()
    @IsString()
    referralId: string;
}
