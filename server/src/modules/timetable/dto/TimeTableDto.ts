"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { AuthEntity } from "../../../modules/auth/auth.entity";
import { ReferralEntity } from "../../../modules/referral/referral.entity";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { TimeTableEntity } from "../timetable.entity";

export class TimeTableDto extends AbstractDto {
    @ApiPropertyOptional()
    startOfSession: Date;

    @ApiPropertyOptional()
    endOfSession: Date;

    @ApiPropertyOptional()
    doctorId: string;

    @ApiPropertyOptional()
    doctor: AuthEntity;

    @ApiPropertyOptional()
    referral: ReferralEntity;

    constructor(timeTable: TimeTableEntity) {
        super(timeTable);
        this.doctor = timeTable.doctor;
        this.doctorId = timeTable.doctorId;
        this.referral = timeTable.referral;
        this.startOfSession = timeTable.startOfSession;
        this.endOfSession = timeTable.endOfSession;
    }
}
