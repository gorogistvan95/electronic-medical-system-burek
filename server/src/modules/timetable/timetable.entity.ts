import { Column, Entity, CreateDateColumn, ManyToOne } from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { ReferralEntity } from "../../modules/referral/referral.entity";
import { TimeTableDto } from "./dto/TimeTableDto";

@Entity({ name: "timetable" })
export class TimeTableEntity extends AbstractEntity<TimeTableDto> {
    @Column({ type: "timestamp" })
    startOfSession: Date;

    @Column({ type: "timestamp" })
    endOfSession: Date;

    @Column({ name: "doctor_id", nullable: false })
    doctorId: string;

    @ManyToOne((type) => AuthEntity, (doctor) => doctor.timeTable, {
        onDelete: "CASCADE",
    })
    doctor: AuthEntity;

    @Column({ name: "referral_id", nullable: false })
    referralId: string;

    @ManyToOne((type) => ReferralEntity, (referral) => referral.timeTable, {
        onDelete: "CASCADE",
    })
    referral: ReferralEntity;

    dtoClass = TimeTableDto;
}
