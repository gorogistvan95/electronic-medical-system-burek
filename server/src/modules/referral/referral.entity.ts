import { ExaminationEntity } from "../../modules/examination/examination.entity";
import {
    Column,
    Entity,
    CreateDateColumn,
    ManyToOne,
    OneToMany,
} from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { PatientEntity } from "../../modules/patient/patient.entity";
import { ReferralDto } from "./dto/ReferralDto";
import { TimeTableEntity } from "../../modules/timetable/timetable.entity";
import { DocumentAvailableEntity } from "../documents-available/document-available.entity";

@Entity({ name: "referral" })
export class ReferralEntity extends AbstractEntity<ReferralDto> {
    @Column({ name: "examination_id", nullable: false })
    examinationId: string;

    @ManyToOne(
        (type) => ExaminationEntity,
        (examination) => examination.referral,
        { onDelete: "CASCADE" }
    )
    examination: ExaminationEntity;

    @Column({ name: "patient_id", nullable: false })
    patientId: string;
    @ManyToOne((type) => PatientEntity, (patient) => patient.referral, {
        onDelete: "CASCADE",
    })
    patient: PatientEntity;

    @Column()
    description: string;

    @OneToMany((type) => TimeTableEntity, (timeTable) => timeTable.referral, {
        onDelete: "CASCADE",
    })
    timeTable: TimeTableEntity[];

    @OneToMany(
        (type) => DocumentAvailableEntity,
        (documentAvalaible) => documentAvalaible.document
    )
    documentAvailable: DocumentAvailableEntity[];

    dtoClass = ReferralDto;
}
