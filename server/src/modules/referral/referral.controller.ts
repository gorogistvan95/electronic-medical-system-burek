import {
    Body,
    Controller,
    HttpCode,
    HttpStatus,
    Post,
    UseGuards,
    UseInterceptors,
    HttpException,
    Get,
    Param,
    Put,
    Delete,
    ForbiddenException,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";

import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "../../modules/auth/auth.service";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { CreateReferralReqDto } from "./dto/CreateReferralReqDto";
import { CreateReferralResponseDto } from "./dto/CreateReferralResponse";
import { ReferralService } from "./referral.service";
import { UpdateReferralReqDto } from "./dto/UpdateReferralReqDto";
import { ListReferralResDto } from "./dto/ListReferralResDto";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/referral")
@ApiTags("api/referral")
export class ReferralController {
    constructor(
        public readonly referralService: ReferralService,
        public readonly authService: AuthService
    ) {}

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Post("create-referral")
    @HttpCode(HttpStatus.OK)
    async createReferral(
        @Body() createReferralReqDto: CreateReferralReqDto
    ): Promise<CreateReferralResponseDto> {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const referral: CreateReferralResponseDto = await this.referralService.createReferral(
                createReferralReqDto
            );
            return referral;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @Put(":id/update-referral")
    @UseGuards(AuthGuard("jwt"))
    @HttpCode(HttpStatus.NO_CONTENT)
    async updateReferral(
        @Param("id") referralId,
        @Body() updateReferralReqDto: UpdateReferralReqDto
    ) {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const user: AuthEntity = AuthService.getAuthUser();
            await this.referralService.updateReferral(
                referralId,
                updateReferralReqDto
            );
            return;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @Delete(":id/delete-referral")
    @UseGuards(AuthGuard("jwt"))
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteReferral(@Param("id") referralId) {
        if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        await this.referralService.deleteReferral(referralId);
    }

    @UseInterceptors(AuthUserInterceptor)
    @Get("patient/:patientId/list-referral")
    @UseGuards(AuthGuard("jwt"))
    @HttpCode(HttpStatus.OK)
    async listReferralByExamination(
        @Param("patientId") patientId
    ): Promise<ListReferralResDto[]> {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const listReferralResDto: ListReferralResDto[] = await this.referralService.listReferralByPatient(
                patientId
            );

            return listReferralResDto;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("/examination/:examinationId/list-referral")
    @HttpCode(HttpStatus.OK)
    async listReferralByPatient(
        @Param("examinationId") examinationId
    ): Promise<ListReferralResDto[]> {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const listReferralResDto: ListReferralResDto[] = await this.referralService.listReferralByExamination(
                examinationId
            );

            return listReferralResDto;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("/patient/:patientId/examination/:examinationId/list-referral")
    @HttpCode(HttpStatus.OK)
    async listReferralByPatientAndExa(
        @Param("patientId") patientId,
        @Param("examinationId") examinationId
    ): Promise<ListReferralResDto[]> {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const listReferralResDto: ListReferralResDto[] = await this.referralService.listReferralByPatientAndExa(
                patientId,
                examinationId
            );

            return listReferralResDto;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
}
