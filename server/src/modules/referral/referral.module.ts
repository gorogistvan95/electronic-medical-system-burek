import { forwardRef, Module } from "@nestjs/common";
import { PatientModule } from "../../modules/patient/patient.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ReferralService } from "./referral.service";
import { ReferralRepository } from "./referral.repository";
import { ExaminationModule } from "../../modules/examination/examination.module";
import { ReferralController } from "./referral.controller";
import { AuthModule } from "../../modules/auth/auth.module";

@Module({
    imports: [
        PatientModule,
        AuthModule,
        ExaminationModule,
        TypeOrmModule.forFeature([ReferralRepository]),
    ],
    controllers: [ReferralController],
    providers: [ReferralService],
    exports: [TypeOrmModule.forFeature([ReferralRepository]), ReferralService],
})
export class ReferralModule {}
