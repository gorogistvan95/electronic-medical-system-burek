"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { ExaminationEntity } from "../../../modules/examination/examination.entity";
import { PatientEntity } from "../../../modules/patient/patient.entity";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { ReferralEntity } from "../referral.entity";

export class ReferralDto extends AbstractDto {
    @ApiPropertyOptional()
    examination: ExaminationEntity;

    @ApiPropertyOptional()
    patient: PatientEntity;

    @ApiPropertyOptional()
    description: string;

    @ApiPropertyOptional()
    examinationId: string;

    constructor(referral: ReferralEntity) {
        super(referral);
        this.examination = referral.examination;
        this.examinationId = referral.examinationId;
        this.patient = referral.patient;
        this.description = referral.description;
    }
}
