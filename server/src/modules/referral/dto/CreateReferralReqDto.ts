"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsString } from "class-validator";



export class CreateReferralReqDto {

    @ApiPropertyOptional()
    @IsString()
    description: string;

    @ApiPropertyOptional()
    @IsString()
    patientId: string;

    @ApiPropertyOptional()
    @IsString()
    examinationId: string;

}
