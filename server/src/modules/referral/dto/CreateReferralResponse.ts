"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { ReferralEntity } from "../referral.entity";

export class CreateReferralResponseDto {
    @ApiPropertyOptional()
    readonly id: string;

    @ApiPropertyOptional()
    readonly examinationId: string;

    @ApiPropertyOptional()
    readonly patientId: string;

    @ApiPropertyOptional()
    readonly description: string;

    constructor(referral: ReferralEntity) {
        this.id = referral.id;
        this.description = referral.description;
        this.examinationId = referral.examination.id;
        this.patientId = referral.patient.id;
    }
}
