"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";

export class UpdateReferralReqDto {
    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly description: string;
}
