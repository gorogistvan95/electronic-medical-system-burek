"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { PatientEntity } from "modules/patient/patient.entity";
import { RoleType } from "../../../common/constants/role-type";
import { ReferralEntity } from "../referral.entity";

export class ListReferralResDto {
    @ApiPropertyOptional()
    readonly id: string;

    @ApiPropertyOptional()
    readonly description: string;

    @ApiPropertyOptional()
    readonly patientId: string;

    @ApiPropertyOptional()
    readonly examinationId?: string;

    constructor(referral: ReferralEntity) {
        this.id = referral.id;
        this.description = referral.description;
        this.patientId = referral.patientId;
        this.examinationId = referral.examinationId;
    }
}
