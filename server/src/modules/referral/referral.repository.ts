import { Repository } from "typeorm";
import { EntityRepository } from "typeorm/decorator/EntityRepository";
import { ReferralEntity } from "./referral.entity";

@EntityRepository(ReferralEntity)
export class ReferralRepository extends Repository<ReferralEntity> { }
