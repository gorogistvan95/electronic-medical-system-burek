import {
    HttpException,
    HttpStatus,
    Injectable,
    NotFoundException,
} from "@nestjs/common";
import { FindConditions } from "typeorm/find-options/FindConditions";
import { ExaminationEntity } from "../../modules/examination/examination.entity";
import { ExaminationService } from "../../modules/examination/examination.service";
import { PatientEntity } from "../../modules/patient/patient.entity";
import { PatientService } from "../../modules/patient/patient.service";
import { CreateReferralReqDto } from "./dto/CreateReferralReqDto";
import { CreateReferralResponseDto } from "./dto/CreateReferralResponse";
import { ReferralEntity } from "./referral.entity";
import { ReferralRepository } from "./referral.repository";
import { UpdateReferralReqDto } from "./dto/UpdateReferralReqDto";
import { ListReferralResDto } from "./dto/ListReferralResDto";
import { createQueryBuilder, getRepository } from "typeorm";
import { AuthService } from "../../modules/auth/auth.service";

@Injectable()
export class ReferralService {
    constructor(
        public readonly referralRepository: ReferralRepository,
        public readonly patientService: PatientService,
        public readonly examinationService: ExaminationService
    ) {}

    async createReferral(
        createReferralReqDto: CreateReferralReqDto
    ): Promise<CreateReferralResponseDto> {
        //Check EXIST USER
        const patientEntity: PatientEntity = await this.patientService.findOne({
            id: createReferralReqDto.patientId,
        });
        const examinationEntity: ExaminationEntity = await this.examinationService.findOne(
            { id: createReferralReqDto.examinationId }
        );
        if (!patientEntity || !examinationEntity) {
            throw new HttpException(
                "patient or examination ID doesnt exist",
                HttpStatus.BAD_REQUEST
            );
        }

        const referral = await this.referralRepository.create({
            description: createReferralReqDto.description,
            patient: patientEntity,
            examination: examinationEntity,
        });

        const referralEntity: ReferralEntity = await this.referralRepository.save(
            referral
        );
        return new CreateReferralResponseDto(referralEntity);
    }

    async deleteReferral(referralId: string) {
        const referralEntity: ReferralEntity = await this.findOne({
            id: referralId,
        });
        if (!referralEntity) {
            throw new NotFoundException(`Referral ${referralId} not found`);
        }

        await this.referralRepository.remove(referralEntity);
    }

    async updateReferral(
        referralId: string,
        updateRefferalDto: UpdateReferralReqDto
    ) {
        const referralEntity: ReferralEntity = await this.findOne({
            id: referralId,
        });
        if (!referralEntity) {
            throw new NotFoundException(`Referral : ${referralId} not found`);
        }
        const updateReferral: ReferralEntity = await this.referralRepository.create(
            updateRefferalDto
        );
        const updatedEntity = await this.referralRepository.save({
            ...referralEntity,
            ...updateReferral,
        });
    }

    async listReferralByPatient(
        patientId: string
    ): Promise<ListReferralResDto[]> {
        const patientEntity: PatientEntity = await this.patientService.findOne({
            id: patientId,
        });
        const referralEntity: ReferralEntity[] = await this.referralRepository.find(
            { patient: patientEntity }
        );
        const listReferralResDto: ListReferralResDto[] = await referralEntity.map(
            (referralEntity) => new ListReferralResDto(referralEntity)
        );
        return listReferralResDto;
    }

    async listReferralByExamination(
        examinationId: string
    ): Promise<ListReferralResDto[]> {
        const examinationEntity: ExaminationEntity = await this.examinationService.findOne(
            {
                id: examinationId,
            }
        );
        const referralEntity: ReferralEntity[] = await this.referralRepository.find(
            { examination: examinationEntity }
        );
        const listReferralResDto: ListReferralResDto[] = await referralEntity.map(
            (referralEntity) => new ListReferralResDto(referralEntity)
        );
        return listReferralResDto;
    }

    async listReferralByPatientAndExa(
        patientId: string,
        examinationId: string
    ): Promise<ListReferralResDto[]> {
        const patientEntity: PatientEntity = await this.patientService.findOne({
            id: patientId,
        });
        const examinationEntity: ExaminationEntity = await this.examinationService.findOne(
            {
                id: examinationId,
            }
        );
        const referralEntity: ReferralEntity[] = await this.referralRepository.find(
            { patient: patientEntity, examination: examinationEntity }
        );
        const listReferralResDto: ListReferralResDto[] = await referralEntity.map(
            (referralEntity) => new ListReferralResDto(referralEntity)
        );
        return listReferralResDto;
    }

    //uselessshit
    async listReferralByOptional(
        patientId?: string,
        examinationId?: string
    ): Promise<ListReferralResDto[]> {
        let patientEntity: PatientEntity;
        let examinationEntity: ExaminationEntity;
        if (patientId) {
            patientEntity = await this.patientService.findOne({
                id: patientId,
            });
            if (!patientEntity) {
                throw new NotFoundException(`patient : ${patientId} not found`);
            }
        }
        if (examinationId) {
            examinationEntity = await this.examinationService.findOne({
                id: examinationId,
            });
            if (!patientEntity) {
                throw new NotFoundException(
                    `examinationId : ${examinationId} not found`
                );
            }
        }

        const referralEntity: ReferralEntity[] = await this.referralRepository.find(
            { patient: patientEntity, examination: examinationEntity }
        );
        const listReferralResDto: ListReferralResDto[] = await referralEntity.map(
            (referralEntity) => new ListReferralResDto(referralEntity)
        );
        return listReferralResDto;
    }

    async findAll(): Promise<ReferralEntity[]> {
        return await this.referralRepository.find({});
    }

    async findOne(
        findData: FindConditions<ReferralEntity>
    ): Promise<ReferralEntity> {
        return this.referralRepository.findOne(findData);
    }
    async proba() {
        var asd = AuthService.getAuthUser().id;
        try {
            const uploaderasdfasdf = AuthService.getAuthUser().id;
            const referral = await getRepository("referral")
                .createQueryBuilder("referral")
                .leftJoinAndSelect(
                    "referral.timeTable",
                    "timeTable",
                    "referral_id = timeTable.referral_id "
                )
                .where("timeTable.doctor_id = :uploaderId", {
                    uploaderId: "4e6ca09b-e38c-4d10-8453-8a62b7903e04",
                })
                .getMany();
            /*.where(
                "timetable.doctor_id= :uploaderId and id=timetable.referral_id",
                {
                    uploaderId: uploader,
                }
            )*/

            const burek = referral;
        } catch (err) {
            console.log(err);
        }
    }
}
