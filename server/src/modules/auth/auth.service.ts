import {
    Injectable,
    ConflictException,
    HttpException,
    HttpStatus,
    NotFoundException,
    BadRequestException,
    UnauthorizedException,
    ForbiddenException,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { UserNotFoundException } from "../../exceptions/user-not-found.exception";
import { ContextService } from "../../providers/context.service";
import { ConfigService } from "../../shared/services/config.service";
import { TokenPayloadDto } from "./dto/TokenPayloadDto";
import { AuthEntity } from "./auth.entity";
import { AuthDto } from "./dto/AuthDto";
import { AuthRepository } from "./auth.repository";
import { createQueryBuilder, FindConditions, getRepository } from "typeorm";
import { UserLoginDto } from "./dto/UserLoginDto";
import { UtilsService } from "../../providers/utils.service";
import { CreateUserDto } from "./dto/CreateUserDto";
import { RoleType } from "../../common/constants/role-type";
import { CoreDto } from "../../common/dto/CoreDto";
import { ClinicClassService } from "../../modules/clinicclass/clinicclass.service";
import { ClinicClassEntity } from "../../modules/clinicclass/clinicclass.entity";
import { ListUsersResDto } from "./dto/ListUsersResDto";
import { UpdateUserReqDto } from "./dto/UpdateUserReqDto";
import { exception } from "console";

@Injectable()
export class AuthService {
    private static _authUserKey = "user_key";
    constructor(
        public readonly authRepository: AuthRepository,
        public readonly jwtService: JwtService,
        public readonly configService: ConfigService,
        public readonly clinicClassService: ClinicClassService
    ) {}
    async createUser(createUserDto: CreateUserDto): Promise<CoreDto> {
        if (
            await this.authRepository.findOne({
                where: { username: createUserDto.username.toLowerCase() },
            })
        ) {
            throw new HttpException("User already exist", HttpStatus.CONFLICT);
        }
        let roleType: RoleType;
        if (createUserDto.role.toLocaleLowerCase() == "user")
            roleType = RoleType.USER;
        else if (createUserDto.role.toLocaleLowerCase() == "receptionist")
            roleType = RoleType.RECEPTIONIST;
        else if (createUserDto.role.toLocaleLowerCase() == "admin")
            roleType = RoleType.ADMIN;
        else throw new BadRequestException("Bad role");

        const clinicClass: ClinicClassEntity = await this.clinicClassService.findOne(
            { id: createUserDto.clinicClassId }
        );
        if (!clinicClass && createUserDto.clinicClassId) {
            throw new HttpException(
                "clinic-class doesnt exist",
                HttpStatus.BAD_REQUEST
            );
        }
        let isDiagnosis: boolean = false;
        if (createUserDto.isDiagnosis == true) {
            isDiagnosis = true;
        }
        let auth: AuthEntity;
        if (createUserDto.name) {
            auth = this.authRepository.create({
                name: createUserDto.name.toLocaleLowerCase(),
                username: createUserDto.username,
                password: createUserDto.password,
                role: roleType,
                isDiagnosis: isDiagnosis,
                clinicClass: clinicClass,
            });
        } else {
            auth = this.authRepository.create({
                username: createUserDto.username.toLocaleLowerCase(),
                password: createUserDto.password,
                role: roleType,
                isDiagnosis: isDiagnosis,
                clinicClass: clinicClass,
            });
        }
        return (await this.authRepository.save(auth)).toCoreDto();
    }

    async createToken(auth: AuthEntity | AuthDto): Promise<TokenPayloadDto> {
        return new TokenPayloadDto({
            expiresIn: this.configService.getNumber("JWT_EXPIRATION_TIME"),
            accessToken: await this.jwtService.signAsync({
                id: auth.id,
                username: auth.username,
                role: auth.role,
                isDiagnosis: auth.isDiagnosis,
            }),
        });
    }

    async listUsersByClinicId(clinicClassId: string): Promise<AuthDto[]> {
        const clinicClassEntity: ClinicClassEntity = await this.clinicClassService.findOne(
            { id: clinicClassId }
        );
        return (
            await this.authRepository.find({
                clinicClass: clinicClassEntity,
            })
        ).toDtos();
    }

    async listUsers() {
        const userListEntities: AuthEntity[] = await this.findAll();

        /* regi listazas
        const listUserResDto = await userListEntities.map(
            async (userListEntities) =>
                new ListUsersResDto(
                    userListEntities,
                   )
                )
        );*/

        const userAndRelatedClinicClass = await getRepository(AuthEntity)
            .createQueryBuilder("auth")
            .leftJoinAndSelect(
                "auth.clinicClass",
                "clinicClass",
                "clinicClass.id = clinic_class_id"
            )
            .getMany();

        const listUserResDto: ListUsersResDto[] = userAndRelatedClinicClass.map(
            (userAndRelatedClinicClass) =>
                new ListUsersResDto(
                    userAndRelatedClinicClass,
                    userAndRelatedClinicClass["clinicClass"]
                )
        );
        return listUserResDto;
    }

    async findAll(): Promise<AuthEntity[]> {
        return await this.authRepository.find({});
    }

    findOne(findData: FindConditions<AuthEntity>): Promise<AuthEntity> {
        return this.authRepository.findOne(findData);
    }

    async validateUser(userLoginDto: UserLoginDto): Promise<AuthDto> {
        const user: AuthEntity = await this.findOne({
            username: userLoginDto.username.toLowerCase(),
        });
        const isPasswordValid: boolean = await UtilsService.validateHash(
            userLoginDto.password,
            user && user.password
        );
        if (!user || !isPasswordValid) {
            throw new UserNotFoundException();
        }
        return new AuthDto(user);
    }
    async deleteUser(userId: string) {
        const authUserEntity: AuthEntity = await this.authRepository.findOne(
            userId
        );
        if (!authUserEntity) {
            throw new NotFoundException(`User ${userId} not found`);
        }
        await this.authRepository.remove(authUserEntity);
    }

    async updateUser(userId: string, updateUserReqDto: UpdateUserReqDto) {
        const authUserEntity: AuthEntity = await this.authRepository.findOne(
            userId
        );
        /*** Későbbiekben ha azt szeretnenk ha az adatokat csak akkor lehessen modositani ha megadjuk a regi jelszavat..
        const isPasswordValid: boolean = await UtilsService.validateHash(
            updateUserReqDto.passwordValidation,
            authUserEntity && authUserEntity.password
        );

            if (!isPasswordValid) {
            throw new NotFoundException("Incorrect password");
        }
*/
        if (!authUserEntity) {
            throw new NotFoundException(`User : ${userId} not found`);
        }

        const updateAuthEntity: AuthEntity = await this.authRepository.create(
            updateUserReqDto
        );
        updateAuthEntity.username = updateAuthEntity.username.toLocaleLowerCase();
        const createdUser = await this.authRepository.save({
            ...authUserEntity,
            ...updateAuthEntity,
        });
        if (!createdUser) {
            throw new BadRequestException("The user couldnt be saved into db");
        }
    }

    findOneAuth(findData: FindConditions<AuthEntity>): Promise<AuthEntity> {
        return this.authRepository.findOne(findData);
    }

    static setAuthUser(user: AuthEntity) {
        ContextService.set(AuthService._authUserKey, user);
    }

    static getAuthUser(): AuthEntity {
        return ContextService.get(AuthService._authUserKey);
    }
}
