import {
    Body,
    Controller,
    HttpCode,
    HttpStatus,
    Post,
    UploadedFile,
    UseGuards,
    UseInterceptors,
    HttpException,
    UsePipes,
    ValidationPipe,
    Delete,
    Param,
    Put,
    Get,
    ForbiddenException,
} from "@nestjs/common";
import { FileInterceptor } from "@nestjs/platform-express";
import {
    ApiBearerAuth,
    ApiConsumes,
    ApiOkResponse,
    ApiTags,
} from "@nestjs/swagger";

import { AuthService } from "./auth.service";
import { LoginPayloadDto } from "./dto/LoginPayloadDto";
import { UserLoginDto } from "./dto/UserLoginDto";
import { AuthDto } from "./dto/AuthDto";
import { TokenPayloadDto } from "./dto/TokenPayloadDto";
import { CoreDto } from "../../common/dto/CoreDto";
import { CreateUserDto } from "./dto/CreateUserDto";
import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { AuthGuard } from "@nestjs/passport";
import { UpdateUserReqDto } from "./dto/UpdateUserReqDto";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/auth")
@ApiTags("auth")
export class AuthController {
    constructor(public readonly authService: AuthService) {}

    @UseInterceptors(AuthUserInterceptor)
    @Post("register")
    @HttpCode(HttpStatus.OK)
    @UsePipes(new ValidationPipe({ transform: true }))
    @ApiOkResponse({ type: AuthDto, description: "Successfully Registered" })
    async userRegister(@Body() createUserDto: CreateUserDto): Promise<CoreDto> {
        try {
            if (AuthService.getAuthUser().role != RoleType.ADMIN) {
                throw new ForbiddenException(
                    "Have no access rights! You are not admin"
                );
            }
            const createdUserCoreDto: CoreDto = await this.authService.createUser(
                createUserDto
            );
            return createdUserCoreDto;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete(":id/delete-user")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteUserById(@Param("id") userId) {
        if (AuthService.getAuthUser().role != RoleType.ADMIN) {
            throw new ForbiddenException(
                "Have no access rights! You are not admin"
            );
        }
        await this.authService.deleteUser(userId);
        return;
    }
    catch(err) {
        throw new HttpException(err, HttpStatus.BAD_REQUEST);
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Put(":id/update-user")
    @HttpCode(HttpStatus.NO_CONTENT)
    async updateUser(
        @Param("id") userId,
        @Body() updateUserReqDto: UpdateUserReqDto
    ) {
        try {
            if (AuthService.getAuthUser().role != RoleType.ADMIN) {
                throw new ForbiddenException(
                    "Have no access rights! You are not admin"
                );
            }
            await this.authService.updateUser(userId, updateUserReqDto);
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
    @Post("login")
    @HttpCode(HttpStatus.OK)
    @ApiOkResponse({
        type: LoginPayloadDto,
        description: "User info with access token",
    })
    async userLogin(
        @Body() userLoginDto: UserLoginDto
    ): Promise<LoginPayloadDto> {
        const authDto: AuthDto = await this.authService.validateUser(
            userLoginDto
        );
        const token: TokenPayloadDto = await this.authService.createToken(
            authDto
        );
        return new LoginPayloadDto(token);
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete(":id/delete-user")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteUser(@Param("id") userId) {
        if (AuthService.getAuthUser().role != RoleType.ADMIN) {
            throw new ForbiddenException(
                "Have no permission! You are not admin"
            );
        }
        await this.authService.deleteUser(userId);
        return;
    }
    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("list-users")
    @HttpCode(HttpStatus.OK)
    async listUser() {
        if (AuthService.getAuthUser().role != RoleType.ADMIN) {
            throw new ForbiddenException(
                "Have no permission! You are not admin"
            );
        }
        const userList = await this.authService.listUsers();
        return userList;
    }
    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get(":id/list-doctors/")
    @HttpCode(HttpStatus.OK)
    async listDoctorsByClinicId(@Param("id") id): Promise<AuthDto[]> {
        try {
            if (
                AuthService.getAuthUser().role != RoleType.ADMIN &&
                AuthService.getAuthUser().role != RoleType.RECEPTIONIST
            ) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const userList: AuthDto[] = await this.authService.listUsersByClinicId(
                id
            );
            return userList;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
}
