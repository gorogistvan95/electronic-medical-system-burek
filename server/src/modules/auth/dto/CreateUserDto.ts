"use strict";

import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import {
    isString,
    IsNotEmpty,
    IsString,
    MinLength,
    IsOptional,
    IsBoolean,
} from "class-validator";
import { Column } from "typeorm";

export class CreateUserDto {
    @IsOptional()
    @IsString()
    @ApiProperty()
    readonly name?: string;

    @IsString()
    @MinLength(5)
    @ApiProperty()
    readonly username: string;

    @IsString()
    @MinLength(5)
    @ApiProperty({ minLength: 5 })
    readonly password: string;

    @IsString()
    @ApiProperty({ minLength: 4 })
    readonly role: string;

    @IsBoolean()
    @IsOptional()
    @ApiProperty()
    readonly isDiagnosis?: boolean;

    @IsString()
    @IsOptional()
    @ApiProperty()
    readonly clinicClassId?: string;
}
