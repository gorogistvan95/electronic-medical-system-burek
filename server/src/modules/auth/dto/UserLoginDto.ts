"use strict";

import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsString, IsNotEmpty } from "class-validator";

export class UserLoginDto {
    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    readonly username: string;

    @IsNotEmpty()
    @IsString()
    @ApiProperty()
    readonly password: string;
}
