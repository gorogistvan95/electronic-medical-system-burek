"use strict";

import { Optional } from "@nestjs/common";
import { ApiProperty } from "@nestjs/swagger";
import {
    isString,
    IsNotEmpty,
    IsString,
    MinLength,
    IsOptional,
} from "class-validator";
import { Column } from "typeorm";

export class UpdateUserReqDto {
    @IsOptional()
    @IsString()
    readonly name?: string;

    @IsOptional()
    @IsString()
    @MinLength(5)
    @ApiProperty()
    readonly username?: string;

    @IsOptional()
    @IsString()
    @MinLength(6)
    @ApiProperty()
    readonly passwordValidation?: string;

    @IsOptional()
    @IsString()
    @MinLength(6)
    @ApiProperty({ minLength: 6 })
    readonly password?: string;
}
