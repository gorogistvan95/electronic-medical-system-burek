"use strict";
import { ApiPropertyOptional } from "@nestjs/swagger";
import { ClinicClassEntity } from "../../../modules/clinicclass/clinicclass.entity";
import { RoleType } from "../../../common/constants/role-type";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { AuthEntity } from "../auth.entity";

export class AuthDto extends AbstractDto {
    @ApiPropertyOptional({ enum: RoleType })
    name?: string;

    @ApiPropertyOptional({ enum: RoleType })
    role: RoleType;

    @ApiPropertyOptional()
    username: string;

    @ApiPropertyOptional()
    password: string;

    @ApiPropertyOptional()
    lastLogin: Date;

    @ApiPropertyOptional()
    isDiagnosis: boolean;

    @ApiPropertyOptional()
    clinicClass?: ClinicClassEntity;

    constructor(auth: AuthEntity) {
        super(auth);
        this.name = auth.name;
        this.role = auth.role;
        this.username = auth.username;
        this.isDiagnosis = auth.isDiagnosis;
        this.password = auth.password;
        this.lastLogin = auth.lastLogin;
        this.clinicClass = auth.clinicClass;
    }
}
