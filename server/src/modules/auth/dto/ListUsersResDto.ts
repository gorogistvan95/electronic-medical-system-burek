"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { ClinicClassEntity } from "../../../modules/clinicclass/clinicclass.entity";
import { RoleType } from "../../../common/constants/role-type";
import { AuthEntity } from "../auth.entity";

export class ListUsersResDto {
    @ApiPropertyOptional()
    readonly id: string;

    @ApiPropertyOptional()
    readonly name: string;

    @ApiPropertyOptional()
    readonly username: string;

    @ApiPropertyOptional()
    readonly role: RoleType;

    @ApiPropertyOptional()
    readonly clinicClassId?: string;

    @ApiPropertyOptional()
    clinicClassName?: string;

    @ApiPropertyOptional()
    readonly isDiagnosis?: boolean;

    @ApiPropertyOptional()
    readonly createdAt?: Date;

    @ApiPropertyOptional()
    readonly updatedAt?: Date;

    constructor(authUser: AuthEntity, clinicClassEntity: ClinicClassEntity) {
        this.id = authUser.id;
        this.name = authUser.name;
        this.username = authUser.username;
        this.role = authUser.role;
        this.isDiagnosis = authUser.isDiagnosis;
        this.createdAt = authUser.createdAt;
        this.updatedAt = authUser.updatedAt;
        this.clinicClassId = authUser.clinicClassId;
        this.clinicClassName = null;
        if (clinicClassEntity) {
            this.clinicClassName = clinicClassEntity.name;
        }
    }
}
