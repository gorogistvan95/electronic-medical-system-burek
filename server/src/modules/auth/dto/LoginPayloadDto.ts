"use strict";

import { ApiProperty } from "@nestjs/swagger";
import { TokenPayloadDto } from "./TokenPayloadDto";
import { AuthDto } from "./AuthDto";

export class LoginPayloadDto {
    @ApiProperty({ type: AuthDto })
    auth: AuthDto;
    @ApiProperty({ type: TokenPayloadDto })
    token: TokenPayloadDto;

    constructor(token: TokenPayloadDto, auth?: AuthDto) {
        this.auth = auth;
        this.token = token;
    }
}
