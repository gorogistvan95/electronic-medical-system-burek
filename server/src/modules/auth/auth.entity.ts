import { TimeTableEntity } from "../../modules/timetable/timetable.entity";
import {
    Column,
    Entity,
    CreateDateColumn,
    ManyToOne,
    OneToMany,
} from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { RoleType } from "../../common/constants/role-type";
import { AuthDto } from "./dto/AuthDto";
import { ClinicClassEntity } from "../../modules/clinicclass/clinicclass.entity";
import { DocumentEntity } from "../../modules/document/document.entity";
import { DiagnosisEntity } from "../../modules/diagnosis/diagnosis.entity";

@Entity({ name: "auth" })
export class AuthEntity extends AbstractEntity<AuthDto> {
    @Column({ nullable: true })
    name?: string;

    @Column({ unique: true })
    username: string;

    @Column()
    password: string;

    @Column({ type: "enum", enum: RoleType, default: RoleType.USER })
    role: RoleType;

    @Column("boolean", { default: false })
    isDiagnosis: boolean;

    @Column({ name: "clinic_class_id", nullable: true })
    clinicClassId: string;

    @ManyToOne(
        (type) => ClinicClassEntity,
        (clinicClass) => clinicClass.doctor,
        { onDelete: "CASCADE" }
    )
    clinicClass?: ClinicClassEntity;

    @CreateDateColumn({
        name: "last_login",
        precision: 3,
        default: () => "CURRENT_TIMESTAMP(3)",
    })
    lastLogin: Date;

    @OneToMany((type) => TimeTableEntity, (timeTable) => timeTable.doctor, {
        onDelete: "CASCADE",
    })
    timeTable: TimeTableEntity[];

    @OneToMany((type) => DocumentEntity, (document) => document.uploader, {
        onDelete: "SET NULL",
        nullable: true,
    })
    document: DocumentEntity[];

    @OneToMany((type) => DiagnosisEntity, (diagnosis) => diagnosis.uploader, {
        nullable: true,
        onDelete: "SET NULL",
    })
    diagnosis: DiagnosisEntity[];

    dtoClass = AuthDto;
}
