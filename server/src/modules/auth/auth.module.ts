import { forwardRef, Module } from "@nestjs/common";
import { PassportModule } from "@nestjs/passport";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { JwtStrategy } from "./jwt.strategy";
import { AuthRepository } from "./auth.repository";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ClinicClassModule } from "../../modules/clinicclass/clinicclass.module";
import { PatientModule } from "../../modules/patient/patient.module";

@Module({
    imports: [
        ClinicClassModule,
        PassportModule.register({ defaultStrategy: "jwt" }),
        TypeOrmModule.forFeature([AuthRepository]),
        PatientModule,
    ],
    controllers: [AuthController],
    providers: [AuthService, JwtStrategy],
    exports: [
        PassportModule.register({ defaultStrategy: "jwt" }),
        AuthService,
        TypeOrmModule.forFeature([AuthRepository]),
    ],
})
export class AuthModule {}
