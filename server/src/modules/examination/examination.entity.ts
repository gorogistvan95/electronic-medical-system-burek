import { ClinicClassEntity } from "../../modules/clinicclass/clinicclass.entity";
import { Column, Entity, ManyToOne, OneToMany } from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { ExaminationDto } from "./dto/ExamationDto";
import { ReferralEntity } from "../../modules/referral/referral.entity";

@Entity({ name: "examination" })
export class ExaminationEntity extends AbstractEntity<ExaminationDto> {
    @Column()
    name: string;

    @Column({ default: 0 })
    price?: number;

    @Column({ type: "text" })
    description?: string;

    @Column({ name: "clinic_class_id", nullable: false })
    clinicClassId: string;
    @ManyToOne(
        (type) => ClinicClassEntity,
        (clinicClass) => clinicClass.examination,
        { onDelete: "CASCADE" }
    )
    clinicClass: ClinicClassEntity;

    @OneToMany((type) => ReferralEntity, (referral) => referral.examination, {})
    referral: ReferralEntity[];

    dtoClass = ExaminationDto;
}
