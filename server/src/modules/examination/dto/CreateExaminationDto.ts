"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsString } from "class-validator";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { ExaminationEntity } from "../examination.entity";



export class CreateExaminationDto {

    @ApiPropertyOptional()
    @IsString()
    name: string;

    @ApiPropertyOptional()
    @IsNumber()
    price?: number;


    @ApiPropertyOptional()
    @IsString()
    description?: string;

    @ApiPropertyOptional()
    @IsString()
    clinicClassId: string;

}
