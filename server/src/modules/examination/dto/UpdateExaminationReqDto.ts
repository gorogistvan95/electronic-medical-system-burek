"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";

export class UpdateExaminationReqDto {
    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly name: string;

    @IsOptional()
    @IsNumber()
    @ApiPropertyOptional()
    readonly price: number;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly description: string;
}
