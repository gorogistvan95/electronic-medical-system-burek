"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";

import { IsString } from "class-validator";
import { ExaminationEntity } from "../examination.entity";

export class ListExaminationResponseDto {
    @ApiPropertyOptional()
    id: string;

    @ApiPropertyOptional()
    name: string;

    @ApiPropertyOptional()
    price: number;

    @ApiPropertyOptional()
    description: string;

    constructor(examination: ExaminationEntity) {
        this.id = examination.id;
        this.price = examination.price;
        this.name = examination.name;
        this.description = examination.description;
    }
}
