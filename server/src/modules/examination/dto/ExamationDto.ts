"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { ClinicClassEntity } from "../../clinicclass/clinicclass.entity";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { ExaminationEntity } from "../examination.entity";



export class ExaminationDto extends AbstractDto {
    @ApiPropertyOptional()
    name: string;

    @ApiPropertyOptional()
    price?: number;

    @ApiPropertyOptional()
    description?: string;

    @ApiPropertyOptional()
    clinicClass: ClinicClassEntity;



    constructor(examination: ExaminationEntity) {
        super(examination);
        this.name = examination.name;
        this.price = examination.price;
        this.description = examination.description;
        this.clinicClass = examination.clinicClass;
    }
}
