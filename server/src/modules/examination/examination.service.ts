import {
    Injectable,
    HttpException,
    HttpStatus,
    NotFoundException,
} from "@nestjs/common";
import { ClinicClassEntity } from "../../modules/clinicclass/clinicclass.entity";
import { ClinicClassService } from "../../modules/clinicclass/clinicclass.service";
import { FindConditions } from "typeorm/find-options/FindConditions";
import { CreateExaminationDto } from "./dto/CreateExaminationDto";
import { ListExaminationResponseDto } from "./dto/ListExaminationResponseDto";
import { ExaminationEntity } from "./examination.entity";
import { ExaminationRepository } from "./examination.repository";
import { UpdateExaminationReqDto } from "./dto/UpdateExaminationReqDto";

@Injectable()
export class ExaminationService {
    constructor(
        public readonly examinationRepository: ExaminationRepository,
        public readonly clinicClassService: ClinicClassService
    ) {}

    async createExamination(
        createExaminationDto: CreateExaminationDto
    ): Promise<ExaminationEntity> {
        const clinicClassEntity: ClinicClassEntity = await this.clinicClassService.findOne(
            {
                id: createExaminationDto.clinicClassId,
            }
        );
        if (!clinicClassEntity) {
            throw new HttpException(
                "Choosed Clinic-Class doesnt exist",
                HttpStatus.BAD_REQUEST
            );
        }
        const examination = await this.examinationRepository.create({
            name: createExaminationDto.name,
            price: createExaminationDto.price,
            description: createExaminationDto.description,
            clinicClass: clinicClassEntity,
        });

        return await this.examinationRepository.save(examination);
    }
    async listExamination(
        clinicClassId: string
    ): Promise<ListExaminationResponseDto[]> {
        const clinicClassEntityInput: ClinicClassEntity = await this.clinicClassService.findOne(
            { id: clinicClassId }
        );
        const examinationEntity: ExaminationEntity[] = await this.examinationRepository.find(
            { clinicClass: clinicClassEntityInput }
        );
        const listExaminationDto: ListExaminationResponseDto[] = await examinationEntity.map(
            (examinationEntity) =>
                new ListExaminationResponseDto(examinationEntity)
        );
        return listExaminationDto;
    }

    async deleteExamination(examinationId: string) {
        const examinationEntity: ExaminationEntity = await this.findOne({
            id: examinationId,
        });
        if (!examinationEntity) {
            throw new NotFoundException(
                `Examination ${examinationId} not found`
            );
        }
        await this.examinationRepository.remove(examinationEntity);
    }

    async updateExamination(
        examinationId: string,
        updateExaminationReqDto: UpdateExaminationReqDto
    ) {
        const examinationEntity: ExaminationEntity = await this.findOne({
            id: examinationId,
        });
        if (!examinationEntity) {
            throw new NotFoundException(
                `Examination : ${examinationId} not found`
            );
        }

        const updateExamination: ExaminationEntity = await this.examinationRepository.create(
            updateExaminationReqDto
        );
        const geci = await this.examinationRepository.save({
            ...examinationEntity,
            ...updateExamination,
        });
    }

    async findOne(
        findData: FindConditions<ExaminationEntity>
    ): Promise<ExaminationEntity> {
        return this.examinationRepository.findOne(findData);
    }

    async findAll(): Promise<ExaminationEntity[]> {
        return await this.examinationRepository.find({});
    }
}
