import { Repository } from "typeorm";
import { EntityRepository } from "typeorm/decorator/EntityRepository";
import { ExaminationEntity } from "./examination.entity";


@EntityRepository(ExaminationEntity)
export class ExaminationRepository extends Repository<ExaminationEntity> { }
