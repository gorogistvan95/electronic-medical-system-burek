import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PatientModule } from "../../modules/patient/patient.module";
import { ClinicClassModule } from "../../modules/clinicclass/clinicclass.module";
import { ClinicClassService } from "../../modules/clinicclass/clinicclass.service";
import { ClinicClassRepository } from "../clinicclass/clinicclass.repository";
import { ExaminationController } from "./examination.controller";
import { ExaminationRepository } from "./examination.repository";
import { ExaminationService } from "./examination.service";
@Module({
    imports: [
        ClinicClassModule,
        PatientModule,
        TypeOrmModule.forFeature([ExaminationRepository]),
        TypeOrmModule.forFeature([ClinicClassRepository]),
    ],
    controllers: [ExaminationController],
    providers: [ExaminationService, ClinicClassService],
    exports: [
        ExaminationService,
        TypeOrmModule.forFeature([ExaminationRepository]),
    ],
})
export class ExaminationModule {}
