import {
    Controller,
    HttpCode,
    HttpStatus,
    UseGuards,
    UseInterceptors,
    Param,
    Delete,
    Put,
    Body,
    HttpException,
    Post,
    Get,
    ForbiddenException,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "../../modules/auth/auth.service";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { ExaminationService } from "./examination.service";
import { UpdateExaminationReqDto } from "./dto/UpdateExaminationReqDto";
import { ListExaminationResponseDto } from "./dto/ListExaminationResponseDto";
import { CoreDto } from "common/dto/CoreDto";
import { CreateExaminationDto } from "./dto/CreateExaminationDto";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/examination")
@ApiTags("api/examination")
export class ExaminationController {
    constructor(public readonly examinationService: ExaminationService) {}

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Post("create-examination")
    @HttpCode(HttpStatus.OK)
    async createExamination(
        @Body() createExaminationDto: CreateExaminationDto
    ): Promise<CoreDto> {
        const createExamination: CoreDto = await await this.examinationService.createExamination(
            createExaminationDto
        );
        return createExamination;
    }

    @UseGuards(AuthGuard("jwt"))
    @UseInterceptors(AuthUserInterceptor)
    @Get(":id/list-examination/")
    @HttpCode(HttpStatus.OK)
    async listExamination(
        @Param("id") id
    ): Promise<ListExaminationResponseDto[]> {
        if (
            AuthService.getAuthUser().role != RoleType.ADMIN &&
            AuthService.getAuthUser().role != RoleType.RECEPTIONIST
        ) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        const listExamination: ListExaminationResponseDto[] = await this.examinationService.listExamination(
            id
        );
        return listExamination;
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete(":id/delete-examination")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteExamination(@Param("id") examinationId) {
        if (AuthService.getAuthUser().role != RoleType.ADMIN) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        await this.examinationService.deleteExamination(examinationId);
        return;
    }

    @UseGuards(AuthGuard("jwt"))
    @UseInterceptors(AuthUserInterceptor)
    @Put(":id/update-examination")
    @HttpCode(HttpStatus.NO_CONTENT)
    async updateExamination(
        @Param("id") examinationId,
        @Body() updateExaminationReqDto: UpdateExaminationReqDto
    ) {
        try {
            if (AuthService.getAuthUser().role != RoleType.ADMIN) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            await this.examinationService.updateExamination(
                examinationId,
                updateExaminationReqDto
            );
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
}
