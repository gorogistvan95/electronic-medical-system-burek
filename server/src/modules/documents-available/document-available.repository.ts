import { Repository } from "typeorm";
import { EntityRepository } from "typeorm/decorator/EntityRepository";
import { DocumentAvailableEntity } from "./document-available.entity";

@EntityRepository(DocumentAvailableEntity)
export class DocumentAvailableRepository extends Repository<
    DocumentAvailableEntity
> {}
