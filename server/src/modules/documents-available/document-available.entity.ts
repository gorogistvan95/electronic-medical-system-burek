import {
    Column,
    Entity,
    CreateDateColumn,
    ManyToOne,
    OneToMany,
} from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { DocumentEntity } from "../document/document.entity";
import { ReferralEntity } from "../referral/referral.entity";
import { DocumentsAvailableDto } from "./dto/DocumentAvalaibleDto";

@Entity({ name: "document-avalaible" })
export class DocumentAvailableEntity extends AbstractEntity<
    DocumentsAvailableDto
> {
    @Column({ name: "document_id", nullable: false })
    documentId: string;

    @ManyToOne(
        (type) => DocumentEntity,
        (document) => document.documentsAvailable,
        { onDelete: "CASCADE" }
    )
    document: DocumentEntity;

    @Column({ name: "referral_id", nullable: false })
    referralId: string;

    @ManyToOne(
        (type) => ReferralEntity,
        (referral) => referral.documentAvailable,
        {
            onDelete: "CASCADE",
        }
    )
    referral: ReferralEntity;

    dtoClass = DocumentsAvailableDto;
}
