import {
    Controller,
    HttpCode,
    HttpStatus,
    UseGuards,
    UseInterceptors,
    Param,
    Delete,
    Body,
    Post,
    Get,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiTags } from "@nestjs/swagger";
import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { AuthService } from "../../modules/auth/auth.service";
import { DocumentAvailableService } from "./document-available.service";
import { CreateDocAvailableDocumentReqDto } from "./dto/CreateDocAvailableReqDto";
import { DeleteDocAvailableReqDto } from "./dto/DeleteDocAvailableReqDto";
import { ListDocumentAvailableResDto } from "./dto/ListDocAvalaibleResDto";

@Controller("api/document-available")
@ApiTags("api/document-available")
export class DocumentAvailableController {
    constructor(
        public readonly documentAvailableService: DocumentAvailableService
    ) {}
    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Post("create-document-available")
    @HttpCode(HttpStatus.CREATED)
    async createDocumentAvailable(
        @Body()
        createDocAvailableDocumentReqDto: CreateDocAvailableDocumentReqDto[]
    ) {
        const user: AuthEntity = AuthService.getAuthUser();
        await this.documentAvailableService.createDocumentAvailable(
            createDocAvailableDocumentReqDto
        );
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get(":referralId/list-document-available")
    async listDocumentAvailable(
        @Param("referralId") referralId
    ): Promise<ListDocumentAvailableResDto[]> {
        const user: AuthEntity = AuthService.getAuthUser();
        return await this.documentAvailableService.listAvailableDocumentsByReferral(
            referralId
        );
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete("delete-documents-available")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteDocumentsAvailable(
        @Body() deleteDocAvailableReqDto: DeleteDocAvailableReqDto[]
    ) {
        const user: AuthEntity = AuthService.getAuthUser();
        await this.documentAvailableService.deleteDocumentsAvailable(
            deleteDocAvailableReqDto
        );
    }
}
