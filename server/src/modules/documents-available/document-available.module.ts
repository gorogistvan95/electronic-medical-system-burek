import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "../auth/auth.module";
import { DocumentModule } from "../document/document.module";
import { DocumentAvailableRepository } from "./document-available.repository";
import { DocumentAvailableService } from "./document-available.service";
import { DocumentAvailableController } from "./document-available.controller";
import { PatientModule } from "../../modules/patient/patient.module";
import { ReferralModule } from "../../modules/referral/referral.module";

@Module({
    imports: [
        TypeOrmModule.forFeature([DocumentAvailableRepository]),
        PatientModule,
        ReferralModule,
        DocumentModule,
        AuthModule,
    ],
    controllers: [DocumentAvailableController],
    providers: [DocumentAvailableService],
    exports: [
        DocumentAvailableService,
        TypeOrmModule.forFeature([DocumentAvailableRepository]),
    ],
})
export class DocumentAvailableModule {}
