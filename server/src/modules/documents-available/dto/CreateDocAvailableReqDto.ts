"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsNumber, IsOptional, IsString } from "class-validator";

export class CreateDocAvailableDocumentReqDto {
    @ApiPropertyOptional()
    @IsString()
    documentId: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    referralId: string;
}
