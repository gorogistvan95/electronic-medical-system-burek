"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsNumber, IsOptional, IsString } from "class-validator";

export class DeleteDocAvailableReqDto {
    @ApiPropertyOptional()
    @IsString()
    id: string;
}
