"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocumentEntity } from "../../document/document.entity";
import { ReferralEntity } from "../../referral/referral.entity";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { AuthEntity } from "../../auth/auth.entity";
import { DocumentAvailableEntity } from "../document-available.entity";

export class DocumentsAvailableDto extends AbstractDto {
    @ApiPropertyOptional()
    document: DocumentEntity;

    @ApiPropertyOptional()
    referral: ReferralEntity;
    constructor(documentAvailable: DocumentAvailableEntity) {
        super(documentAvailable);
        this.document = documentAvailable.document;
        this.referral = documentAvailable.referral;
    }
}
