"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocumentAvailableEntity } from "../document-available.entity";

export class ListDocumentAvailableResDto {
    @ApiPropertyOptional()
    readonly id: string;

    @ApiPropertyOptional()
    readonly documentId: string;

    @ApiPropertyOptional()
    readonly referralId: string;

    constructor(documentAvailable: DocumentAvailableEntity) {
        this.id = documentAvailable.id;
        this.documentId = documentAvailable.documentId;
        this.referralId = documentAvailable.referralId;
    }
}
