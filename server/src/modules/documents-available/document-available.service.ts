import {
    HttpException,
    HttpStatus,
    Injectable,
    NotFoundException,
    UploadedFile,
} from "@nestjs/common";
import { CreateDocAvailableDocumentReqDto } from "./dto/CreateDocAvailableReqDto";
import { DocumentAvailableRepository } from "./document-available.repository";
import { DocumentService } from "../../modules/document/document.service";
import { ReferralService } from "../../modules/referral/referral.service";
import { DocumentEntity } from "../../modules/document/document.entity";
import { ReferralEntity } from "../../modules/referral/referral.entity";
import { DocumentAvailableEntity } from "./document-available.entity";
import { FindConditions } from "typeorm";
import { DocumentsAvailableDto } from "./dto/DocumentAvalaibleDto";
import { ListDocumentAvailableResDto } from "./dto/ListDocAvalaibleResDto";
import { DeleteDocAvailableReqDto } from "./dto/DeleteDocAvailableReqDto";

@Injectable()
export class DocumentAvailableService {
    constructor(
        public readonly documentAvalaibleRepository: DocumentAvailableRepository,
        public readonly documentService: DocumentService,
        public readonly referralService: ReferralService
    ) {}

    async createDocumentAvailable(
        createDocAvailableDocumentReqDto: CreateDocAvailableDocumentReqDto[]
    ) {
        let documentEntity: DocumentEntity;
        let responseDocAvalaibleEntities: DocumentAvailableEntity[] = [];
        for (const prop in createDocAvailableDocumentReqDto) {
            let locDocumentEntity: DocumentEntity = await this.documentService.findOne(
                {
                    id: createDocAvailableDocumentReqDto[prop].documentId,
                }
            );
            let referralEntity: ReferralEntity = await this.referralService.findOne(
                {
                    id: createDocAvailableDocumentReqDto[prop].referralId,
                }
            );
            if (prop == "toDtos") {
                break;
            }
            if (!locDocumentEntity || !referralEntity) {
                throw new NotFoundException(
                    "documentID or ReferralID  doesnt exist"
                );
            }

            const docAvailable = await this.documentAvalaibleRepository.create({
                document: locDocumentEntity,
                referral: referralEntity,
            });
            const docAvailableEntity: DocumentAvailableEntity = await this.documentAvalaibleRepository.save(
                docAvailable
            );
            responseDocAvalaibleEntities.push(docAvailableEntity);
        }

        return await responseDocAvalaibleEntities.map(
            (responseDocAvalaibleEntities) =>
                new DocumentsAvailableDto(responseDocAvalaibleEntities)
        );
    }
    async listAvailableDocumentsByReferral(
        referralId: string
    ): Promise<ListDocumentAvailableResDto[]> {
        const referralEntity: ReferralEntity = await this.referralService.findOne(
            {
                id: referralId,
            }
        );
        if (!!referralEntity) {
            throw new NotFoundException("ReferralID  doesnt exist");
        }
        const documentAvailable: DocumentAvailableEntity[] = await this.documentAvalaibleRepository.find(
            { referral: referralEntity }
        );
        const listDocumentAvailableResDto: ListDocumentAvailableResDto[] = documentAvailable.map(
            (documentAvailable) =>
                new ListDocumentAvailableResDto(documentAvailable)
        );
        return listDocumentAvailableResDto;
    }

    async deleteDocumentsAvailable(
        deleteDocAvailableReqDto: DeleteDocAvailableReqDto[]
    ) {
        for (const prop in deleteDocAvailableReqDto) {
            if (prop == "toDtos") {
                break;
            }
            const documentAvailable: DocumentAvailableEntity = await this.findOne(
                {
                    id: deleteDocAvailableReqDto[prop].id,
                }
            );
            if (!documentAvailable) {
                throw new NotFoundException(
                    `Document ${deleteDocAvailableReqDto[prop].id}  not found`
                );
            }
            await this.documentAvalaibleRepository.remove(documentAvailable);
        }
    }
    async findOne(
        findData: FindConditions<DocumentAvailableEntity>
    ): Promise<DocumentAvailableEntity> {
        return this.documentAvalaibleRepository.findOne(findData);
    }
}
