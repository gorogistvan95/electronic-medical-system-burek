"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocumentEntity } from "../document.entity";

export class CreateDocumentResDto {
    @ApiPropertyOptional()
    readonly name: string;

    @ApiPropertyOptional()
    readonly id: string;

    @ApiPropertyOptional()
    readonly documentPath: string;

    @ApiPropertyOptional()
    readonly description?: string;

    @ApiPropertyOptional()
    readonly isAvailableAlways: boolean;

    @ApiPropertyOptional()
    readonly uploader: string;

    @ApiPropertyOptional()
    readonly patient: string;

    constructor(document: DocumentEntity) {
        this.id = document.id;
        this.name = document.name;
        this.description = document.description;
        this.isAvailableAlways = document.isAvailableAlways;
        this.uploader = document.uploader.id;
        this.patient = document.patient.id;
    }
}
