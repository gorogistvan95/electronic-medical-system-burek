"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { DocumentEntity } from "../document.entity";

export class ListDocumentResDto {
    @ApiPropertyOptional()
    readonly id: string;

    @ApiPropertyOptional()
    readonly documentPath: string;

    @ApiPropertyOptional()
    readonly description: string;

    @ApiPropertyOptional()
    readonly isAvailableAlways: boolean;

    @ApiPropertyOptional()
    readonly uploader: string;

    @ApiPropertyOptional()
    readonly patient: string;

    @ApiPropertyOptional()
    readonly createdAt: Date;

    @ApiPropertyOptional()
    readonly updatedAt: Date;

    @ApiPropertyOptional()
    readonly mimeType: string;

    @ApiPropertyOptional()
    readonly size: string;

    @ApiPropertyOptional()
    readonly name: string;

    constructor(document: DocumentEntity) {
        this.id = document.id;
        this.name = document.name;
        this.description = document.description;
        this.isAvailableAlways = document.isAvailableAlways;
        this.uploader = document.uploaderId;
        this.patient = document.patientId;
        this.mimeType = document.mimeType;
        this.size = document.size;
        this.createdAt = document.createdAt;
        this.updatedAt = document.updatedAt;
    }
}
