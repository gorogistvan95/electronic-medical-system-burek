"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { DocumentEntity } from "../document.entity";
import { AuthEntity } from "../../../modules/auth/auth.entity";
import { PatientEntity } from "../../../modules/patient/patient.entity";

export class DocumentDto extends AbstractDto {
    @ApiPropertyOptional()
    name: string;

    @ApiPropertyOptional()
    documentPath: string;

    @ApiPropertyOptional()
    description?: string;

    @ApiPropertyOptional()
    mimeType: string;

    @ApiPropertyOptional()
    size: string;

    @ApiPropertyOptional()
    isAvailableAlways?: boolean;

    @ApiPropertyOptional()
    patient: PatientEntity;

    @ApiPropertyOptional()
    uploader: AuthEntity;

    constructor(document: DocumentEntity) {
        super(document);
        this.name = document.name;
        this.documentPath = document.documentPath;
        this.description = document.description;
        this.isAvailableAlways = document.isAvailableAlways;
        this.uploader = document.uploader;
        this.patient = document.patient;
        this.size = document.size;
        this.mimeType = document.mimeType;
    }
}
