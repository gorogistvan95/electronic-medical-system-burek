import {
    Column,
    Entity,
    CreateDateColumn,
    ManyToOne,
    OneToMany,
} from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { PatientEntity } from "../../modules/patient/patient.entity";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { DocumentDto } from "./dto/DocumentDto";
import { DocumentAvailableEntity } from "../documents-available/document-available.entity";

@Entity({ name: "document" })
export class DocumentEntity extends AbstractEntity<DocumentDto> {
    @Column()
    name: string;

    //vegenfalse
    @Column({ nullable: true })
    mimeType: string;
    //vegenfalse
    @Column({ nullable: true })
    size: string;

    @Column()
    documentPath: string;

    @Column({ type: "text" })
    description?: string;

    @Column({ default: false })
    isAvailableAlways?: boolean;

    //atirni falsera
    @Column({ name: "uploader_id", nullable: true })
    uploaderId: string;
    @ManyToOne((type) => AuthEntity, (auth) => auth.document, {
        nullable: true,
        onDelete: "SET NULL",
    })
    uploader: AuthEntity;

    //atirni falsera
    @Column({ name: "patient_id", nullable: true })
    patientId: string;
    @ManyToOne((type) => PatientEntity, (patient) => patient.document, {
        onDelete: "CASCADE",
    })
    patient: PatientEntity;

    @Column({ name: "user_id", nullable: true })
    userId: string;

    @ManyToOne((type) => AuthEntity, (auth) => auth.document, {
        nullable: true,
        onDelete: "SET NULL",
    })
    user: AuthEntity;

    @OneToMany(
        (type) => DocumentAvailableEntity,
        (documentAvalaible) => documentAvalaible.document
    )
    documentsAvailable: DocumentAvailableEntity[];

    dtoClass = DocumentDto;
}
