import {
    Controller,
    HttpCode,
    HttpStatus,
    UseGuards,
    UseInterceptors,
    Param,
    Delete,
    Put,
    Body,
    HttpException,
    Post,
    UploadedFile,
    Get,
    Header,
    Res,
    HttpService,
    ForbiddenException,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { FileInterceptor } from "@nestjs/platform-express/multer/interceptors/file.interceptor";
import { ApiTags } from "@nestjs/swagger";
import { multerOptions } from "../../interceptors/fileupload-interceptor.service";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { AuthService } from "../../modules/auth/auth.service";
import { PatientService } from "../../modules/patient/patient.service";
import { extname } from "path";
import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { DocumentService } from "./document.service";
import { CreateDocumentReqDto } from "./dto/CreateDocumentReqDto";
import { CreateDocumentResDto } from "./dto/CreateDocumentResDto";
import { ListDocumentResDto } from "./dto/ListDocumentResDto";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/document")
@ApiTags("api/document")
export class DocumentController {
    constructor(
        public readonly documentService: DocumentService,
        public readonly patientService: PatientService,
        private readonly httpService: HttpService
    ) {}
    @UseInterceptors(
        AuthUserInterceptor,
        FileInterceptor("file", multerOptions)
    )
    @UseGuards(AuthGuard("jwt"))
    @Post("document-upload/:id")
    @HttpCode(HttpStatus.OK)
    async upload(
        @Body() createDocumentReqDto: CreateDocumentReqDto,
        @Param("id") id,
        @UploadedFile() file
    ) {
        if (
            AuthService.getAuthUser().role != RoleType.USER &&
            AuthService.getAuthUser().role != RoleType.RECEPTIONIST
        ) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        if (!file) {
            throw new HttpException(
                "File wasnt attached",
                HttpStatus.BAD_REQUEST
            );
        }
        const documentName = file.filename;
        let patient = PatientService.getPatient();
        let fileSize: string = this.documentService.formatBytes(file.size);
        const uploadedDocumentResDto: CreateDocumentResDto = await this.documentService.uploadDocument(
            createDocumentReqDto,
            file.filename.toString(),
            extname(file.filename),
            fileSize
        );
        return uploadedDocumentResDto;
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete(":id/delete-document")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteDocument(@Param("id") documentId) {
        if (
            AuthService.getAuthUser().role != RoleType.USER &&
            AuthService.getAuthUser().role != RoleType.RECEPTIONIST
        ) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        await this.documentService.deleteDocument(documentId);
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get(":patientId/list-document")
    async listDocument(
        @Param("patientId") patientId
    ): Promise<ListDocumentResDto[]> {
        if (
            AuthService.getAuthUser().role != RoleType.USER &&
            AuthService.getAuthUser().role != RoleType.RECEPTIONIST
        ) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        const user: AuthEntity = AuthService.getAuthUser();
        const listDocumentsResDto: ListDocumentResDto[] = await this.documentService.listPatientEveryDocuments(
            patientId
        );
        return listDocumentsResDto;
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get(":patientId/list-document-by-doctor")
    async listPatientDocumentsByDoctor(
        @Param("patientId") patientId
    ): Promise<ListDocumentResDto[]> {
        if (
            AuthService.getAuthUser().role != RoleType.USER &&
            AuthService.getAuthUser().role != RoleType.RECEPTIONIST
        ) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        const listDocumentsResDto: ListDocumentResDto[] = await this.documentService.listPatientDocumentsByDoctor(
            patientId
        );
        return listDocumentsResDto;
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get(":patientId/list-always-available-documents")
    async listAlwaysAvailableDocuments(
        @Param("patientId") patientId
    ): Promise<ListDocumentResDto[]> {
        if (
            AuthService.getAuthUser().role != RoleType.USER ||
            AuthService.getAuthUser().role != RoleType.RECEPTIONIST
        ) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        const listDocumentsResDto: ListDocumentResDto[] = await this.documentService.listAlwaysAvailableDocuments(
            patientId
        );
        return listDocumentsResDto;
    }

    @Get(":documentId/document-download")
    async documentDownload(@Param("documentId") documentId, @Res() res) {
        await this.documentService.isDocumentAvailableToDownloader(documentId);
        const filePath = await this.documentService.getDocumentFilePath(
            documentId
        );
        res.download(filePath);
    }
}
