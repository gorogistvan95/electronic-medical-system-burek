import { forwardRef, Module } from "@nestjs/common";
import { PatientModule } from "../../modules/patient/patient.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { DocumentRepository } from "./document.repository";
import { DocumentService } from "./document.service";
import { AuthModule } from "../../modules/auth/auth.module";
import { DocumentController } from "./document.controller";

@Module({
    imports: [
        TypeOrmModule.forFeature([DocumentRepository]),
        PatientModule,
        AuthModule,
    ],
    controllers: [DocumentController],
    providers: [DocumentService],
    exports: [DocumentService, TypeOrmModule.forFeature([DocumentRepository])],
})
export class DocumentModule {}
