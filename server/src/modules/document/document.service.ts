import {
    ForbiddenException,
    Injectable,
    NotFoundException,
    UploadedFile,
} from "@nestjs/common";
import { AuthService } from "../../modules/auth/auth.service";
import { DocumentEntity } from "./document.entity";
import { DocumentRepository } from "./document.repository";
import { CreateDocumentReqDto } from "./dto/CreateDocumentReqDto";
import { CreateDocumentResDto } from "./dto/CreateDocumentResDto";
import { PatientService } from "../../modules/patient/patient.service";
import { PatientEntity } from "../../modules/patient/patient.entity";
import { ListDocumentResDto } from "./dto/ListDocumentResDto";
import { FindConditions, getRepository, ObjectType } from "typeorm";
import { AuthEntity } from "../../modules/auth/auth.entity";
import * as path from "path";
import * as fs from "fs";
import { multerConfig } from "../../interceptors/fileupload-interceptor.service";

@Injectable()
export class DocumentService {
    constructor(
        public readonly documentRepository: DocumentRepository,
        public readonly patientService: PatientService,
        public readonly authService: AuthService
    ) {}

    async uploadDocument(
        createDocumentReqDto: CreateDocumentReqDto,
        documentPath: string,
        mimeType: string,
        size: string
    ): Promise<CreateDocumentResDto> {
        const patientEntity: PatientEntity = await PatientService.getPatient();

        const user: AuthEntity = await this.authService.findOne({
            id: AuthService.getAuthUser().id,
        });
        if (!user) {
            throw new NotFoundException("Nincs ilyen doktor tetya");
        }
        let isAvailableAlwaysValue: boolean = false;
        if (createDocumentReqDto.isAvailableAlways) {
            const requestIsAvailable = await createDocumentReqDto.isAvailableAlways.toLocaleLowerCase();
            if (requestIsAvailable == "true") {
                isAvailableAlwaysValue = true;
            }
        }
        const document = await this.documentRepository.create({
            name: createDocumentReqDto.name,
            documentPath: documentPath,
            user: user,
            isAvailableAlways: isAvailableAlwaysValue,
            description: createDocumentReqDto.description,
            uploader: AuthService.getAuthUser(),
            patient: patientEntity,
            mimeType: mimeType,
            size: size,
        });

        const documentEntity: DocumentEntity = await this.documentRepository.save(
            document
        );
        return new CreateDocumentResDto(documentEntity);
    }

    async listPatientDocumentsByDoctor(
        patientId: string
    ): Promise<ListDocumentResDto[]> {
        const patientEntity: PatientEntity = await this.patientService.findOne({
            id: patientId,
        });
        const documentEntity: DocumentEntity[] = await this.documentRepository.find(
            { patient: patientEntity, uploader: AuthService.getAuthUser() }
        );
        try {
            const documentEntityQuery = await this.documentRepository
                .createQueryBuilder("appointment")
                .where(
                    "patient_id = :patientId AND (uploader_id = :auth OR is_Available_Always = :boolean )",
                    {
                        patientId: `${patientId}`,
                        auth: `${AuthService.getAuthUser().id}`,
                        boolean: true,
                    }
                )
                .getMany();
            const listDocumentResDto: ListDocumentResDto[] = documentEntityQuery.map(
                (documentEntityQuery) =>
                    new ListDocumentResDto(documentEntityQuery)
            );
            return listDocumentResDto;
        } catch (err) {
            console.log(err);
        }
    }
    async listAlwaysAvailableDocuments(patientId: string) {
        const patientEntity: PatientEntity = await this.patientService.findOne({
            id: patientId,
        });
        const documentEntity: DocumentEntity[] = await this.documentRepository.find(
            { patient: patientEntity, isAvailableAlways: true }
        );
        const listDocumentResDto: ListDocumentResDto[] = documentEntity.map(
            (documentEntity) => new ListDocumentResDto(documentEntity)
        );
        return listDocumentResDto;
    }

    async listPatientEveryDocuments(
        patientId: string
    ): Promise<ListDocumentResDto[]> {
        const patientEntity: PatientEntity = await this.patientService.findOne({
            id: patientId,
        });
        const documentEntity: DocumentEntity[] = await this.documentRepository.find(
            { patient: patientEntity }
        );
        const listDocumentResDto: ListDocumentResDto[] = documentEntity.map(
            (documentEntity) => new ListDocumentResDto(documentEntity)
        );
        return listDocumentResDto;
    }

    async deleteDocument(documentId: string) {
        const documentEntity: DocumentEntity = await this.findOne({
            id: documentId,
        });
        if (!documentEntity) {
            throw new NotFoundException(`Document ${documentId}  not found`);
        }
        await this.removeDocumentPhysically(documentEntity);
        await this.documentRepository.remove(documentEntity);
    }

    async removeDocumentPhysically(documentEntity: DocumentEntity) {
        const fs = require("fs");
        try {
            const patientTryerPath = path.resolve(
                __dirname,
                "../../",
                multerConfig.dest,
                documentEntity.patientId,
                documentEntity.documentPath
            );
            const defaultPath = this.getDocumentDefaultPath();
            const documentumPath = path.resolve(
                await this.getDocumentDefaultPath(),
                documentEntity.patientId,
                documentEntity.documentPath
            );
            fs.unlinkSync(documentumPath);
        } catch (err) {
            console.error(err);
        }
    }
    async getDocumentFilePath(documentId: string) {
        const document = await this.findOne({ id: documentId });
        if (!document) {
            throw new NotFoundException(
                `Document ${documentId}   Doesnt exist`
            );
        }
        const filePath = path.resolve(
            await this.getDocumentDefaultPath(),
            document.patientId,
            document.documentPath
        );
        try {
            const stats = fs.statSync(filePath);
        } catch (e) {
            throw new NotFoundException("File doesnt exist");
        }
        return filePath;
    }

    async getDocMimeType(documentId: string) {
        const document = await this.findOne({ id: documentId });
        if (!document) {
            throw new NotFoundException(
                `Document ${documentId}   Doesnt exist`
            );
        }
        const filePath = path.resolve(
            await this.getDocumentDefaultPath(),
            document.patientId,
            document.documentPath
        );
        return await require("file-type");
    }
    async findOne(
        findData: FindConditions<DocumentEntity>
    ): Promise<DocumentEntity> {
        return this.documentRepository.findOne(findData);
    }
    async getDocumentDefaultPath(): Promise<string> {
        const defaultPath = path.resolve(
            __dirname,
            "../../../",
            multerConfig.dest
        );
        return defaultPath;
    }

    async isDocumentAvailableToDownloader(documentId: string) {
        const documentEntity: DocumentEntity = await this.findOne({
            id: documentId,
        });
        if (!documentEntity) {
            throw new NotFoundException(`Document ${documentId}  not found`);
        }
        /*     if (
            documentEntity.uploaderId != AuthService.getAuthUser().id &&
            documentEntity.isAvailableAlways == false
        ) {
            throw new ForbiddenException(
                "Doesnt have permission to download this file"
            );
        }*/
    }
    formatBytes(bytes: number, decimals = 2) {
        if (bytes === 0) {
            return "0 Bytes";
        }

        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return (
            parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i]
        );
    }
}
