import {
    BadRequestException,
    ForbiddenException,
    Injectable,
    NotFoundException,
} from "@nestjs/common";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { PatientEntity } from "../../modules/patient/patient.entity";
import { AuthService } from "../../modules/auth/auth.service";
import { PatientService } from "../../modules/patient/patient.service";
import { CreateDiagnosisReqDto } from "./dto/CreateDiagnosisReqDto";
import { DiagnosisRepository } from "./diagnosis.repository";
import { DiagnosisEntity } from "./diagnosis.entity";
import { ListDiagnosisByPatientAndDoctorResDto } from "./dto/ListDiagnosisByPatientAndDoctorResDto";
import { UpdateDiagnosisReqDto } from "./dto/UpdateDiagnosisReqDto";
import { FindConditions } from "typeorm/find-options/FindConditions";
import * as path from "path";
import { ConfigService } from "../../shared/services/config.service";

@Injectable()
export class DiagnosisService {
    constructor(
        public readonly patientService: PatientService,
        public readonly authService: AuthService,
        public readonly diagnosisRepository: DiagnosisRepository,
        public readonly configService: ConfigService
    ) {}

    async createDiagnosis(createDiagnosisReqDto: CreateDiagnosisReqDto) {
        const patientEntity: PatientEntity = await this.patientService.findOne({
            id: createDiagnosisReqDto.patientId,
        });
        if (!patientEntity) {
            throw new NotFoundException("Patient could not found");
        }
        const uploader: AuthEntity = await this.authService.findOne({
            id: AuthService.getAuthUser().id,
        });
        if (!uploader) {
            throw new NotFoundException("Doctor doesnt exist");
        }
        if (!uploader.isDiagnosis) {
            throw new ForbiddenException(
                "Have no permisson to write diagnosis"
            );
        }

        const diagnosis = await this.diagnosisRepository.create({
            ...createDiagnosisReqDto,
            uploader,
        });

        const diagnosisEntity: DiagnosisEntity = await this.diagnosisRepository.save(
            diagnosis
        );
        if (!diagnosisEntity) {
            throw new BadRequestException(
                "The diagnosis cant be saved into db"
            );
        }
    }

    async listDiagnosistByDoctorAndPatient(
        patientId: string
    ): Promise<ListDiagnosisByPatientAndDoctorResDto[]> {
        const patientEntity: PatientEntity = await this.patientService.findOne({
            id: patientId,
        });

        const uploaderEntity: AuthEntity = await this.authService.findOne({
            id: AuthService.getAuthUser().id,
        });
        if (!uploaderEntity) {
            throw new NotFoundException("Doctor doesnt exist");
        }
        if (!uploaderEntity.isDiagnosis) {
            throw new ForbiddenException(
                "Have no permisson to write diagnosis"
            );
        }

        const diagnosisEntities: DiagnosisEntity[] = await this.diagnosisRepository.find(
            { patient: patientEntity, uploader: uploaderEntity }
        );
        return diagnosisEntities.map(
            (diagnosisEntities) =>
                new ListDiagnosisByPatientAndDoctorResDto(diagnosisEntities)
        );
    }

    async deleteDiagnosisById(diagnosisId: string) {
        const uploaderEntity: AuthEntity = await this.authService.findOne({
            id: AuthService.getAuthUser().id,
        });
        if (!uploaderEntity) {
            throw new NotFoundException("Doctor doesnt exist");
        }
        if (!uploaderEntity.isDiagnosis) {
            throw new ForbiddenException(
                "Have no permisson to write diagnosis"
            );
        }

        const diagnosisEntity: DiagnosisEntity = await this.diagnosisRepository.findOne(
            {
                id: diagnosisId,
            }
        );
        if (!diagnosisEntity) {
            throw new NotFoundException("Diagnosis entity cannot found");
        }
        if (!(await this.diagnosisRepository.remove(diagnosisEntity))) {
            throw new NotFoundException("cannot remove this diagnosis");
        }
    }

    async updateDiagnosis(
        diagnosisId: string,
        updateDiagnosisReqDto: UpdateDiagnosisReqDto
    ) {
        const diagnosisEntity: DiagnosisEntity = await this.findOne({
            id: diagnosisId,
        });
        if (!diagnosisEntity) {
            throw new NotFoundException(`Diagnosis: ${diagnosisId} not found`);
        }
        const updatedDiagnosis: DiagnosisEntity = await this.diagnosisRepository.create(
            updateDiagnosisReqDto
        );
        await this.diagnosisRepository.save({
            ...diagnosisEntity,
            ...updatedDiagnosis,
        });
    }
    async listDiagnosisBno() {
        const defaultPath = path.resolve(
            __dirname,
            "../../../",
            this.configService.get("CS_FILE_NAME") + ".csv"
        );
        const csv = require("csvtojson");
        try {
            const jsonArray = await csv({
                delimiter: ",",
            }).fromFile(defaultPath);
            return jsonArray;
        } catch (err) {
            console.log(err);
        }
    }
    async findOne(
        findData: FindConditions<DiagnosisEntity>
    ): Promise<DiagnosisEntity> {
        return this.diagnosisRepository.findOne(findData);
    }
}
