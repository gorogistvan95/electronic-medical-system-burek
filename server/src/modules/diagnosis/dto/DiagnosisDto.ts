"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { AuthEntity } from "../../../modules/auth/auth.entity";
import { PatientEntity } from "../../../modules/patient/patient.entity";
import { DiagnosisEntity } from "../diagnosis.entity";

export class DiagnosisDto extends AbstractDto {
    @ApiPropertyOptional()
    code: string;

    @ApiPropertyOptional()
    name: string;

    @ApiPropertyOptional()
    description: string;

    @ApiPropertyOptional()
    treatment?: string;

    @ApiPropertyOptional()
    patient: PatientEntity;

    @ApiPropertyOptional()
    uploaderDoctor: AuthEntity;

    @ApiPropertyOptional()
    uploaderDoctorId: string;

    constructor(diagnosis: DiagnosisEntity) {
        super(diagnosis);
        this.name = diagnosis.name;
        this.description = diagnosis.description;
        this.treatment = diagnosis.description;
        this.uploaderDoctor = diagnosis.uploader;
        this.uploaderDoctorId = diagnosis.uploaderId;
        this.patient = diagnosis.patient;
    }
}
