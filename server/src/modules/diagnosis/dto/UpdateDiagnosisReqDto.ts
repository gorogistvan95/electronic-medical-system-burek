"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsOptional, IsString } from "class-validator";

export class UpdateDiagnosisReqDto {
    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly description?: string;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly treatment?: string;
}
