"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsNumber, IsOptional, IsString } from "class-validator";

export class CreateDiagnosisReqDto {
    @ApiPropertyOptional()
    @IsString()
    patientId: string;

    @ApiPropertyOptional()
    @IsString()
    code: string;

    @ApiPropertyOptional()
    @IsString()
    name: string;

    @IsOptional()
    @ApiPropertyOptional()
    @IsString()
    description: string;

    @ApiPropertyOptional()
    @IsOptional()
    @IsString()
    treatment?: string;
}
