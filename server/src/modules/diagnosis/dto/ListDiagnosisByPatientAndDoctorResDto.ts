"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { DiagnosisEntity } from "../diagnosis.entity";

export class ListDiagnosisByPatientAndDoctorResDto {
    @ApiPropertyOptional()
    readonly id: string;

    @ApiPropertyOptional()
    readonly code: string;

    @ApiPropertyOptional()
    readonly name: string;

    @ApiPropertyOptional()
    readonly description: string;

    @ApiPropertyOptional()
    readonly treatment?: string;

    @ApiPropertyOptional()
    readonly createdAt: Date;

    @ApiPropertyOptional()
    readonly updatedAt: Date;

    constructor(diagnosis: DiagnosisEntity) {
        this.id = diagnosis.id;
        this.code = diagnosis.code;
        this.name = diagnosis.name;
        this.description = diagnosis.description;
        this.treatment = diagnosis.treatment;
        this.createdAt = diagnosis.createdAt;
        this.updatedAt = diagnosis.updatedAt;
    }
}
