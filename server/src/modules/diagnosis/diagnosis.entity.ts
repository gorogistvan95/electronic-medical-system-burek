import {
    Column,
    Entity,
    ManyToOne,
} from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { AuthEntity } from "../auth/auth.entity";
import { PatientEntity } from "../patient/patient.entity";
import { DiagnosisDto } from "./dto/DiagnosisDto";

@Entity({ name: "diagnosis" })
export class DiagnosisEntity extends AbstractEntity<DiagnosisDto> {
    @Column()
    code: string;

    @Column()
    name: string;

    @Column({ type: "text" })
    description: string;

    @Column({ type: "text" })
    treatment: string;

    @Column({ name: "uploader_id", nullable: true })
    uploaderId: string;
    @ManyToOne((type) => AuthEntity, (auth) => auth.diagnosis, {
        onDelete: "SET NULL",
    })
    uploader: AuthEntity;

    //atirni falsera
    @Column({ name: "patient_id", nullable: true })
    patientId: string;
    @ManyToOne((type) => PatientEntity, (patient) => patient.document, {
        onDelete: "CASCADE",
    })
    patient: PatientEntity;

    dtoClass = DiagnosisDto;
}
