import {
    Controller,
    HttpCode,
    HttpStatus,
    UseGuards,
    UseInterceptors,
    Param,
    Delete,
    Put,
    Body,
    HttpException,
    Post,
    Get,
    HttpService,
    ForbiddenException,
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiTags } from "@nestjs/swagger";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { AuthService } from "../../modules/auth/auth.service";
import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { DiagnosisService } from "./diagnosis.service";
import { CreateDiagnosisReqDto } from "./dto/CreateDiagnosisReqDto";
import { UpdateDiagnosisReqDto } from "./dto/UpdateDiagnosisReqDto";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/diagnosis")
@ApiTags("api/diagnosis")
export class DiagnosisController {
    constructor(
        public readonly diagnosisService: DiagnosisService,
        private readonly httpService: HttpService
    ) {}

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Post("create-diagnosis")
    @HttpCode(HttpStatus.CREATED)
    async createDiagnosis(
        @Body() createDiagnosisReqDto: CreateDiagnosisReqDto
    ) {
        const user: AuthEntity = AuthService.getAuthUser();
        await this.diagnosisService.createDiagnosis(createDiagnosisReqDto);
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get(":patientId/list-diagnosis")
    @HttpCode(HttpStatus.CREATED)
    async listDiagnosisByDoctorAndPatient(@Param("patientId") patientId) {
        if (AuthService.getAuthUser().role != RoleType.USER) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        return await this.diagnosisService.listDiagnosistByDoctorAndPatient(
            patientId
        );
    }

    //  @UseInterceptors(AuthUserInterceptor)
    //  @UseGuards(AuthGuard("jwt"))
    @Get("list-diagnosis-bno")
    @HttpCode(HttpStatus.CREATED)
    async listDiagnosisBno() {
        /*     if (AuthService.getAuthUser().role != RoleType.USER) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }*/
        return await this.diagnosisService.listDiagnosisBno();
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete(":diagnosisId/delete-diagnosis")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteDiagnosis(@Param("diagnosisId") diagnosisId) {
        if (AuthService.getAuthUser().role != RoleType.USER) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        await this.diagnosisService.deleteDiagnosisById(diagnosisId);
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Put(":id/update-diagnosis")
    @HttpCode(HttpStatus.NO_CONTENT)
    async updateDiagnosis(
        @Param("id") diagnosisId,
        @Body() updateDiagnosisReqDto: UpdateDiagnosisReqDto
    ) {
        try {
            if (AuthService.getAuthUser().role != RoleType.USER) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            await this.diagnosisService.updateDiagnosis(
                diagnosisId,
                updateDiagnosisReqDto
            );
            return;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
}