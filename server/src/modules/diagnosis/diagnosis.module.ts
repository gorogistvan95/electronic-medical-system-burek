import { forwardRef, Module } from "@nestjs/common";
import { PatientModule } from "../../modules/patient/patient.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "../../modules/auth/auth.module";
import { DiagnosisRepository } from "./diagnosis.repository";
import { DiagnosisService } from "./diagnosis.service";
import { DiagnosisController } from "./diagnosis.controller";

@Module({
    imports: [
        TypeOrmModule.forFeature([DiagnosisRepository]),
        forwardRef(() => PatientModule),
        AuthModule,
    ],
    controllers: [DiagnosisController],
    providers: [DiagnosisService],
    exports: [
        DiagnosisService,
        TypeOrmModule.forFeature([DiagnosisRepository]),
    ],
})
export class DiagnosisModule {}
