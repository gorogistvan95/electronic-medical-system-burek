import { Repository } from "typeorm";
import { EntityRepository } from "typeorm/decorator/EntityRepository";
import { DiagnosisEntity } from "./diagnosis.entity";

@EntityRepository(DiagnosisEntity)
export class DiagnosisRepository extends Repository<DiagnosisEntity> {}
