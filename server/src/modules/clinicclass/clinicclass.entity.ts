import { ExaminationEntity } from "../../modules/examination/examination.entity";
import {
    Column,
    Entity,
    CreateDateColumn,
    ManyToOne,
    OneToMany,
} from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { ClinicClassDto } from "./dto/ClinicClassDto";
import { ReferralEntity } from "../../modules/referral/referral.entity";
import { AuthEntity } from "../../modules/auth/auth.entity";

@Entity({ name: "clinicClass" })
export class ClinicClassEntity extends AbstractEntity<ClinicClassDto> {
    @Column({ unique: true })
    name: string;

    @Column({ default: false })
    onlyOneUser: boolean;

    @OneToMany(
        (type) => ExaminationEntity,
        (examination) => examination.clinicClass,
        {}
    )
    examination: ExaminationEntity[];

    @OneToMany((type) => AuthEntity, (doctor) => doctor.clinicClass, {
        onDelete: "CASCADE",
    })
    doctor: AuthEntity[];

    dtoClass = ClinicClassDto;
}
