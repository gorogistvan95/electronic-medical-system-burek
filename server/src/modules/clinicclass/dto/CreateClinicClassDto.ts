"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsString } from "class-validator";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { ClinicClassEntity } from "../clinicclass.entity";



export class CreateClinicClassDto {

    @IsString()
    @ApiPropertyOptional()
    readonly name: string;


    @IsBoolean()
    @ApiPropertyOptional()
    readonly onlyOneUser: boolean;

}
