"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";

import { IsString } from "class-validator";
import { ClinicClassEntity } from "../clinicclass.entity";

export class ListClinicClassResponseDto {

    @ApiPropertyOptional()
    id: string;

    @ApiPropertyOptional()
    name: string;

    @ApiPropertyOptional()
    onlyOneUser: boolean;

    constructor(clinicClass: ClinicClassEntity) {
        this.id = clinicClass.id;
        this.name = clinicClass.name;
        this.onlyOneUser = clinicClass.onlyOneUser;
    }

}