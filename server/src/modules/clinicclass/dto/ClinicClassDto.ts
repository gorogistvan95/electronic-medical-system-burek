"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { ClinicClassEntity } from "../clinicclass.entity";



export class ClinicClassDto extends AbstractDto {
    @ApiPropertyOptional()
    name: string;

    @ApiPropertyOptional()
    onlyOneUser?: boolean;

    constructor(clinicClass: ClinicClassEntity) {
        super(clinicClass);
        this.name = clinicClass.name;
        this.onlyOneUser = clinicClass.onlyOneUser;
    }
}
