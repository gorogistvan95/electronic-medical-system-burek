"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsBoolean, IsOptional, IsString } from "class-validator";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { ClinicClassEntity } from "../clinicclass.entity";

export class UpdateClinicClassReqDto {
    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly name: string;

    @IsOptional()
    @IsBoolean()
    @ApiPropertyOptional()
    readonly onlyOneUser: boolean;
}
