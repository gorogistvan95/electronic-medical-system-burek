import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthRepository } from "../../modules/auth/auth.repository";
import { ExaminationRepository } from "../../modules/examination/examination.repository";
import { ClinicClassController } from "./clinicclass.controller";
import { ClinicClassRepository } from "./clinicclass.repository";
import { ClinicClassService } from "./clinicclass.service";
import { PatientModule } from "../../modules/patient/patient.module";

@Module({
    imports: [
        PatientModule,
        TypeOrmModule.forFeature([ClinicClassRepository]),
        TypeOrmModule.forFeature([AuthRepository]),
        TypeOrmModule.forFeature([ExaminationRepository]),
    ],
    controllers: [ClinicClassController],
    providers: [ClinicClassService],
    exports: [
        ClinicClassService,
        TypeOrmModule.forFeature([ClinicClassRepository]),
    ],
})
export class ClinicClassModule {}
