import { Repository } from "typeorm";
import { EntityRepository } from "typeorm/decorator/EntityRepository";
import { ClinicClassEntity } from "./clinicclass.entity";


@EntityRepository(ClinicClassEntity)
export class ClinicClassRepository extends Repository<ClinicClassEntity> { }
