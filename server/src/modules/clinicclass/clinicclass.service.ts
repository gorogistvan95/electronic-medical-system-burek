import {
    Injectable,
    HttpException,
    HttpStatus,
    NotFoundException,
} from "@nestjs/common";
import { FindConditions } from "typeorm/find-options/FindConditions";
import { ClinicClassEntity } from "./clinicclass.entity";
import { ClinicClassRepository } from "./clinicclass.repository";
import { CreateClinicClassDto } from "./dto/CreateClinicClassDto";
import { ListClinicClassResponseDto } from "./dto/ListClinicClassDto";
import { UpdateClinicClassReqDto } from "./dto/UpdateClinicClassReqDto";

@Injectable()
export class ClinicClassService {
    constructor(public readonly clinicClassRepository: ClinicClassRepository) {}

    async createPatient(
        createClinicClassDto: CreateClinicClassDto
    ): Promise<ClinicClassEntity> {
        const clinicClass = await this.clinicClassRepository.create({
            ...createClinicClassDto,
        });

        return await this.clinicClassRepository.save(clinicClass);
    }

    async findOne(
        findData: FindConditions<ClinicClassEntity>
    ): Promise<ClinicClassEntity> {
        return this.clinicClassRepository.findOne(findData);
    }

    async findAll(): Promise<ClinicClassEntity[]> {
        return await this.clinicClassRepository.find({});
    }

    async listClinicClass(): Promise<ListClinicClassResponseDto[]> {
        const clinicClassEntity: ClinicClassEntity[] = await this.findAll();
        const clinicClassDto: ListClinicClassResponseDto[] = clinicClassEntity.map(
            (clinicClassEntity) =>
                new ListClinicClassResponseDto(clinicClassEntity)
        );
        return clinicClassDto;
    }

    async deleteClinicClass(clinicClassId: string) {
        const clinicClassEntity: ClinicClassEntity = await this.findOne({
            id: clinicClassId,
        });
        if (!clinicClassEntity) {
            throw new NotFoundException(
                `Clinic class ${clinicClassId} not found`
            );
        }
        await this.clinicClassRepository.remove(clinicClassEntity);
    }

    async updateClinicClass(
        clinicClassId: string,
        updateClinicClassReqDto: UpdateClinicClassReqDto
    ) {
        const clinicClassEntity: ClinicClassEntity = await this.findOne({
            id: clinicClassId,
        });
        if (!clinicClassEntity) {
            throw new NotFoundException(
                `Clinic class : ${clinicClassId} not found`
            );
        }
        const updateClinicClass: ClinicClassEntity = await this.clinicClassRepository.create(
            updateClinicClassReqDto
        );
        await this.clinicClassRepository.save({
            ...clinicClassEntity,
            ...updateClinicClass,
        });
    }
}
