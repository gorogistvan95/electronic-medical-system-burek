import {
    Controller,
    HttpCode,
    HttpStatus,
    UseGuards,
    UseInterceptors,
    Param,
    Delete,
    Put,
    Body,
    HttpException,
    Post,
    Get,
    ForbiddenException,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "../../modules/auth/auth.service";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { ClinicClassService } from "../../modules/clinicclass/clinicclass.service";
import { UpdateClinicClassReqDto } from "./dto/UpdateClinicClassReqDto";
import { CreateClinicClassDto } from "./dto/CreateClinicClassDto";
import { ListClinicClassResponseDto } from "./dto/ListClinicClassDto";
import { CoreDto } from "../../common/dto/CoreDto";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/clinic-class")
@ApiTags("api/clinic-class")
export class ClinicClassController {
    constructor(public readonly clinicClassService: ClinicClassService) {}

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete(":id/delete-clinic-class")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deleteClinicClass(@Param("id") clinicClassId) {
        if (AuthService.getAuthUser().role != RoleType.ADMIN) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        await this.clinicClassService.deleteClinicClass(clinicClassId);
        return;
    }

    @UseGuards(AuthGuard("jwt"))
    @UseInterceptors(AuthUserInterceptor)
    @Put(":id/update-clinic-class")
    @HttpCode(HttpStatus.NO_CONTENT)
    async updateClinicClassById(
        @Param("id") clinicClassId,
        @Body() updateClinicClassReqDto: UpdateClinicClassReqDto
    ) {
        try {
            if (AuthService.getAuthUser().role != RoleType.ADMIN) {
                throw new ForbiddenException(
                    "Have no permission! You have no access rights"
                );
            }
            await this.clinicClassService.updateClinicClass(
                clinicClassId,
                updateClinicClassReqDto
            );
            return;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Post("createclinicclass")
    @HttpCode(HttpStatus.OK)
    async createClinicClass(
        @Body() createClinicClassDto: CreateClinicClassDto
    ): Promise<CoreDto> {
        if (AuthService.getAuthUser().role != RoleType.ADMIN) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        const createdClinicClass: CoreDto = await (
            await this.clinicClassService.createPatient(createClinicClassDto)
        ).toCoreDto();
        return createdClinicClass;
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("listclinicclass")
    @HttpCode(HttpStatus.OK)
    async listClinicClass(): Promise<ListClinicClassResponseDto[]> {
        try {
            if (
                AuthService.getAuthUser().role != RoleType.ADMIN &&
                AuthService.getAuthUser().role != RoleType.RECEPTIONIST
            ) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const listClinicClass: ListClinicClassResponseDto[] = await this.clinicClassService.listClinicClass();
            return listClinicClass;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
}
