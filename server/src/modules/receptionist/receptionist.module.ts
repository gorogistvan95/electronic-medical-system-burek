import { forwardRef, Module } from "@nestjs/common";
import { ReceptionistController } from "./receptionist.controller";
import { PatientModule } from "../../modules/patient/patient.module";
import { ReceptionistService } from "./receptionist.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthRepository } from "../../modules/auth/auth.repository";
import { ExaminationModule } from "../../modules/examination/examination.module";
import { ClinicClassModule } from "../../modules/clinicclass/clinicclass.module";
import { ReferralModule } from "../../modules/referral/referral.module";
import { TimeTableModule } from "../../modules/timetable/timetable.module";
import { DocumentModule } from "../../modules/document/document.module";

@Module({
    imports: [
        TypeOrmModule.forFeature([AuthRepository]),
        PatientModule,
        ExaminationModule,
        ClinicClassModule,
        ReferralModule,
        TimeTableModule,
        DocumentModule,
    ],
    controllers: [ReceptionistController],
    providers: [ReceptionistService],
    exports: [],
})
export class ReceptonistModule {}
