import { Injectable } from "@nestjs/common";
import { CreatePatientDto } from "../../modules/patient/dto/CreatePatientDto";
import { CoreDto } from "common/dto/CoreDto";
import { PatientService } from "../../modules/patient/patient.service";
import { PatientEntity } from "../../modules/patient/patient.entity";

@Injectable()
export class ReceptionistService {
    constructor(public readonly patientService: PatientService) {}

    async CreatePatient(createPatientDto: CreatePatientDto): Promise<CoreDto> {
        //Check EXIST USER
        const createdPatient: PatientEntity = await this.patientService.createPatient(
            createPatientDto
        );

        return createdPatient.toCoreDto();
    }
}
