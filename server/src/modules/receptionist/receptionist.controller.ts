import {
    Body,
    Controller,
    HttpCode,
    HttpStatus,
    Post,
    UploadedFile,
    UseGuards,
    UseInterceptors,
    HttpException,
    Get,
    Param,
    Delete,
    ForbiddenException,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { ReceptionistService } from "./receptionist.service";
import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "../../modules/auth/auth.service";
import { ListExaminationResponseDto } from "../../modules/examination/dto/ListExaminationResponseDto";
import { ExaminationService } from "../../modules/examination/examination.service";
import { ListClinicClassResponseDto } from "../../modules/clinicclass/dto/ListClinicClassDto";
import { ClinicClassService } from "../../modules/clinicclass/clinicclass.service";
import { ReferralService } from "../../modules/referral/referral.service";
import { TimeTableService } from "../../modules/timetable/timetable.service";
import { DocumentService } from "../../modules/document/document.service";
import { ConfigService } from "../../shared/services/config.service";
import { PatientService } from "../../modules/patient/patient.service";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/receptionist")
@ApiTags("api/receptionist")
export class ReceptionistController {
    constructor(
        public readonly serviceReceptionist: ReceptionistService,
        public readonly examinationService: ExaminationService,
        public readonly clinicClassService: ClinicClassService,
        public readonly referralService: ReferralService,
        public readonly timeTableService: TimeTableService,
        public readonly documentService: DocumentService,
        public readonly patientService: PatientService,
        public readonly configService: ConfigService
    ) {}

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("list-examination/:id")
    @HttpCode(HttpStatus.OK)
    async ListExamination(
        @Param("id") id
    ): Promise<ListExaminationResponseDto[]> {
        if (
            AuthService.getAuthUser().role != RoleType.ADMIN &&
            AuthService.getAuthUser().role != RoleType.RECEPTIONIST
        ) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        const listExamination: ListExaminationResponseDto[] = await this.examinationService.listExamination(
            id
        );
        return listExamination;
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("listclinicclass")
    @HttpCode(HttpStatus.OK)
    async ListClinicClass(): Promise<ListClinicClassResponseDto[]> {
        try {
            if (
                AuthService.getAuthUser().role != RoleType.ADMIN &&
                AuthService.getAuthUser().role != RoleType.RECEPTIONIST
            ) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const listClinicClass: ListClinicClassResponseDto[] = await this.clinicClassService.listClinicClass();
            return listClinicClass;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
}
