import { ReferralEntity } from "../../modules/referral/referral.entity";
import {
    Column,
    Entity,
    CreateDateColumn,
    ManyToOne,
    OneToMany,
} from "typeorm";
import { AbstractEntity } from "../../common/abstract.entity";
import { PatientDto } from "./dto/PatientDto";
import { DocumentEntity } from "../../modules/document/document.entity";
import { DiagnosisEntity } from "../../modules/diagnosis/diagnosis.entity";

@Entity({ name: "patient" })
export class PatientEntity extends AbstractEntity<PatientDto> {
    @Column()
    name: string;

    @Column()
    gender: string;

    @Column()
    address: string;

    @Column()
    idCardNumber: string;

    @Column({ unique: true })
    tajNumber: string;

    @OneToMany((type) => ReferralEntity, (referral) => referral.patient, {
        onDelete: "CASCADE",
    })
    referral: ReferralEntity[];
    @OneToMany((type) => DocumentEntity, (document) => document.patient, {
        onDelete: "CASCADE",
    })
    document: DocumentEntity[];

    @OneToMany((type) => DiagnosisEntity, (diagnosis) => diagnosis.patient, {
        onDelete: "CASCADE",
    })
    diagnosises: DiagnosisEntity[];

    dtoClass = PatientDto;
}
