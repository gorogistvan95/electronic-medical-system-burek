import {
    Injectable,
    HttpException,
    HttpStatus,
    NotFoundException,
} from "@nestjs/common";
import { CreatePatientDto } from "./dto/CreatePatientDto";
import { PatientEntity } from "./patient.entity";
import { PatientRepository } from "./patient.repository";
import { createQueryBuilder, FindConditions, getRepository } from "typeorm";
import { ContextService } from "../../providers/context.service";
import { UpdatePatientReqDto } from "./dto/UpdatePatientReqDto";
import { ListPatientResponseDto } from "./dto/ListPatientResponseDto";
import { multerConfig } from "../../interceptors/fileupload-interceptor.service";
import * as path from "path";
import * as fs from "fs";
import { AuthService } from "../../modules/auth/auth.service";
import { ReferralRepository } from "../../modules/referral/referral.repository";
import { PatientDto } from "./dto/PatientDto";
import { ExaminationEntity } from "../../modules/examination/examination.entity";
import { ExaminationRepository } from "../../modules/examination/examination.repository";

@Injectable()
export class PatientService {
    private static _patientKey = "patient_key";
    constructor(
        public readonly patientRepository: PatientRepository,
        public readonly referralRepository: ReferralRepository,
        public readonly examinationRepository: ExaminationRepository
    ) {}

    async createPatient(
        createPatientDto: CreatePatientDto
    ): Promise<PatientEntity> {
        if (
            await this.patientRepository.findOne({
                tajNumber: createPatientDto.tajNumber,
            })
        ) {
            throw new HttpException("User already exist", HttpStatus.CONFLICT);
        }
        const patient = await this.patientRepository.create({
            ...createPatientDto,
        });

        const createdPatient: PatientEntity = await this.patientRepository.save(
            patient
        );
        if (!createdPatient) {
            throw new HttpException(
                "Database connection error",
                HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
        await this.createPatientFolder(createdPatient.id);
        return createdPatient;
    }
    async createPatientFolder(patientFolderName: string) {
        const patientFolderPath = path.resolve(
            __dirname,
            "../../../",
            multerConfig.dest,
            patientFolderName
        );
        !fs.existsSync(patientFolderPath) && fs.mkdirSync(patientFolderPath);
    }
    async deletePatientFolder(patientFolderName: string) {
        const patientFolderPath = path.resolve(
            __dirname,
            "../../../",
            multerConfig.dest,
            patientFolderName
        );
        await fs.rmdirSync(patientFolderPath, { recursive: true });
    }

    async listAllPatient(): Promise<ListPatientResponseDto[]> {
        const patientEntity: PatientEntity[] = await this.findAll();
        return patientEntity.map(
            (patientEntity) => new ListPatientResponseDto(patientEntity)
        );
    }

    async deletePatient(patientId: string) {
        const patientEntity: PatientEntity = await this.findOne({
            id: patientId,
        });
        if (!patientEntity) {
            throw new NotFoundException(`Patient ${patientId} not found`);
        }

        if (await this.patientRepository.remove(patientEntity)) {
            await this.deletePatientFolder(patientId);
        }
    }

    async updatePatient(
        patientId: string,
        updatePatientReqDto: UpdatePatientReqDto
    ) {
        const patientEntity: PatientEntity = await this.findOne({
            id: patientId,
        });
        if (!patientEntity) {
            throw new NotFoundException(`Patient : ${patientId} not found`);
        }
        const updatePatient: PatientEntity = await this.patientRepository.create(
            updatePatientReqDto
        );
        const updatedEntity = await this.patientRepository.save({
            ...patientEntity,
            ...updatePatient,
        });
    }

    async findOne(
        findData: FindConditions<PatientEntity>
    ): Promise<PatientEntity> {
        return this.patientRepository.findOne(findData);
    }

    async findAll(): Promise<PatientEntity[]> {
        return await this.patientRepository.find({});
    }

    static setPatient(patient: PatientEntity) {
        ContextService.set(PatientService._patientKey, patient);
    }

    static getPatient(): PatientEntity {
        return ContextService.get(PatientService._patientKey);
    }

    async getPatientByTajNumber(tajNumber: string) {
        return await this.patientRepository.findOne({ tajNumber });
    }

    async isNumber(value) {
        return typeof value === "number" && isFinite(value);
    }

    async listPatientandRelatedObjectsByDoctor() {
        try {
            const authenticatedUserId = AuthService.getAuthUser().id;
            const result = await getRepository(PatientEntity)
                .createQueryBuilder("patient")
                .leftJoinAndSelect(
                    "patient.referral",
                    "referral",
                    "referral.patient_id = patient_id"
                )
                .leftJoinAndSelect(
                    "referral.timeTable",
                    "timeTable",
                    "referral_id = timeTable.referral_id "
                )
                .where("timeTable.doctor_id = :uploaderId", {
                    uploaderId: authenticatedUserId,
                })
                .getMany();

            const burek = await this.mappingResult(result);
            return burek;

            let responseArray = [];
            result.forEach(async (element) => {
                let patientAndRelatedObject = {
                    patient: element.toDto(),
                    referral: [],
                };
                for (let referral in element[`referral`]) {
                    if ((await this.isNumber(parseInt(referral))) == true) {
                        const referralObject = {
                            referral: element["referral"][referral].toDto(),
                            timetable: element["referral"][referral][
                                `timeTable`
                            ].toDtos(),
                        };
                        patientAndRelatedObject.referral.push(referralObject);
                    }
                }
                responseArray.push(patientAndRelatedObject);
            });
            return responseArray;
        } catch (err) {
            console.log(err);
        }
    }
    async mappingResult(patientEntites: PatientEntity[]) {
        const promises = patientEntites.map(async (patientEntity) => {
            const patient = new PatientDto(patientEntity);
            const referral = patientEntity["referral"].map(async (referral) => {
                const examination: ExaminationEntity = await this.examinationRepository.findOne(
                    { id: referral.examinationId }
                );
                const referralObject = {
                    referral: referral.toDto(),
                    timetable: referral[`timeTable`].toDtos(),
                    examinationName: examination.name,
                };
                return referralObject;
            });
            return { patient: patient, referral: await Promise.all(referral) };
        });
        return await Promise.all(promises);
    }
}
