import {
    Controller,
    HttpCode,
    HttpStatus,
    UseGuards,
    UseInterceptors,
    Param,
    Delete,
    Put,
    Body,
    HttpException,
    Post,
    Get,
    ForbiddenException,
} from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { AuthUserInterceptor } from "../../interceptors/auth-user-interceptor.service";
import { AuthGuard } from "@nestjs/passport";
import { AuthService } from "../../modules/auth/auth.service";
import { AuthEntity } from "../../modules/auth/auth.entity";
import { PatientService } from "./patient.service";
import { UpdatePatientReqDto } from "./dto/UpdatePatientReqDto";
import { CreatePatientDto } from "./dto/CreatePatientDto";
import { CoreDto } from "../../common/dto/CoreDto";
import { ListPatientResponseDto } from "./dto/ListPatientResponseDto";
import { RoleType } from "../../common/constants/role-type";

@Controller("api/patient")
@ApiTags("api/patient")
export class PatientController {
    constructor(public readonly patientService: PatientService) {}

    @UseGuards(AuthGuard("jwt"))
    @UseInterceptors(AuthUserInterceptor)
    @Post("register")
    @HttpCode(HttpStatus.OK)
    async createPatient(@Body() createPatientDto: CreatePatientDto) {
        try {
            const createdPatient = await this.patientService.createPatient(
                createPatientDto
            );
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Delete(":id/delete-patient")
    @HttpCode(HttpStatus.NO_CONTENT)
    async deletePatient(@Param("id") patientId) {
        if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
            throw new ForbiddenException(
                "Have no permission! You have no acces rights"
            );
        }
        await this.patientService.deletePatient(patientId);
        return;
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Put(":id/update-patient")
    @HttpCode(HttpStatus.NO_CONTENT)
    async updatePatient(
        @Param("id") patientId,
        @Body() updatePatientReqDto: UpdatePatientReqDto
    ) {
        try {
            if (
                AuthService.getAuthUser().role != RoleType.USER &&
                AuthService.getAuthUser().role != RoleType.RECEPTIONIST
            ) {
                throw new ForbiddenException(
                    "Have no permission! You have no access rights"
                );
            }
            await this.patientService.updatePatient(
                patientId,
                updatePatientReqDto
            );
            return;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("listallpatient")
    @HttpCode(HttpStatus.OK)
    async listPatient(): Promise<ListPatientResponseDto[]> {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const allPatient: ListPatientResponseDto[] = await this.patientService.listAllPatient();

            return allPatient;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("taj/:tajNumber")
    @HttpCode(HttpStatus.OK)
    async getPatientByTAJ(@Param("tajNumber") tajNumber) {
        try {
            if (AuthService.getAuthUser().role != RoleType.RECEPTIONIST) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const patient = (
                await this.patientService.getPatientByTajNumber(tajNumber)
            ).toDto();
            if (!patient) {
                throw new HttpException(undefined, HttpStatus.NOT_FOUND);
            }
            return patient;
        } catch (e) {
            throw new HttpException(e, HttpStatus.BAD_REQUEST);
        }
    }

    @UseInterceptors(AuthUserInterceptor)
    @UseGuards(AuthGuard("jwt"))
    @Get("list-patient-and-related-stuffs-bydoctor")
    @HttpCode(HttpStatus.OK)
    async listPatientandRelatedObjectsByDoctor() {
        try {
            if (AuthService.getAuthUser().role != RoleType.USER) {
                throw new ForbiddenException(
                    "Have no permission! You have no acces rights"
                );
            }
            const allPatientStuffs = await this.patientService.listPatientandRelatedObjectsByDoctor();
            return allPatientStuffs;
        } catch (err) {
            throw new HttpException(err, HttpStatus.BAD_REQUEST);
        }
    }
}
