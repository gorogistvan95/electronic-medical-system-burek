import { forwardRef, Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { PatientService } from "./patient.service";
import { PatientRepository } from "./patient.repository";
import { AuthRepository } from "../../modules/auth/auth.repository";
import { PatientController } from "./patient.controller";
import { ReferralRepository } from "../../modules/referral/referral.repository";
import { ExaminationRepository } from "../../modules/examination/examination.repository";

@Module({
    imports: [
        TypeOrmModule.forFeature([PatientRepository]),
        TypeOrmModule.forFeature([AuthRepository]),
        TypeOrmModule.forFeature([ReferralRepository]),
        TypeOrmModule.forFeature([ExaminationRepository]),
    ],
    controllers: [PatientController],
    providers: [PatientService],
    exports: [PatientService, TypeOrmModule.forFeature([PatientRepository])],
})
export class PatientModule {}
