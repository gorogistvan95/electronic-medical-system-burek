"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsString } from "class-validator";

export class CreatePatientDto {
    @IsString()
    @ApiPropertyOptional()
    readonly name: string;

    @IsString()
    @ApiPropertyOptional()
    readonly gender: string;

    @IsString()
    @ApiPropertyOptional()
    readonly address: string;

    @IsString()
    @ApiPropertyOptional()
    readonly idCardNumber: string;

    @IsString()
    @ApiPropertyOptional()
    readonly tajNumber: string;
}
