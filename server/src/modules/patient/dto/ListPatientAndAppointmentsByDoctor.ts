"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { PatientEntity } from "../patient.entity";
import { IsArray, IsString } from "class-validator";
import { ReferralDto } from "../../../modules/referral/dto/ReferralDto";
import { TimeTableDto } from "../../../modules/timetable/dto/TimeTableDto";

export class ListPatientAndAppointmentsByDoctor {
    @IsString()
    @ApiPropertyOptional()
    readonly id: string;

    @IsString()
    @ApiPropertyOptional()
    readonly name: string;

    @IsString()
    @ApiPropertyOptional()
    readonly gender: string;

    @IsString()
    @ApiPropertyOptional()
    readonly address: string;

    @IsString()
    @ApiPropertyOptional()
    readonly idCardNumber: string;

    @IsString()
    @ApiPropertyOptional()
    readonly tajNumber: string;

    @ApiPropertyOptional()
    readonly referral: {
        referral: ReferralDto;
        appointments: TimeTableDto[];
    };

    constructor(
        patient: PatientEntity,
        referral: { referral: ReferralDto; appointments: TimeTableDto[] }
    ) {
        this.id = patient.id;
        this.name = patient.name;
        this.gender = patient.gender;
        this.tajNumber = patient.tajNumber;
        this.address = patient.address;
        this.idCardNumber = patient.idCardNumber;
        this.referral = referral;
    }
}
