"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";

import { RoleType } from "../../../common/constants/role-type";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { PatientEntity } from "../patient.entity";
import { IsString } from "class-validator";

export class ListPatientResponseDto {

    @IsString()
    @ApiPropertyOptional()
    readonly id: string;

    @IsString()
    @ApiPropertyOptional()
    readonly name: string;

    @IsString()
    @ApiPropertyOptional()
    readonly gender: string;

    @IsString()
    @ApiPropertyOptional()
    readonly address: string;

    @IsString()
    @ApiPropertyOptional()
    readonly idCardNumber: string;

    @IsString()
    @ApiPropertyOptional()
    readonly tajNumber: string;

    constructor(patient: PatientEntity) {
        this.id = patient.id;
        this.name = patient.name;
        this.gender = patient.gender;
        this.tajNumber = patient.tajNumber;
        this.address = patient.address;
        this.idCardNumber = patient.idCardNumber;
    }

}