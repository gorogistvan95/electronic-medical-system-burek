"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";

import { RoleType } from "../../../common/constants/role-type";
import { AbstractDto } from "../../../common/dto/AbstractDto";
import { PatientEntity } from "../patient.entity";

export class PatientDto extends AbstractDto {
    @ApiPropertyOptional()
    name: string;

    @ApiPropertyOptional()
    gender: string;

    @ApiPropertyOptional()
    address?: string;

    @ApiPropertyOptional()
    idCardNumber?: string;

    @ApiPropertyOptional()
    tajNumber?: string;

    constructor(patient: PatientEntity) {
        super(patient);
        this.name = patient.name;
        this.gender = patient.gender;
        this.tajNumber = patient.tajNumber;
        this.address = patient.address;
        this.idCardNumber = patient.idCardNumber;
    }
}
