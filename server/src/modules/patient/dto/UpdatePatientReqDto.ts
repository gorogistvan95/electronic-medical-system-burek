"use strict";

import { ApiPropertyOptional } from "@nestjs/swagger";
import { IsNumber, IsOptional, IsString } from "class-validator";

export class UpdatePatientReqDto {
    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly name: string;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly address: string;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly idCardNumber: string;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    readonly tajNumber: string;
}
