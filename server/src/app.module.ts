import "./boilerplate.polyfill";

import { MiddlewareConsumer, Module, NestModule } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { contextMiddleware } from "./middlewares";
import { AuthModule } from "./modules/auth/auth.module";
import { ConfigService } from "./shared/services/config.service";
import { SharedModule } from "./shared/shared.module";
import { PatientModule } from "./modules/patient/patient.module";
import { ReceptonistModule } from "./modules/receptionist/receptionist.module";
import { ClinicClassModule } from "./modules/clinicclass/clinicclass.module";
import { ExaminationModule } from "./modules/examination/examination.module";
import { ReferralModule } from "./modules/referral/referral.module";
import { TimeTableModule } from "./modules/timetable/timetable.module";
import { DocumentModule } from "./modules/document/document.module";
import { MulterModule } from "@nestjs/platform-express";
import { DocumentAvailableModule } from "./modules/documents-available/document-available.module";
import { DiagnosisModule } from "./modules/diagnosis/diagnosis.module";

@Module({
    imports: [
        DiagnosisModule,
        DocumentAvailableModule,
        DocumentModule,
        TimeTableModule,
        ReferralModule,
        ClinicClassModule,
        ExaminationModule,
        AuthModule,
        PatientModule,
        ReceptonistModule,
        MulterModule.register({
            dest: "./documents",
        }),
        TypeOrmModule.forRootAsync({
            imports: [SharedModule],
            useFactory: (configService: ConfigService) =>
                configService.typeOrmConfig,

            inject: [ConfigService],
        }),
    ],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
        consumer.apply(contextMiddleware).forRoutes("*");
    }
}
