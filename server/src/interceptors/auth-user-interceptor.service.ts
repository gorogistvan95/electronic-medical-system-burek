import {
    CallHandler,
    ExecutionContext,
    Injectable,
    NestInterceptor,
    UnauthorizedException,
    HttpStatus,
    HttpException,
} from "@nestjs/common";
import { Observable } from "rxjs";

import { AuthService } from "../modules/auth/auth.service";
import { AuthEntity } from "../modules/auth/auth.entity";
import { AuthRepository } from "../modules/auth/auth.repository";
import { JwtService } from "@nestjs/jwt";
import { PatientService } from "../modules/patient/patient.service";
import { PatientEntity } from "../modules/patient/patient.entity";

@Injectable()
export class AuthUserInterceptor implements NestInterceptor {
    constructor(
        public readonly authRepository: AuthRepository,
        public readonly patientService: PatientService,
        private readonly jwtService: JwtService
    ) {}
    async intercept(
        context: ExecutionContext,
        next: CallHandler
    ): Promise<Observable<any>> {
        const request: Request = context.switchToHttp().getRequest();
        const reqUrl = request["_parsedUrl"]["_raw"];
        let splittedUrl: string[] = reqUrl.split("/").reverse();
        await this.patientValidator(splittedUrl);
        try {
            const token = request.headers["authorization"];
            if (!token || token.split(" ")[0] != "Bearer") {
                throw new UnauthorizedException();
            }
            const decodedToken = this.jwtService.decode(token.split(" ")[1]);
            const user: AuthEntity = await this.authRepository.findOne(
                decodedToken["id"]
            );
            if (user) AuthService.setAuthUser(user);
            return next.handle();
        } catch (err) {
            throw new HttpException("Bad token", HttpStatus.BAD_REQUEST);
            return next.handle();
        }
        return next.handle();
    }

    async patientValidator(splittedUrl: string[]) {
        if (splittedUrl[1] == "document-upload") {
            const patientEntity: PatientEntity = await this.patientService.findOne(
                { id: splittedUrl[0] }
            );
            if (!patientEntity) {
                throw new HttpException(
                    "Doesnt exist  this patientID",
                    HttpStatus.BAD_REQUEST
                );
            }
            PatientService.setPatient(patientEntity);
        }
    }

    async documentDownloadValidator(splittedUrl: string[]) {
        if (splittedUrl[1] == "document-upload") {
            const patientEntity: PatientEntity = await this.patientService.findOne(
                { id: splittedUrl[0] }
            );
            if (!patientEntity) {
                throw new HttpException(
                    "Doesnt exist  this patientID",
                    HttpStatus.BAD_REQUEST
                );
            }
            PatientService.setPatient(patientEntity);
        }
    }
}
