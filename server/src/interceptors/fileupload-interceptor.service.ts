import { extname } from "path";
import { diskStorage } from "multer";
import { v4 as uuidv4 } from "uuid";
import { PatientService } from "../modules/patient/patient.service";
import * as path from "path";
import * as fs from "fs";

// Multer configuration
export let multerConfig = {
    dest: `documents`,
    defaultPath: __dirname,
};
// Multer upload options
export const multerOptions = {
    // Enable file size limits
    /*    limits: {
        fileSize: +process.env.MAX_FILE_SIZE,
    },
    // Check the mimetypes to allow for upload
   fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpg|jpeg|png|gif)$/)) {
            // Allow storage of file
            cb(null, true);
        } else {
            // Reject file
            cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
        }
    },*/
    storage: diskStorage({
        destination: (req: any, file: any, cb: any) => {
            let uploadPath = path.resolve(
                __dirname,
                "../../",
                multerConfig.dest,
                `${PatientService.getPatient().id.toString()}`
            );
            if (!fs.existsSync(uploadPath)) {
                fs.mkdirSync(uploadPath, { recursive: true });
                cb(null, uploadPath);
            } else {
                cb(null, uploadPath);
            }
        },
        filename: (req: any, file: any, cb: any) => {
            const filename = uuidv4();
            cb(null, filename + extname(file.originalname));
        },
    }),
};
