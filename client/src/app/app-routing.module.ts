import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './common/guards/auth.guard';
import { AdminDashboardComponent } from './pages/admin/admin-dashboard/admin-dashboard.component';
import { ClinicClassCreateComponent } from './pages/admin/clinic-class-create/clinic-class-create.component';
import { ClinicClassUpdateComponent } from './pages/admin/clinic-class-update/clinic-class-update.component';
import { ExaminationCreateComponent } from './pages/admin/examination-create/examination-create.component';
import { ExaminationUpdateComponent } from './pages/admin/examination-update/examination-update.component';
import { UserCreateComponent } from './pages/admin/user-create/user-create.component';
import { UserUpdateComponent } from './pages/admin/user-update/user-update.component';
import { LoginComponent } from './pages/login/login.component';
import { PatientAppointmentsListUpdateComponent } from './pages/receptionist/patient-list-update/patient-list-update.component';
import { ReceptionistDashboardComponent } from './pages/receptionist/receptionist-dashboard/receptionist-dashboard.component';
import { AppointmentsComponent } from './pages/user/user-dashboard/appointments/appointments.component';
import { DiagnosisListUpdateComponent } from './pages/user/user-dashboard/diagnosis-list-update/diagnosis-list-update.component';
import { UserDashboardComponent } from './pages/user/user-dashboard/user-dashboard.component';

const routes: Routes = [
  {
    path: 'admin',
    children: [
      {
        path: 'dashboard',
        component: AdminDashboardComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'user/create',
        component: UserCreateComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'user/update',
        component: UserUpdateComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'clinic-class/create',
        component: ClinicClassCreateComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'clinic-class/update',
        component: ClinicClassUpdateComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'examination/create',
        component: ExaminationCreateComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'examination/update',
        component: ExaminationUpdateComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: 'receptionist',
    children: [
      {
        path: 'dashboard',
        component: ReceptionistDashboardComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'patient/appointments-list-update',
        component: PatientAppointmentsListUpdateComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  {
    path: 'user',
    children: [
      {
        path: 'dashboard',
        component: UserDashboardComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'diagnosises',
        component: DiagnosisListUpdateComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'appointments',
        component: AppointmentsComponent,
        canActivate: [AuthGuard],
      },
    ],
  },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
