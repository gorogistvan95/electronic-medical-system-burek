import { Component, OnInit } from '@angular/core';
import { AuthAPIService } from 'src/app/common/api/auth/auth-api.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';

  constructor(
    private authAPIService: AuthAPIService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {}

  async login() {
    try {
      const response = await this.authAPIService
        .login(this.username, this.password)
        .toPromise();
      if (response) {
        this.authService.login(response.body.token.accessToken);
      }
    } catch (e) {
      console.log(e);
    }
  }
}
