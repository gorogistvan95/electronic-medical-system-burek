import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AuthAPIService } from 'src/app/common/api/auth/auth-api.service';
import { ClinicClassMappedToNameAndId } from 'src/app/common/api/clinic-class/clinic-class.api.interface';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';
import { MustMatch } from 'src/app/common/validators/must-match.validator';
import { RoleType } from '../../../common/enums/role-types.enum';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss'],
})
export class UserCreateComponent implements OnInit {
  RoleTypes = [];
  RoleType = RoleType;
  form: FormGroup;
  clinicClasses$: Observable<ClinicClassMappedToNameAndId[]>;

  constructor(
    private fb: FormBuilder,
    private authAPIService: AuthAPIService,
    private snackBar: MatSnackBar,
    private clinicClassAPIService: ClinicClassAPIService
  ) {}

  ngOnInit(): void {
    for (let role in RoleType) {
      this.RoleTypes.push(role);
    }

    this.clinicClasses$ = this.clinicClassAPIService
      .listClinicClassMappedToIdAndName()
      .pipe(shareReplay());

    this.form = this.fb.group(
      {
        username: ['', [Validators.required, Validators.minLength(5)]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        name: ['', []],
        clinicClassId: [''],
        isDiagnosis: Boolean,
        passwordConfirm: ['', [Validators.required]],
        role: ['', [Validators.required, Validators.minLength(4)]],
      },
      {
        validator: MustMatch('password', 'passwordConfirm'),
      }
    );
  }

  async submit(form: FormGroup, formDirective: FormGroupDirective) {
    if (form.invalid) {
      this.snackBar.open('Hibásan kitöltött mezők!', 'Rendben');
      return;
    }
    try {
      const response = await this.authAPIService
        .register({
          ...this.form.value,
          clinicClassId:
            this.form.get('role').value === RoleType.USER
              ? this.form.get('clinicClassId').value
              : undefined,
        })
        .toPromise();
      if (response.ok) {
        form.reset();
        formDirective.resetForm();
        this.snackBar.open('A felhasználó elmentve.', 'Rendben');
      }
    } catch (e) {
      this.snackBar.open(
        'Hiba történt, nem tudtuk létrehozni a felhasználót.',
        'Rendben'
      );
    }
  }
}
