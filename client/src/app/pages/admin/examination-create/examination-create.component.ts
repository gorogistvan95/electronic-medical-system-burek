import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { ClinicClassMappedToNameAndId } from 'src/app/common/api/clinic-class/clinic-class.api.interface';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';
import { ExaminationAPIService } from 'src/app/common/api/examination/examination.api.service';
@Component({
  selector: 'app-examination-create',
  templateUrl: './examination-create.component.html',
  styleUrls: ['./examination-create.component.scss'],
})
export class ExaminationCreateComponent implements OnInit {
  form: FormGroup;
  clinicClasses$: Observable<ClinicClassMappedToNameAndId[]>;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private clinicClassService: ClinicClassAPIService,
    private examinationAPIService: ExaminationAPIService
  ) {}

  ngOnInit(): void {
    this.clinicClasses$ = this.clinicClassService
      .listClinicClassMappedToIdAndName()
      .pipe(shareReplay());

    this.form = this.fb.group({
      name: ['', [Validators.required]],
      price: [0, []],
      description: ['', []],
      clinicClassId: ['', [Validators.required]],
    });
  }

  async submit(form: FormGroup, formDirective: FormGroupDirective) {
    if (form.invalid) {
      this.snackBar.open('Hibásan kitöltött mezők!', 'Rendben');
      return;
    }
    try {
      const response = await this.examinationAPIService
        .createExamination(form.value)
        .toPromise();

      if (response.ok) {
        formDirective.resetForm();
        this.form.reset();
        this.snackBar.open('Sikeres vizsgálat felvétel!', 'Rendben');
      }
    } catch (e) {
      this.snackBar.open('Sikertelen vizsgálat felvétel!', 'Rendben');
    }
  }
}
