import { Component } from '@angular/core';

@Component({
  selector: 'app-clinic-class-confirm-delete-dialog',
  templateUrl: 'clinic-class-confirm-delete-dialog.component.html',
  styleUrls: ['clinic-class-confirm-delete-dialog.component.scss'],
})
export class ClinicClassConfirmDeleteDialogComponent {}
