import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, noop, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ListClinicClassResponseDto } from 'src/app/common/api/clinic-class/clinic-class.api.interface';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';
import { ClinicClassConfirmDeleteDialogComponent } from './clinic-class-confirm-delete-dialog/clinic-class-confirm-delete-dialog.component';
import { ClinicClassUpdateDialogComponent } from './clinic-class-update-dialog/clinic-class-update-dialog.component';

@Component({
  selector: 'app-clinic-class-update',
  templateUrl: './clinic-class-update.component.html',
  styleUrls: ['./clinic-class-update.component.scss'],
})
export class ClinicClassUpdateComponent implements OnInit {
  displayedColumns: string[] = ['name', 'onlyOneUser', 'update', 'delete'];
  clinicClasses$: BehaviorSubject<
    ListClinicClassResponseDto[]
  > = new BehaviorSubject([]);
  private asd = true;
  private subs: Subscription[] = [];

  constructor(
    private clinicClassService: ClinicClassAPIService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit(): Promise<void> {
    await this.loadClinicClasses();
  }

  openClinicClassUpdateDialog(data: ListClinicClassResponseDto) {
    const dialogRef = this.dialog.open(ClinicClassUpdateDialogComponent, {
      data,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe(async (o) => {
        await this.loadClinicClasses();
      });
  }

  openClinicClassConfirmDeleteDialog(clinicClassId: string) {
    const dialogRef = this.dialog.open(ClinicClassConfirmDeleteDialogComponent);
    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.deleteClinicClass(clinicClassId);
      });
  }

  async deleteClinicClass(clinicClassId: string) {
    try {
      const response = await this.clinicClassService
        .deleteClinicClass(clinicClassId)
        .toPromise();
      if (response.ok) {
        await this.loadClinicClasses();
        this.snackBar.open('Sikeres torles', 'Rendben');
      }
    } catch (error) {
      this.snackBar.open('Sikertelen torles', 'Rendben');
    }
  }

  async loadClinicClasses() {
    try {
      const response = await this.clinicClassService
        .listClinicClasses()
        .toPromise();
      if (response.ok) {
        this.clinicClasses$.next(response.body);
      }
    } catch (e) {
      this.snackBar.open('Nem tudtuk betolteni', 'Rendben');
    }
  }
}
