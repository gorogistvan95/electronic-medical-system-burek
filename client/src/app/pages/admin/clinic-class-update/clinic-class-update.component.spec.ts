import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicClassUpdateComponent } from './clinic-class-update.component';

describe('ClinicClassUpdateComponent', () => {
  let component: ClinicClassUpdateComponent;
  let fixture: ComponentFixture<ClinicClassUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicClassUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicClassUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
