import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { ListClinicClassResponseDto } from 'src/app/common/api/clinic-class/clinic-class.api.interface';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';

@Component({
  selector: 'app-clinic-class-update-dialog',
  templateUrl: './clinic-class-update-dialog.component.html',
  styleUrls: ['./clinic-class-update-dialog.component.scss'],
})
export class ClinicClassUpdateDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ListClinicClassResponseDto,
    public dialogRef: MatDialogRef<ClinicClassUpdateDialogComponent>,
    private fb: FormBuilder,
    public clinicClassApiService: ClinicClassAPIService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: [this.data.name, [Validators.required]],
      onlyOneUser: [this.data.onlyOneUser, []],
    });
  }

  async updateClinicClass() {
    try {
      const response = await this.clinicClassApiService
        .updateClinicClass(this.form.value, this.data.id)
        .toPromise();
      if (response.ok) {
        this.dialogRef.close(true);
      }
    } catch (error) {
      console.log(error);
    }
  }
}
