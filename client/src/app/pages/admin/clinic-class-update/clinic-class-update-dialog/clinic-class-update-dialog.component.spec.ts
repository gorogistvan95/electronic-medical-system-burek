import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicClassUpdateDialogComponent } from './clinic-class-update-dialog.component';

describe('ClinicClassUpdateDialogComponent', () => {
  let component: ClinicClassUpdateDialogComponent;
  let fixture: ComponentFixture<ClinicClassUpdateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicClassUpdateDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicClassUpdateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
