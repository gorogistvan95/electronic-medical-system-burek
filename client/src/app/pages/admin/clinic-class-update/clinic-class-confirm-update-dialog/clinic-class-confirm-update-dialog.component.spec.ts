import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicClassConfirmUpdateDialogComponent } from './clinic-class-confirm-update-dialog.component';

describe('ClinicClassConfirmUpdateDialogComponent', () => {
  let component: ClinicClassConfirmUpdateDialogComponent;
  let fixture: ComponentFixture<ClinicClassConfirmUpdateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicClassConfirmUpdateDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicClassConfirmUpdateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
