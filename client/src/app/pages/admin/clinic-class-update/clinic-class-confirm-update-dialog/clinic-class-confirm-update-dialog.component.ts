import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clinic-class-confirm-update-dialog',
  templateUrl: './clinic-class-confirm-update-dialog.component.html',
  styleUrls: ['./clinic-class-confirm-update-dialog.component.scss']
})
export class ClinicClassConfirmUpdateDialogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
