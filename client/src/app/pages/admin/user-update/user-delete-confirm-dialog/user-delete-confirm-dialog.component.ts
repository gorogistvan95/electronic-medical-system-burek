import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-delete-confirm-dialog',
  templateUrl: './user-delete-confirm-dialog.component.html',
  styleUrls: ['./user-delete-confirm-dialog.component.scss'],
})
export class UserDeleteConfirmDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { id: string; username: string },
    public dialogRef: MatDialogRef<UserDeleteConfirmDialogComponent>,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {}

  async delete() {
    try {
      // TODO: call user delete API
      if (true) {
        this.dialogRef.close(this.data.id);
        this.snackBar.open('Sikeresen törölted a felhasználót', 'Rendben');
      }
    } catch (e) {
      this.snackBar.open('Nem tudtuk törölni a felhasználót', 'Rendben');
      this.dialogRef.close();
    }
  }
}
