import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  AuthDto,
  UpdateUserReqDto,
} from 'src/app/common/api/auth/auth-api.interface';
import { AuthAPIService } from 'src/app/common/api/auth/auth-api.service';
import { MustMatch } from 'src/app/common/validators/must-match.validator';

@Component({
  selector: 'app-user-update-dialog',
  templateUrl: './user-update-dialog.component.html',
  styleUrls: ['./user-update-dialog.component.scss'],
})
export class UserUpdateDialogComponent implements OnInit {
  form: FormGroup;
  updateUserInterFace: UpdateUserReqDto;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: AuthDto,
    public dialogRef: MatDialogRef<UserUpdateDialogComponent>,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private authAPIService: AuthAPIService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group(
      {
        username: [
          this.data.username,
          [
            /*Validators.required, Validators.minLength(5)*/
          ],
        ],
        name: [this.data.name, []],
        password: [undefined, []],
        passwordConfirm: [undefined, []],
      },
      {
        validator: MustMatch('password', 'passwordConfirm'),
      }
    );
  }

  async updateUser() {
    try {
      const formValues = this.form.value;
      if (formValues.name === this.data.name) {
        delete formValues.name;
      }
      if (formValues.username === this.data.username) {
        delete formValues.username;
      }
      const filteredVormValue = await Object.entries(formValues).reduce(
        (a, [k, v]) => (v ? ((a[k] = v), a) : a),
        {}
      );
      if (this.form.invalid) {
        this.snackBar.open('Hibásan kitöltött mezők!', 'Rendben');
        return;
      }
      console.log(filteredVormValue);
      const response = await this.authAPIService
        .updateUser(filteredVormValue, this.data.id)
        .toPromise();

      if (response.ok) {
        this.snackBar.open('Sikeres felhasználó módosítás', 'Rendben');
        this.dialogRef.close(true);
      }
    } catch (e) {
      this.snackBar.open('Sikertelen felhasználó módosítás', 'Rendben');
      this.dialogRef.close();
    }
  }
}
