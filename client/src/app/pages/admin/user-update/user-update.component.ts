import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { filter as _filter } from 'lodash';
import {
  AuthDto,
  UpdateUserReqDto,
} from 'src/app/common/api/auth/auth-api.interface';
import { UserDeleteConfirmDialogComponent } from './user-delete-confirm-dialog/user-delete-confirm-dialog.component';
import { map as _map } from 'lodash';
import { UserUpdateDialogComponent } from './user-update-dialog/user-update-dialog.component';
import { AuthAPIService } from 'src/app/common/api/auth/auth-api.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss'],
})
export class UserUpdateComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = [
    'username',
    'name',
    'role',
    'clinicClassName',
    `isDiagnosis`,
    'createdAt',
    'updatedAt',
    'update',
    'delete',
  ];
  users$: BehaviorSubject<AuthDto[]> = new BehaviorSubject([]);
  private subs: Subscription[] = [];

  constructor(
    public dialog: MatDialog,
    private authAPIService: AuthAPIService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.subs.push(
      this.authAPIService
        .listUsers()
        .pipe(map((r) => r.body))
        .subscribe((users) => {
          this.users$.next(users);
        })
    );
  }

  ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }

  openUserDeleteConfirmDialog(id: string, username: string) {
    const dialogRef = this.dialog.open(UserDeleteConfirmDialogComponent, {
      data: { id, username },
    });
    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.deleteUser(id);
      });

    this.subs.push(
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.users$.next(_filter(this.users$.value, (u) => u.id !== result));
        }
      })
    );
  }
  async deleteUser(userId: string) {
    try {
      const response = await this.authAPIService.deleteUser(userId).toPromise();
      if (response.ok) {
        await this.loadUsers();
        this.snackBar.open('Sikeres törlés', 'Rendben');
      }
    } catch (error) {
      this.snackBar.open('Sikertelen törlés', 'Rendben');
    }
  }

  async loadUsers() {
    try {
      const response = await this.authAPIService.listUsers().toPromise();
      if (response.ok) {
        this.users$.next(response.body);
      }
    } catch (e) {
      this.snackBar.open('Nem tudtuk betölteni', 'Rendben');
    }
  }

  async openUserUpdateDialog(userData: AuthDto) {
    const dialogRef = this.dialog.open(UserUpdateDialogComponent, {
      data: userData,
    });
    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.loadUsers();
      });

    this.subs.push(
      dialogRef.afterClosed().subscribe((result: AuthDto) => {
        if (result) {
          this.users$.next(
            _map(this.users$.value, (u) => (u.id !== result.id ? u : result))
          );
        }
      })
    );
  }
}
