import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicClassCreateComponent } from './clinic-class-create.component';

describe('ClinicClassCreateComponent', () => {
  let component: ClinicClassCreateComponent;
  let fixture: ComponentFixture<ClinicClassCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClinicClassCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicClassCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
