import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';

@Component({
  selector: 'app-clinic-class-create',
  templateUrl: './clinic-class-create.component.html',
  styleUrls: ['./clinic-class-create.component.scss'],
})
export class ClinicClassCreateComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private clinicClassService: ClinicClassAPIService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      onlyOneUser: [false, []],
    });
  }

  async submit() {
    if (this.form.invalid) {
      this.snackBar.open('Hibásan kitöltött mezők!', 'Rendben');
      return;
    }
    try {
      const response = await this.clinicClassService
        .createClinicClass(this.form.value)
        .toPromise();

      if (response.ok) {
        this.form = this.fb.group({
          name: ['', [Validators.required]],
          onlyOneUser: [false, []],
        });
        this.snackBar.open('Sikeres klinika osztály felvétel', 'Rendben');
      }
    } catch (e) {
      this.snackBar.open('Sikertelen klinika osztály felvétel', 'Rendben');
    }
  }
}
