import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss'],
})
export class AdminDashboardComponent implements OnInit {
  dashBoardItems: { title: string; link: string }[] = [
    { title: 'Felhasználó Felvétel', link: '/admin/user/create' },
    { title: 'Felhasználó Módosítás', link: '/admin/user/update' },
    { title: 'Klinika Osztály Felvétel', link: '/admin/clinic-class/create' },
    { title: 'Klinika Osztály Módosítás', link: '/admin/clinic-class/update' },
    { title: 'Vizsgálat Felvétel', link: '/admin/examination/create' },
    { title: 'Vizsgálat Módosítás', link: '/admin/examination/update' },
  ];

  constructor() {}

  ngOnInit(): void {}
}
