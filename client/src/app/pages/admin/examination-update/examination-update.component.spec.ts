import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExaminationUpdateDialogComponent } from './examination-update-dialog/examination-update-dialog.component';

describe('ExaminationUpdateDialogComponent', () => {
  let component: ExaminationUpdateDialogComponent;
  let fixture: ComponentFixture<ExaminationUpdateDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExaminationUpdateDialogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExaminationUpdateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
