import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { ClinicClassMappedToNameAndId } from 'src/app/common/api/clinic-class/clinic-class.api.interface';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';
import {
  ExaminationDto,
  ListExaminationResponseDto,
} from 'src/app/common/api/examination/examination.api.interface';
import { ExaminationAPIService } from 'src/app/common/api/examination/examination.api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { filter, map } from 'rxjs/operators';
import { map as _map } from 'lodash';
import { MatDialog } from '@angular/material/dialog';
import { ExaminationUpdateDialogComponent } from './examination-update-dialog/examination-update-dialog.component';
import { ExaminationDeleteConfirmDialogComponent } from './examination-delete-confirm-dialog/examination-delete-confirm-dialog';

@Component({
  selector: 'app-examination-update',
  templateUrl: './examination-update.component.html',
  styleUrls: ['./examination-update.component.scss'],
})
export class ExaminationUpdateComponent implements OnInit {
  form: FormGroup;
  clinicClasses$: Observable<ClinicClassMappedToNameAndId[]>;
  displayedColumns: string[] = [
    'name',
    'price',
    `description`,
    'update',
    'delete',
  ];
  examinations$: BehaviorSubject<
    ListExaminationResponseDto[]
  > = new BehaviorSubject([]);
  private subs: Subscription[] = [];
  selectedClinicClassId: string;

  constructor(
    public dialog: MatDialog,
    private clinicClassService: ClinicClassAPIService,
    private examinationApiService: ExaminationAPIService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({ clinicClassId: [''] });
    this.clinicClasses$ = this.clinicClassService
      .listClinicClassMappedToIdAndName()
      .pipe(shareReplay());
    this.subs.push(
      this.examinationApiService
        .listExamination(`${this.selectedClinicClassId}`)
        .pipe(map((r) => r.body))
        .subscribe((examinations) => {
          this.examinations$.next(examinations);
        })
    );
  }
  async openExaminationUpdateDialog(examinationData: ExaminationDto) {
    const dialogRef = this.dialog.open(ExaminationUpdateDialogComponent, {
      data: examinationData,
    });
    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.loadExaminations(this.selectedClinicClassId);
      });

    this.subs.push(
      dialogRef
        .afterClosed()
        .subscribe((result: ListExaminationResponseDto) => {
          if (result) {
            this.examinations$.next(
              _map(this.examinations$.value, (u) =>
                u.id !== result.id ? u : result
              )
            );
          }
        })
    );
  }

  openExaminationDeleteConfirmDialog(
    examinationId: string,
    examinationName: string
  ) {
    const dialogRef = this.dialog.open(
      ExaminationDeleteConfirmDialogComponent,
      {
        data: { examinationId, examinationName },
      }
    );
    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.deleteExamination(examinationId);
      });
  }

  async deleteExamination(exainationId: string) {
    try {
      const response = await this.examinationApiService
        .deleteExamination(exainationId)
        .toPromise();
      if (response.ok) {
        await this.loadExaminations(this.selectedClinicClassId);
        this.snackBar.open('sikeres törlés', 'Rendben');
      }
    } catch (error) {
      this.snackBar.open('Sikertelen törlés', 'Rendben');
    }
  }

  async selectAndRefreshTable() {
    this.subs.push(
      this.examinationApiService
        .listExamination(`${this.selectedClinicClassId}`)
        .pipe(map((r) => r.body))
        .subscribe((examinations) => {
          this.examinations$.next(examinations);
        })
    );
  }

  async loadExaminations(clinicClassId: string) {
    try {
      const response = await this.examinationApiService
        .listExamination(clinicClassId)
        .toPromise();
      if (response.ok) {
        this.examinations$.next(response.body);
      }
    } catch (e) {
      this.snackBar.open('Nem tudtuk betolteni', 'Rendben');
    }
  }
}
