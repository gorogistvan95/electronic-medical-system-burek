import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UpdateExaminationReqDto } from 'src/app/common/api/clinic-class/clinic-class.api.interface';
import { ExaminationDto } from 'src/app/common/api/examination/examination.api.interface';
import { ExaminationAPIService } from 'src/app/common/api/examination/examination.api.service';

@Component({
  selector: 'app-examination-update-dialog',
  templateUrl: './examination-update-dialog.component.html',
  styleUrls: ['./examination-update-dialog.component.scss'],
})
export class ExaminationUpdateDialogComponent implements OnInit {
  form: FormGroup;
  updateExaminationInterFace: UpdateExaminationReqDto;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ExaminationDto,
    public dialogRef: MatDialogRef<ExaminationUpdateDialogComponent>,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private examinationApiService: ExaminationAPIService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: [this.data.name, [Validators.required, Validators.minLength(1)]],
      price: [this.data.price, [Validators.required]],
      description: [this.data.description, []],
    });
  }
  async updateExamination() {
    try {
      const formValues = this.form.value;
      const filteredVormValue = await Object.entries(formValues).reduce(
        (a, [k, v]) => (v ? ((a[k] = v), a) : a),
        {}
      );
      if (this.form.invalid) {
        this.snackBar.open('Hibásan kitöltött mezők!', 'Rendben');
        return;
      }
      const response = await this.examinationApiService
        .updateExamination(this.data.id, filteredVormValue)
        .toPromise();

      if (response.ok) {
        this.snackBar.open('Sikeres felhasználó módosítás', 'Rendben');
        this.dialogRef.close(true);
      }
    } catch (e) {
      this.snackBar.open('Sikertelen felhasználó módosítás', 'Rendben');
      this.dialogRef.close();
    }
  }
}
