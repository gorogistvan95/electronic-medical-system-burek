import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-examination-delete-confirm-dialog',
  templateUrl: './examination-delete-confirm-dialog.component.html',
  styleUrls: ['./examination-delete-confirm-dialog.component.scss'],
})
export class ExaminationDeleteConfirmDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { id: string; name: string },
    public dialogRef: MatDialogRef<ExaminationDeleteConfirmDialogComponent>,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {}

  async delete() {
    try {
      // TODO: call user delete API
      if (true) {
        this.dialogRef.close(this.data.id);
        this.snackBar.open('Sikeresen törölted a vizsgálatot', 'Rendben');
      }
    } catch (e) {
      this.snackBar.open('Nem tudtuk törölni a vizsgálatot', 'Rendben');
      this.dialogRef.close();
    }
  }
}
