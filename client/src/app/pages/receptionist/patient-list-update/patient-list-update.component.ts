import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';
import { ListTimeTableByPatientResDto } from 'src/app/common/api/timetable/timetable.api.interface';
import { TimetableAPIService } from 'src/app/common/api/timetable/timetable.api.service';
import { PatientIdPassingService } from 'src/app/services/patient-id-passing-service';
import { PatientAppointmentDeleteConfirmDialogComponent } from './patient-appointment-list-delete-confirm-dialog/patient-appointment-delete-confirm-dialog.component';

@Component({
  selector: 'app-patient-list-update.component',
  templateUrl: './patient-list-update.component.html',
  styleUrls: ['./patient-list-update.component.scss'],
})
export class PatientAppointmentsListUpdateComponent implements OnInit {
  displayedColumns: string[] = [
    'clinicClassName',
    'examinationName',
    'startOfSession',
    'endOfSession',
    'delete',
  ];
  appointments$: BehaviorSubject<
    ListTimeTableByPatientResDto[]
  > = new BehaviorSubject([]);
  private subs: Subscription[] = [];
  patientId: string = PatientIdPassingService.getPatientId();
  constructor(
    private clinicClassService: ClinicClassAPIService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private patientIdPassingService: PatientIdPassingService,
    private timetableAPIService: TimetableAPIService
  ) {}

  async ngOnInit(): Promise<void> {
    this.subs.push(
      this.timetableAPIService
        .getAppointmentsByPatientId(this.patientId)
        .pipe(map((r) => r.body))
        .subscribe((users) => {
          this.appointments$.next(users);
        })
    );
  }

  openAppointmentDeleteConfirmDialog(
    appointmentId: string,
    examinationName: string
  ) {
    const dialogRef = this.dialog.open(
      PatientAppointmentDeleteConfirmDialogComponent,
      {
        data: { appointmentId, examinationName },
      }
    );
    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.deleteAppointment(appointmentId);
      });
  }

  async deleteAppointment(appointmentId: string) {
    try {
      const response = await this.timetableAPIService
        .deleteAppointment(appointmentId)
        .toPromise();
      if (response.ok) {
        await this.loadAppointment();
        this.snackBar.open('Sikeres torles', 'Rendben');
      }
    } catch (error) {
      this.snackBar.open('Sikertelen torles', 'Rendben');
    }
  }

  async loadAppointment() {
    try {
      const response = await this.timetableAPIService
        .getAppointmentsByPatientId(this.patientId)
        .toPromise();
      if (response.ok) {
        this.appointments$.next(response.body);
      }
    } catch (e) {
      this.snackBar.open('Nem tudtuk betolteni', 'Rendben');
    }
  }
}
