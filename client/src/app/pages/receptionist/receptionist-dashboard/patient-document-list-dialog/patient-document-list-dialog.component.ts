import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { ListDocumentResDto } from 'src/app/common/api/document/document.api.interface';
import { DocumentAPIService } from 'src/app/common/api/document/document.api.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-patient-document-list-dialog',
  templateUrl: 'patient-document-list-dialog.component.html',
  styleUrls: ['patient-document-list-dialog.component.scss'],
})
export class PatientDocumentReceptionistDialogComponent implements OnInit {
  public files$: BehaviorSubject<ListDocumentResDto[]> = new BehaviorSubject(
    []
  );
  public authId$ = new Observable<string>();
  displayedColumns: string[] = [
    'name',
    'description',
    'size',
    'createdAt',
    'download',
    'delete',
  ];
  constructor(
    public dialogRef: MatDialogRef<PatientDocumentReceptionistDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    public documentAPIService: DocumentAPIService,
    private snackBar: MatSnackBar,
    public authService: AuthService
  ) {}

  async ngOnInit() {
    await this.loadFiles();
    this.authId$ = this.authService.authId$;
  }

  async deleteDocument(documentId: string) {
    const response = await this.documentAPIService
      .deleteDocument(documentId)
      .toPromise();
    if (response.ok) {
      this.loadFiles();
      this.snackBar.open('Sikeres fájl törlés!', 'Rendben');
    }
  }
  catch(e) {
    console.log(e);
    this.snackBar.open('Sikertelen fájl törlés!', 'Rendben');
  }

  async loadFiles() {
    try {
      const response = await this.documentAPIService
        .listPatientDocumentsByDoctor(this.data.id)
        .toPromise();
      this.files$.next(response.body);
    } catch (e) {
      this.snackBar.open('Hiba történt a fájlok listázásakor', 'Rendben');
    }
  }
}
