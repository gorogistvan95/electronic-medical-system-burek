import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-patient-delete-confirm-dialog',
  templateUrl: './patient-delete-confirm-dialog.component.html',
  styleUrls: ['./patient-delete-confirm-dialog.component.scss'],
})
export class PatientDeleteConfirmDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { id: string; name: string },
    public dialogRef: MatDialogRef<PatientDeleteConfirmDialogComponent>,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {}

  async delete() {
    try {
      // TODO: call user delete API
      if (true) {
        this.dialogRef.close(this.data.id);
        this.snackBar.open('Sikeresen törölted a páciensst', 'Rendben');
      }
    } catch (e) {
      this.snackBar.open('Nem tudtuk törölni a pácienst', 'Rendben');
      this.dialogRef.close();
    }
  }
}
