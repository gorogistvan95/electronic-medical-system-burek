import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { AuthDto } from 'src/app/common/api/auth/auth-api.interface';
import { MatHorizontalStepper } from '@angular/material/stepper';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClinicClassMappedToNameAndId } from 'src/app/common/api/clinic-class/clinic-class.api.interface';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';
import { AuthAPIService } from 'src/app/common/api/auth/auth-api.service';
import { PatientAPIService } from 'src/app/common/api/patient/patient.api.service';
import { ReferralAPIService } from 'src/app/common/api/referral/referral.api.service';
import { TimetableAPIService } from 'src/app/common/api/timetable/timetable.api.service';
import { ExaminationAPIService } from 'src/app/common/api/examination/examination.api.service';
import { CreateReferralResponseDto } from 'src/app/common/api/referral/referral.api.interface';
import * as moment from 'moment';
import { ListTimeTableResDto } from 'src/app/common/api/timetable/timetable.api.interface';
import { PatientResDto } from 'src/app/common/api/patient/patient.api.interface';
import { DocumentAPIService } from 'src/app/common/api/document/document.api.service';
import { ListExaminationResponseDto } from 'src/app/common/api/examination/examination.api.interface';
import { PatientDeleteConfirmDialogComponent } from './patient-delete-confirm-dialog/patient-delete-confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { PatientUpdateDialogComponent } from './patient-update-dialog/patient-update-dialog.component';
import { PatientDocumentReceptionistDialogComponent } from './patient-document-list-dialog/patient-document-list-dialog.component';

@Component({
  selector: 'app-receptionist-dashboard',
  templateUrl: './receptionist-dashboard.component.html',
  styleUrls: ['./receptionist-dashboard.component.scss'],
})
export class ReceptionistDashboardComponent implements OnInit {
  @ViewChild(MatHorizontalStepper) stepper: MatHorizontalStepper;
  doctorsTimetableData$: BehaviorSubject<
    ListTimeTableResDto[]
  > = new BehaviorSubject([]);
  isNewPatient = true;
  appointmentListStartDate: string;
  appointmentListEndDate: string;

  dashBoardItem = {
    title: 'Időpontok listázása',
    link: '/receptionist/patient/appointments-list-update',
  };
  file: any;

  firstFormGroup: FormGroup;
  firstStepResponse: {
    id: string;
    name: string;
    address: string;
    tajNumber: string;
    idCardNumber: string;
  };

  secondFormGroup: FormGroup;
  clinicClassMappedToIdAndName$: Observable<ClinicClassMappedToNameAndId[]>;

  thirdFormGroup: FormGroup;
  @Input() examinationName: string;
  examinations$: BehaviorSubject<
    ListExaminationResponseDto[]
  > = new BehaviorSubject([]);

  fourthFormGroup: FormGroup;
  users$: BehaviorSubject<AuthDto[]> = new BehaviorSubject([]);

  fifthFormGroup: FormGroup;
  fifthStepResponse: CreateReferralResponseDto;

  sixthFormGroup: FormGroup;

  seventhFormGroup: FormGroup;

  private subs: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private clinicClassService: ClinicClassAPIService,
    private authAPIService: AuthAPIService,
    private patientAPIService: PatientAPIService,
    private referralAPIService: ReferralAPIService,
    private timetableAPIService: TimetableAPIService,
    private examinationAPIService: ExaminationAPIService,
    private documentAPIService: DocumentAPIService,
    public dialog: MatDialog
  ) {}

  async ngOnInit(): Promise<void> {
    this.clinicClassMappedToIdAndName$ = this.clinicClassService.listClinicClassMappedToIdAndName();

    this.firstFormGroup = this.fb.group({
      name: ['', Validators.required],
      gender: ['', Validators.required],
      address: ['', Validators.required],
      idCardNumber: ['', Validators.required],
      tajNumber: ['', Validators.required],
    });

    this.secondFormGroup = this.fb.group({
      id: ['', Validators.required],
    });

    this.thirdFormGroup = this.fb.group({
      id: ['', Validators.required],
    });

    this.fourthFormGroup = this.fb.group({
      id: ['', [Validators.required]],
    });

    this.fifthFormGroup = this.fb.group({
      description: ['', [Validators.required]],
    });

    this.sixthFormGroup = this.fb.group({
      date: ['', [Validators.required]],
      startTimeOfSession: ['', [Validators.required]],
      endTimeOfSession: ['', [Validators.required]],
    });

    this.seventhFormGroup = this.fb.group({
      name: ['', [Validators.required]],
      description: ['', []],
      isAvailableAlways: [false, []],
    });

    this.subs.push(
      this.secondFormGroup
        .get('id')
        .valueChanges.pipe(filter((x) => !!x))
        .subscribe(async (id) => {
          await this.listExamination(id);
          await this.listDoctorsByClinicId(id);
        }),
      this.fourthFormGroup
        .get('id')
        .valueChanges.pipe(
          filter((x) => !!x),
          distinctUntilChanged()
        )
        .subscribe(async (doctorId) => await this.getAppointmentsByDoctorId())
    );
  }

  /** FLOW STEP - 1
   * 1. lepes (patient) /api/receptionist/register
   * <- request (CreatePatientDto)
   * name: string;
   * gender: string;
   * address: string;
   * idCardNumber: string;
   * tajNumber: string;
   * -> response (CoreDto)
   */

  async createPatient(
    formGroup: FormGroup,
    formGroupDirective: FormGroupDirective
  ) {
    if (!this.firstFormGroup.valid) {
      return;
    }
    try {
      const response = await this.patientAPIService
        .createPatient(formGroup.value)
        .toPromise();
      if (response.ok) {
        this.firstStepResponse = response.body;
        this.stepper.next();
      }
    } catch (e) {
      console.log(e);
      this.snackBar.open('Sikertelen páciens felvétel', 'Rendben');
    }
  }
  async openPatientConfirmDeleteDialog() {
    const dialogRef = this.dialog.open(PatientDeleteConfirmDialogComponent, {
      data: {
        id: this.firstStepResponse.id,
        name: this.firstStepResponse.name,
      },
    });

    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.deletePatient(this.firstStepResponse.id);
      });
  }
  async openPatientUpdateDialog() {
    const dialogRef = this.dialog.open(PatientUpdateDialogComponent, {
      data: this.firstStepResponse,
    });

    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.reset();
      });
  }

  async deletePatient(patientId: string) {
    try {
      const response = await this.patientAPIService
        .deletePatient(patientId)
        .toPromise();
      if (response.ok) {
        this.snackBar.open('sikeres torles', 'Rendben');
        this.reset();
      }
    } catch (error) {
      this.snackBar.open('Sikertelen torles', 'Rendben');
    }
  }

  async listExamination(clinicId: string) {
    try {
      const body = await this.examinationAPIService
        .listExamination(clinicId)
        .pipe(map((r) => (r.body ? r.body : [])))
        .toPromise();

      if (body) {
        this.examinations$.next(body);
      }
    } catch (error) {}
  }

  async listDoctorsByClinicId(clinicId: string) {
    try {
      const body = await this.authAPIService
        .listDoctorsByClinicId(clinicId)
        .pipe(map((r) => (r.body ? r.body : [])))
        .toPromise();

      if (body) {
        this.users$.next(body);
      }
    } catch (error) {
      this.snackBar.open('Sikertelen orvos kiválasztás', 'Rendben');
    }
  }

  async createReferral(
    form: FormGroup,
    formGroupDirective: FormGroupDirective
  ) {
    try {
      const body = {
        description: form.get('description').value,
        examinationId: this.thirdFormGroup.get('id').value,
        patientId: this.firstStepResponse.id,
      };
      const response = await this.referralAPIService
        .createReferral(body)
        .toPromise();
      if (response.ok) {
        this.fifthStepResponse = response.body;
        this.stepper.next();
      }
    } catch (e) {
      this.snackBar.open('Sikertelen beutaló felvétel', 'Rendben');
      console.log(e);
    }
  }
  openPatientFilesDialogComponent() {
    this.dialog.open(PatientDocumentReceptionistDialogComponent, {
      data: this.firstStepResponse,
    });
  }
  async createAppointment(
    form: FormGroup,
    formGroupDirective: FormGroupDirective
  ) {
    if (form.invalid) {
      return;
    }
    try {
      const date = new Date(form.get('date').value);
      const startTimeOfSession = form
        .get('startTimeOfSession')
        .value.split(':');
      const endTimeOfSession = form.get('endTimeOfSession').value.split(':');
      const startOfSession = new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        startTimeOfSession[0],
        startTimeOfSession[1]
      );
      const endOfSession = new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        endTimeOfSession[0],
        endTimeOfSession[1]
      );
      const body = {
        authId: this.fourthFormGroup.get('id').value as string,
        referralId: this.fifthStepResponse.id,
        startOfSession,
        endOfSession,
      };
      const response = await this.timetableAPIService
        .createAppointment(body)
        .toPromise();
      if (response.ok) {
        const newAppointment = (response.body as unknown) as ListTimeTableResDto;
        newAppointment.patientName = this.firstFormGroup.get('name').value;
        this.examinations$['_value'].forEach((element) => {
          if (element.id == this.thirdFormGroup.get('id').value) {
            newAppointment.examinationName = element.name;
          }
        });
        this.doctorsTimetableData$.next([
          ...this.doctorsTimetableData$.value,
          newAppointment,
        ]);
        this.snackBar.open('Sikeres időpont felvétel', 'Rendben');
      }
    } catch (e) {
      this.snackBar.open('Sikertelen időpont felvétel', 'Rendben');
      console.log(e);
    }
  }

  async getAppointmentsByDoctorId(
    startDate = moment().day(1).startOf('day').toISOString(),
    endDate = moment().day(2).add(7, 'days').endOf('day').toISOString()
  ) {
    this.appointmentListStartDate = startDate;
    this.appointmentListEndDate = endDate;
    try {
      const response = await this.timetableAPIService
        .getAppointmentsByDoctorId(
          this.fourthFormGroup.get('id').value,
          startDate,
          endDate
        )
        .toPromise();
      if (response.ok) {
        this.doctorsTimetableData$.next(response.body);
      }
    } catch (error) {
      console.log(error);
    }
  }

  public onPatientSearched(patient: PatientResDto) {
    this.isNewPatient = false;
    this.firstStepResponse = patient;
    this.firstFormGroup.setValue({
      name: patient.name,
      gender: patient.gender,
      address: patient.address,
      idCardNumber: patient.idCardNumber,
      tajNumber: patient.tajNumber,
    });
    ['name', 'gender', 'address', 'idCardNumber', 'tajNumber'].forEach(
      (controlName) => {
        this.firstFormGroup.get(controlName).disable();
      }
    );
  }

  public async onTimeTableIntervalChange({ startDate, endDate }) {
    this.getAppointmentsByDoctorId(startDate, endDate);
  }

  public async uploadDocument() {
    if (this.seventhFormGroup.invalid) {
      return;
    }
    try {
      const form = new FormData();
      form.append('name', this.seventhFormGroup.get('name').value);
      form.append(
        'description',
        this.seventhFormGroup.get('description').value
      );
      form.append(
        'isAvailableAlways',
        this.seventhFormGroup.get('isAvailableAlways').value
      );
      form.append('file', this.file);
      form.append('userId', this.fourthFormGroup.get('id').value);
      const response = await this.documentAPIService
        .upload(form, this.firstStepResponse.id)
        .toPromise();
      if (response.ok) {
        this.snackBar.open('Sikeres fájl feltöltés!', 'Rendben');
      }
    } catch (e) {
      this.snackBar.open('Sikertelen fájl feltöltés!', 'Rendben');
    }
  }

  onFileSelected(e) {
    this.file = e.target.files[0];
  }

  reset() {
    this.isNewPatient = true;
    this.doctorsTimetableData$.next([]);
    this.stepper.reset();
    /** 1 */
    ['name', 'gender', 'address', 'idCardNumber', 'tajNumber'].forEach(
      (controlName) => {
        this.firstFormGroup.get(controlName).setValue('');
        this.firstFormGroup.get(controlName).enable();
      }
    );
    this.setFormControlErrorsNull(this.firstFormGroup);

    /** 2 */
    ['id'].forEach((controlName) => {
      this.secondFormGroup.get(controlName).setValue('');
    });
    this.setFormControlErrorsNull(this.secondFormGroup);

    /** 3 */
    ['id'].forEach((controlName) => {
      this.thirdFormGroup.get(controlName).setValue('');
    });
    this.setFormControlErrorsNull(this.thirdFormGroup);

    /** 4 */
    ['id'].forEach((controlName) => {
      this.fourthFormGroup.get(controlName).setValue('');
    });
    this.setFormControlErrorsNull(this.fourthFormGroup);

    /** 5 */
    ['description'].forEach((controlName) => {
      this.fifthFormGroup.get(controlName).setValue('');
    });
    this.setFormControlErrorsNull(this.fifthFormGroup);

    /** 6 */
    ['date', 'startTimeOfSession', 'endTimeOfSession'].forEach(
      (controlName) => {
        this.sixthFormGroup.get(controlName).setValue('');
      }
    );
    this.setFormControlErrorsNull(this.sixthFormGroup);

    /** 7 */
    // TODO
  }

  setFormControlErrorsNull(form: FormGroup) {
    Object.keys(form.controls).forEach((key) => {
      form.controls[key].setErrors(null);
    });
  }
}
