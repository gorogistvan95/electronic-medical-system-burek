import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PatientResDto } from 'src/app/common/api/patient/patient.api.interface';
import { PatientAPIService } from 'src/app/common/api/patient/patient.api.service';

@Component({
  selector: 'app-patient-class-update-dialog',
  templateUrl: './patient-update-dialog.component.html',
  styleUrls: ['./patient-update-dialog.component.scss'],
})
export class PatientUpdateDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PatientResDto,
    public dialogRef: MatDialogRef<PatientUpdateDialogComponent>,
    private fb: FormBuilder,
    public patientAPIService: PatientAPIService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: [this.data.name, []],
      address: [this.data.address, []],
      idCardNumber: [this.data.idCardNumber, []],
      tajNumber: [this.data.tajNumber, []],
    });
  }

  async updatePatient() {
    try {
      console.log(this.form.value);
      const response = await this.patientAPIService
        .updatePatient(this.data.id, this.form.value)
        .toPromise();
      if (response.ok) {
        this.dialogRef.close(true);
        this.snackBar.open('Sikeres módosítás', 'Rendben');
      }
    } catch (error) {
      this.snackBar.open('Sikertelen módosítás', 'Rendben');
      this.dialogRef.close();
    }
  }
}
