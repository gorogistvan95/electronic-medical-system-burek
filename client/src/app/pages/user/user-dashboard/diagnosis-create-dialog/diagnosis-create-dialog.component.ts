import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { debounce, filter as _filter } from 'lodash';
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import {
  debounceTime,
  filter,
  map,
  shareReplay,
  startWith,
  switchMapTo,
} from 'rxjs/operators';

import { DiagnosisApiService } from 'src/app/common/api/diagnosis/diagnosis..api.service';
import {
  ListBnoDiagnosisResponseDto,
  ListDiagnosisResponseDto,
  ListMappedBnoDiagnosisResponseDto,
  UpdateDiagnosisReqDto,
} from 'src/app/common/api/diagnosis/diagnosis.api.interface';

@Component({
  selector: 'app-diagnosis-create-dialog',
  templateUrl: './diagnosis-create-dialog.component.html',
  styleUrls: ['./diagnosis-create-dialog.component.scss'],
})
export class DiagnosisCreateDialogComponent implements OnInit, OnDestroy {
  form: FormGroup;
  diagnosisInterFace: ListDiagnosisResponseDto;
  diagnosisBno$: BehaviorSubject<
    ListMappedBnoDiagnosisResponseDto[]
  > = new BehaviorSubject([]);
  filteredOptions$: Observable<ListMappedBnoDiagnosisResponseDto[]>;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string,
    public dialogRef: MatDialogRef<DiagnosisCreateDialogComponent>,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private diagnosisApiService: DiagnosisApiService
  ) {}

  async ngOnInit(): Promise<void> {
    this.form = this.fb.group({
      code: ['', [Validators.required]],
      name: [''],
      description: [''],
      treatment: [''],
    });
    this.filteredOptions$ = combineLatest([
      this.form.get('code').valueChanges,
      this.diagnosisBno$.asObservable(),
    ]).pipe(
      startWith(''),
      filter(([x]) => !!x),
      debounceTime(400),
      map(([x, value]) => this._filter(value)),
      startWith([])
    );
    await this.loadData();
  }
  ngOnDestroy() {}
  async loadData() {
    try {
      const response = await this.diagnosisApiService
        .listMappedBnoDiagnosises()
        .toPromise();
      this.diagnosisBno$.next(response);
    } catch (err) {
      //TOODO
    }
  }
  selectedOption(event: MatAutocompleteSelectedEvent) {
    const name = this.diagnosisBno$.value.find(
      (x) => x.code === event.option.value
    ).name;
    this.form.get('name').setValue(name);
  }

  async createDiagnosis() {
    try {
      const formValues = this.form.value;
      if (this.form.invalid) {
        this.snackBar.open('Hibásan kitöltött mezők!', 'Rendben');
        return;
      }
      const response = await this.diagnosisApiService
        .createDiagnosis({ patientId: this.data, ...formValues })
        .toPromise();

      if (response.ok) {
        this.snackBar.open('Sikeres diagnózis létrehozás', 'Rendben');
        this.dialogRef.close(true);
      }
    } catch (e) {
      this.snackBar.open('Sikertelen diagnózis létrehozás', 'Rendben');
      this.dialogRef.close();
    }
  }
  private _filter(
    value: ListMappedBnoDiagnosisResponseDto[]
  ): ListMappedBnoDiagnosisResponseDto[] {
    const search = this.form.get('code').value;
    let maxItems = 0;
    const resultsArray = [];
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < value.length; i++) {
      const x = value[i];
      const name = `${x.code} - ${x.name}`;
      if (name.includes(search)) {
        resultsArray.push({ ...x, name });
        maxItems++;
      }
      if (maxItems > 50) {
        break;
      }
    }
    return resultsArray;
  }
}
