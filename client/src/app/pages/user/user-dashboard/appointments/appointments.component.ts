import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, combineLatest, Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { PatientDetails } from 'src/app/common/api/patient/patient.api.interface';
import { PatientAPIService } from 'src/app/common/api/patient/patient.api.service';
import { PatientDataAndVisitDetails } from '../user-dashboard.component';

@Component({
  selector: 'app-appointments',
  templateUrl: 'appointments.component.html',
  styleUrls: ['appointments.component.scss'],
})
export class AppointmentsComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  private subs: Subscription[] = [];
  public visitDetails$: BehaviorSubject<
    PatientDataAndVisitDetails[]
  > = new BehaviorSubject([]);
  private _referralResponse$: BehaviorSubject<
    PatientDetails[]
  > = new BehaviorSubject([]);

  constructor(
    private fb: FormBuilder,
    private patientAPIService: PatientAPIService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({ start: [''], end: [''] });
    this.subs.push(
      combineLatest([
        this.form.get('start').valueChanges,
        this.form.get('end').valueChanges,
      ]).subscribe(([start, end]) => {
        this.visitDetails$.next(
          this.mapResponseToPatientDataAndVisitDetails(
            this._referralResponse$.value
          )
        );
      })
    );
    this.loadAppointments();
  }
  ngOnDestroy(): void {
    this.subs.forEach((s) => s.unsubscribe());
  }

  async loadAppointments() {
    try {
      const response = await this.patientAPIService
        .listPatientandRelatedObjectsByDoctor()
        .toPromise();
      this._referralResponse$.next(response.body);
    } catch (error) {
      this.snackBar.open('Hiba történt a listázáskor', 'Rendben');
    }
  }

  private mapResponseToPatientDataAndVisitDetails(response: PatientDetails[]) {
    const startOfToday = this.form.get('start').value;
    const endOfToday = this.form.get('end').value;
    const patientDataAndVisitDetails: PatientDataAndVisitDetails[] = [];
    response.forEach(({ referral, patient }) => {
      referral.forEach(({ timetable, referral, examinationName }) => {
        timetable.forEach((t) => {
          const startOfSession = new Date((t as any).startOfSession);
          if (startOfToday < startOfSession && startOfSession < endOfToday) {
            patientDataAndVisitDetails.push({
              address: patient.address,
              createdAt: patient.createdAt,
              gender: patient.gender,
              id: patient.id,
              idCardNumber: patient.idCardNumber,
              name: patient.name,
              tajNumber: patient.tajNumber,
              updatedAt: patient.updatedAt,
              // referral
              referralId: referral.id,
              referralDescription: referral.description,
              examinationId: referral.examinationId,
              examinationName,
              // timetable
              startOfSession: (t as any).startOfSession,
              endOfSession: (t as any).endOfSession,
            });
          }
        });
      });
    });
    return patientDataAndVisitDetails.sort((a, b) =>
      a.startOfSession < b.startOfSession
        ? -1
        : a.startOfSession > b.startOfSession
        ? 1
        : 0
    );
  }
}
