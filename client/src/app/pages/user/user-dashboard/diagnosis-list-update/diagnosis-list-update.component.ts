import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { filter, map, shareReplay } from 'rxjs/operators';
import { AuthAPIService } from 'src/app/common/api/auth/auth-api.service';
import { ClinicClassMappedToNameAndId } from 'src/app/common/api/clinic-class/clinic-class.api.interface';
import { ClinicClassAPIService } from 'src/app/common/api/clinic-class/clinic-class.api.service';
import { DiagnosisApiService } from 'src/app/common/api/diagnosis/diagnosis..api.service';
import { ListDiagnosisResponseDto } from 'src/app/common/api/diagnosis/diagnosis.api.interface';
import { MustMatch } from 'src/app/common/validators/must-match.validator';
import { PatientIdPassingService } from 'src/app/services/patient-id-passing-service';
import { filter as _filter } from 'lodash';
import { map as _map } from 'lodash';
import { DiagnosisDeleteConfirmDialogComponent } from './diagnosis-delete-confirm-dialog/diagnosis-delete-confirm-dialog.component';
import { DiagnosisUpdateDialogComponent } from './diagnosis-update-dialog/diagnosis-update-dialog.component';
@Component({
  selector: 'app-diagnosis-list-update',
  templateUrl: './diagnosis-list-update.component.html',
  styleUrls: ['./diagnosis-list-update.component.scss'],
})
export class DiagnosisListUpdateComponent implements OnInit {
  displayedColumns: string[] = [
    'code',
    'name',
    'description',
    'treatment',
    'createdAt',
    'updatedAt',
    'delete',
    'update',
  ];
  diagnosises$: BehaviorSubject<
    ListDiagnosisResponseDto[]
  > = new BehaviorSubject([]);
  private subs: Subscription[] = [];
  patientId: string = PatientIdPassingService.getPatientId();
  constructor(
    private diagnosisService: DiagnosisApiService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private patientIdPassingService: PatientIdPassingService
  ) {}

  async ngOnInit(): Promise<void> {
    this.subs.push(
      this.diagnosisService
        .listDiagnosises(PatientIdPassingService.getPatientId())
        .pipe(map((r) => r.body))
        .subscribe((diagnosises) => {
          this.diagnosises$.next(diagnosises);
        })
    );
  }

  ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }

  openDiagnosisDeleteConfirmDialog(id: string, name: string) {
    const dialogRef = this.dialog.open(DiagnosisDeleteConfirmDialogComponent, {
      data: { id, name },
    });
    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.deleteDiagnosis(id);
      });

    this.subs.push(
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.diagnosises$.next(
            _filter(this.diagnosises$.value, (u) => u.id !== result)
          );
        }
      })
    );
  }
  async deleteDiagnosis(diagnosisId: string) {
    try {
      const response = await this.diagnosisService
        .deleteDiagnosis(diagnosisId)
        .toPromise();
      if (response.ok) {
        await this.loadUsers();
        this.snackBar.open('Sikeres torles', 'Rendben');
      }
    } catch (error) {
      this.snackBar.open('Sikertelen torles', 'Rendben');
    }
  }

  async loadUsers() {
    try {
      const response = await this.diagnosisService
        .listDiagnosises(this.patientId)
        .toPromise();
      if (response.ok) {
        this.diagnosises$.next(response.body);
      }
    } catch (e) {
      this.snackBar.open('Nem tudtuk betolteni', 'Rendben');
    }
  }

  async openDiagnosisUpdateDialog(diagonsisData: ListDiagnosisResponseDto) {
    const dialogRef = this.dialog.open(DiagnosisUpdateDialogComponent, {
      data: diagonsisData,
    });
    dialogRef
      .afterClosed()
      .pipe(filter((x) => x))
      .subscribe((o) => {
        this.loadUsers();
      });

    this.subs.push(
      dialogRef.afterClosed().subscribe((result: ListDiagnosisResponseDto) => {
        if (result) {
          this.diagnosises$.next(
            _map(this.diagnosises$.value, (u) =>
              u.id !== result.id ? u : result
            )
          );
        }
      })
    );
  }
}
