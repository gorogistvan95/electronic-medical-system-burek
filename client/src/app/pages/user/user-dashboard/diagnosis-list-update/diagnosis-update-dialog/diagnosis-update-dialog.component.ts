import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DiagnosisApiService } from 'src/app/common/api/diagnosis/diagnosis..api.service';

import {
  ListDiagnosisResponseDto,
  UpdateDiagnosisReqDto,
} from 'src/app/common/api/diagnosis/diagnosis.api.interface';

@Component({
  selector: 'app-examination-update-dialog',
  templateUrl: './diagnosis-update-dialog.component.html',
  styleUrls: ['./diagnosis-update-dialog.component.scss'],
})
export class DiagnosisUpdateDialogComponent implements OnInit {
  form: FormGroup;
  updateDiagnosisInterFace: UpdateDiagnosisReqDto;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ListDiagnosisResponseDto,
    public dialogRef: MatDialogRef<DiagnosisUpdateDialogComponent>,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    private diagnosisApiService: DiagnosisApiService
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      code: [this.data.code, [Validators.required, Validators.minLength(1)]],
      name: [this.data.name, [Validators.required, Validators.minLength(1)]],
      description: [this.data.description, []],
      treatment: [this.data.treatment, []],
    });
  }
  async updateDiagnosis() {
    try {
      const formValues = this.form.value;
      const filteredVormValue = await Object.entries(formValues).reduce(
        (a, [k, v]) => (v ? ((a[k] = v), a) : a),
        {}
      );
      filteredVormValue;
      if (this.form.invalid) {
        this.snackBar.open('Hibásan kitöltött mezők!', 'Rendben');
        return;
      }
      const response = await this.diagnosisApiService
        .updateDiagnosis(this.data.id, filteredVormValue)
        .toPromise();

      if (response.ok) {
        this.snackBar.open('Sikeres diagnózis módosítás', 'Rendben');
        this.dialogRef.close(true);
      }
    } catch (e) {
      this.snackBar.open('Sikertelen diagnózis módosítás', 'Rendben');
      this.dialogRef.close();
    }
  }
}
