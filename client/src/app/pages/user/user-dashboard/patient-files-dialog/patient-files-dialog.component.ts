import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthAPIService } from 'src/app/common/api/auth/auth-api.service';
import { ListDocumentResDto } from 'src/app/common/api/document/document.api.interface';
import { DocumentAPIService } from 'src/app/common/api/document/document.api.service';
import { AuthService } from 'src/app/services/auth.service';
import { PatientDataAndVisitDetails } from '../user-dashboard.component';

@Component({
  selector: 'app-patient-files-dialog',
  templateUrl: 'patient-files-dialog.component.html',
  styleUrls: ['patient-files-dialog.component.scss'],
})
export class PatientFilesDialogComponent implements OnInit {
  public files$: BehaviorSubject<ListDocumentResDto[]> = new BehaviorSubject(
    []
  );
  public file;
  public form: FormGroup;
  public authId$ = new Observable<string>();
  displayedColumns: string[] = [
    'name',
    'description',
    'size',
    'createdAt',
    'download',
    'delete',
  ];
  constructor(
    public dialogRef: MatDialogRef<PatientFilesDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public patientDataAndVisitDetails: PatientDataAndVisitDetails,
    public documentAPIService: DocumentAPIService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    public authService: AuthService
  ) {}

  async ngOnInit() {
    this.form = this.fb.group({
      name: ['', [Validators.required]],
      description: ['', []],
      isAvailableAlways: [false, []],
    });

    await this.loadFiles();
    this.authId$ = this.authService.authId$;
  }

  async deleteDocument(documentId: string) {
    const response = await this.documentAPIService
      .deleteDocument(documentId)
      .toPromise();
    if (response.ok) {
      this.loadFiles();
      this.snackBar.open('Sikeres fájl törlés!', 'Rendben');
    }
  }
  catch(e) {
    console.log(e);
    this.snackBar.open('Sikertelen fájl törlés!', 'Rendben');
  }

  async loadFiles() {
    try {
      const response = await this.documentAPIService
        .listPatientDocumentsByDoctor(this.patientDataAndVisitDetails.id)
        .toPromise();
      this.files$.next(response.body);
    } catch (e) {
      this.snackBar.open('Hiba történt a fájlok listázásakor', 'Rendben');
    }
  }

  public async uploadDocument() {
    if (this.form.invalid) {
      return;
    }
    try {
      const form = new FormData();
      form.append('name', this.form.get('name').value);
      form.append('description', this.form.get('description').value);
      form.append(
        'isAvailableAlways',
        this.form.get('isAvailableAlways').value
      );
      form.append('file', this.file);
      form.append('userId', this.patientDataAndVisitDetails.id);
      const response = await this.documentAPIService
        .upload(form, this.patientDataAndVisitDetails.id)
        .toPromise();
      if (response.ok) {
        this.files$.next([
          ...this.files$.value,
          response.body as ListDocumentResDto,
        ]);
        this.snackBar.open('Sikeres fájl feltöltés!', 'Rendben');
      }
    } catch (e) {
      console.log(e);
      this.snackBar.open('Sikertelen fájl feltöltés!', 'Rendben');
    }
  }

  onFileSelected(e) {
    this.file = e.target.files[0];
  }
}
