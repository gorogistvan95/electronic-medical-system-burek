import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, interval, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { PatientDetails } from 'src/app/common/api/patient/patient.api.interface';
import { PatientAPIService } from 'src/app/common/api/patient/patient.api.service';
import { map as _map } from 'lodash';
import { ListTimeTableResDto } from 'src/app/common/api/timetable/timetable.api.interface';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { PatientFilesDialogComponent } from './patient-files-dialog/patient-files-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DiagnosisCreateDialogComponent } from './diagnosis-create-dialog/diagnosis-create-dialog.component';
import { PatientIdPassingService } from 'src/app/services/patient-id-passing-service';
import { AuthService } from 'src/app/services/auth.service';

export interface PatientDataAndVisitDetails {
  address: string;
  createdAt: string;
  gender: 'male' | 'female';
  id: string;
  idCardNumber: string;
  name: string;
  tajNumber: string;
  updatedAt: string;
  // referral
  referralId: string;
  referralDescription: string;
  examinationId: string;
  examinationName: string;
  // timetable
  startOfSession: string;
  endOfSession: string;
}

@Component({
  selector: 'app-user-dashboard',
  templateUrl: 'user-dashboard.component.html',
  styleUrls: ['user-dashboard.component.scss'],
})
export class UserDashboardComponent implements OnInit, OnDestroy {
  private _referralResponse$: BehaviorSubject<
    PatientDetails[]
  > = new BehaviorSubject([]);
  public timeTableData$: Observable<ListTimeTableResDto[]>;
  public patientDataAndVisitDetails$: Observable<PatientDataAndVisitDetails[]>;
  public startDate;
  public actualDate;
  public endDate;
  private subs: Subscription[] = [];

  constructor(
    private patientAPIService: PatientAPIService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    public authService: AuthService
  ) {}

  async ngOnInit() {
    this.startDate = moment().day(1).startOf('day').toISOString();
    this.actualDate = new Date();
    this.endDate = moment().day(2).add(7, 'days').endOf('day').toISOString();
    this.timeTableData$ = this._referralResponse$
      .asObservable()
      .pipe(map(this.mapResponseToTimeTableData));
    this.patientDataAndVisitDetails$ = this._referralResponse$
      .asObservable()
      .pipe(map(this.mapResponseToPatientDataAndVisitDetails));
    await this.loadData();
    this.subs.push(
      interval(30000).subscribe(async () => {
        await this.loadData();
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach((s) => s.unsubscribe());
  }

  async loadData() {
    try {
      const response = await this.patientAPIService
        .listPatientandRelatedObjectsByDoctor()
        .toPromise();
      this._referralResponse$.next(response.body);
    } catch (error) {
      this.snackBar.open('Hiba történt a listázáskor', 'Rendben');
    }
  }

  private mapResponseToTimeTableData(response: PatientDetails[]) {
    const timeTableData: ListTimeTableResDto[] = [];
    response.forEach(({ referral, patient }) => {
      // tslint:disable-next-line: no-shadowed-variable
      referral.forEach(({ timetable, referral, examinationName }) => {
        timetable.forEach((t) => {
          timeTableData.push({
            doctorId: (t as any).doctorId,
            startOfSession: (t as any).startOfSession,
            endOfSession: (t as any).endOfSession,
            patientName: patient.name,
            examinationName,
            referralId: '',
            id: (t as any).id,
          });
        });
      });
    });
    return timeTableData;
  }

  private mapResponseToPatientDataAndVisitDetails(response: PatientDetails[]) {
    const startOfToday = new Date(moment().startOf('day').toISOString());
    const endOfToday = new Date(moment().endOf('day').toISOString());
    const patientDataAndVisitDetails: PatientDataAndVisitDetails[] = [];
    response.forEach(({ referral, patient }) => {
      referral.forEach(({ timetable, referral, examinationName }) => {
        timetable.forEach((t) => {
          const startOfSession = new Date((t as any).startOfSession);
          if (startOfToday < startOfSession && startOfSession < endOfToday) {
            patientDataAndVisitDetails.push({
              address: patient.address,
              createdAt: patient.createdAt,
              gender: patient.gender,
              id: patient.id,
              idCardNumber: patient.idCardNumber,
              name: patient.name,
              tajNumber: patient.tajNumber,
              updatedAt: patient.updatedAt,
              // referral
              referralId: referral.id,
              referralDescription: referral.description,
              examinationId: referral.examinationId,
              examinationName,
              // timetable
              startOfSession: (t as any).startOfSession,
              endOfSession: (t as any).endOfSession,
            });
          }
        });
      });
    });
    return patientDataAndVisitDetails.sort((a, b) =>
      a.startOfSession < b.startOfSession
        ? -1
        : a.startOfSession > b.startOfSession
        ? 1
        : 0
    );
  }
}
