import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class PatientIdPassingService {
  // tslint:disable-next-line: variable-name
  static _patientId: string;

  set setPatientId(newValue: string) {
    PatientIdPassingService._patientId = newValue;
  }

  static getPatientId(): string {
    return this._patientId;
  }
}
