import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

export interface IParsedJwtToken {
  id: string;
  username: string;
  role: 'ADMIN' | 'USER' | 'RECEPTIONIST';
  isDiagnosis: boolean;
  iat: number;
}

@Injectable({ providedIn: 'root' })
export class AuthService {
  private _parsedToken$: BehaviorSubject<IParsedJwtToken> = new BehaviorSubject(
    undefined
  );

  public isAdmin$: Observable<boolean>;
  public isUser$: Observable<boolean>;
  public isDiagnosis$: Observable<boolean>;
  public isReceptionist$: Observable<boolean>;
  public isAuthenticated$: Observable<boolean>;
  public userName$: Observable<string>;
  public authId$: Observable<string>;

  constructor(private router: Router) {
    const getRole = (
      token: IParsedJwtToken,
      role: 'ADMIN' | 'USER' | 'RECEPTIONIST'
    ) => !!token && role === token.role;
    this.isAdmin$ = this._parsedToken$.pipe(
      map((token) => getRole(token, 'ADMIN')),
      startWith(false)
    );
    this.isUser$ = this._parsedToken$.pipe(
      map((token) => getRole(token, 'USER')),
      startWith(false)
    );
    this.isReceptionist$ = this._parsedToken$.pipe(
      map((token) => getRole(token, 'RECEPTIONIST')),
      startWith(false)
    );
    this.isAuthenticated$ = this._parsedToken$.pipe(map((x) => !!x));
    this.userName$ = this._parsedToken$.pipe(
      map((token) => (token && token.username ? token.username : '')),
      startWith('')
    );
    this.isAuthenticated$ = this._parsedToken$.pipe(map((x) => !!x));
    this.authId$ = this._parsedToken$.pipe(
      map((token) => (token && token.id ? token.id : '')),
      startWith('')
    );
    this.isDiagnosis$ = this._parsedToken$.pipe(
      map((token) => (token && token.isDiagnosis ? token.isDiagnosis : false)),
      startWith(false)
    );
  }

  init() {
    if (this.getParsedToken()) {
      this._parsedToken$.next(this.getParsedToken());
    }
  }

  setToken(token: string) {
    try {
      window.localStorage.setItem('TOKEN', token);
    } catch (e) {
      console.log(e);
    }
  }

  getStringToken() {
    try {
      return window.localStorage.getItem('TOKEN');
    } catch (error) {
      return undefined;
    }
  }

  getParsedToken() {
    try {
      const stringToken = window.localStorage.getItem('TOKEN');
      const decoded = jwt_decode(stringToken);
      return decoded;
    } catch (e) {
      return null;
    }
  }

  login(token: string) {
    this.setToken(token);
    const parsedToken: IParsedJwtToken = this.getParsedToken();
    this._parsedToken$.next(parsedToken);
    if ('ADMIN' === parsedToken.role) {
      this.router.navigate(['admin', 'dashboard']);
    } else if ('RECEPTIONIST' === parsedToken.role) {
      this.router.navigate(['receptionist', 'dashboard']);
    } else if ('USER' === parsedToken.role) {
      this.router.navigate(['user', 'dashboard']);
    }
  }

  logout() {
    window.localStorage.removeItem('TOKEN');
    this._parsedToken$.next(undefined);
    this.router.navigate(['login']);
  }
}
