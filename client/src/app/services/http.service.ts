import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class HttpService {
  get defaultOptions() {
    return {
      observe: 'response',
      headers: {
        Authorization: !!this.authService.getStringToken()
          ? `Bearer ${this.authService.getStringToken()}`
          : '',
      },
    };
  }

  constructor(private http: HttpClient, private authService: AuthService) {}

  public post<T>(url: string, body: any, options?: any): Observable<T | any> {
    return this.http.post(`${url}`, body, {
      ...this.defaultOptions,
      ...options,
    });
  }

  public put<T>(url: string, body: any, options?: any): Observable<T | any> {
    return this.http.put(`${url}`, body, {
      ...this.defaultOptions,
      ...options,
    });
  }
  public delete<T>(url: string, options?: any): Observable<T | any> {
    return this.http.delete(`${url}`, {
      ...this.defaultOptions,
      ...options,
    });
  }

  public get<T>(url: string, options?: any): Observable<T | any> {
    return this.http.get(`${url}`, {
      ...this.defaultOptions,
      ...options,
    });
  }
}
