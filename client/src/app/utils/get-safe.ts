export function getSafe(f: any) {
  try {
    return f();
  } catch (e) {
    return undefined;
  }
}
