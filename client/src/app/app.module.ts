import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { AdminDashboardComponent } from './pages/admin/admin-dashboard/admin-dashboard.component';
import { UserCreateComponent } from './pages/admin/user-create/user-create.component';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ClinicClassCreateComponent } from './pages/admin/clinic-class-create/clinic-class-create.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ExaminationCreateComponent } from './pages/admin/examination-create/examination-create.component';
import { UserUpdateComponent } from './pages/admin/user-update/user-update.component';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { UserDeleteConfirmDialogComponent } from './pages/admin/user-update/user-delete-confirm-dialog/user-delete-confirm-dialog.component';
import { UserUpdateDialogComponent } from './pages/admin/user-update/user-update-dialog/user-update-dialog.component';
import { ClinicClassUpdateComponent } from './pages/admin/clinic-class-update/clinic-class-update.component';
import { ClinicClassUpdateDialogComponent } from './pages/admin/clinic-class-update/clinic-class-update-dialog/clinic-class-update-dialog.component';
import { ClinicClassConfirmUpdateDialogComponent } from './pages/admin/clinic-class-update/clinic-class-confirm-update-dialog/clinic-class-confirm-update-dialog.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { NavbarComponent } from './common/components/navbar/navbar.component';
import { NgZeeTimeTableModule } from 'ng-zee-timetable';
import { ReceptionistDashboardComponent } from './pages/receptionist/receptionist-dashboard/receptionist-dashboard.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatRadioModule } from '@angular/material/radio';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { TimetableComponent } from './common/components/timetable/timetable.component';
import { SearchPatientComponent } from './common/components/search-patient/search-patient.component';
import { ClinicClassConfirmDeleteDialogComponent } from './pages/admin/clinic-class-update/clinic-class-confirm-delete-dialog/clinic-class-confirm-delete-dialog.component';
import { ExaminationUpdateComponent } from './pages/admin/examination-update/examination-update.component';
import { CommonModule } from '@angular/common';
import { UserDashboardComponent } from './pages/user/user-dashboard/user-dashboard.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { PatientFilesDialogComponent } from './pages/user/user-dashboard/patient-files-dialog/patient-files-dialog.component';
import { MatDividerModule } from '@angular/material/divider';
import { ExaminationUpdateDialogComponent } from './pages/admin/examination-update/examination-update-dialog/examination-update-dialog.component';
import { ExaminationDeleteConfirmDialogComponent } from './pages/admin/examination-update/examination-delete-confirm-dialog/examination-delete-confirm-dialog';
import { PatientAppointmentsListUpdateComponent } from './pages/receptionist/patient-list-update/patient-list-update.component';
import { PatientAppointmentDeleteConfirmDialogComponent } from './pages/receptionist/patient-list-update/patient-appointment-list-delete-confirm-dialog/patient-appointment-delete-confirm-dialog.component';
import { DiagnosisListUpdateComponent } from './pages/user/user-dashboard/diagnosis-list-update/diagnosis-list-update.component';
import { DiagnosisDeleteConfirmDialogComponent } from './pages/user/user-dashboard/diagnosis-list-update/diagnosis-delete-confirm-dialog/diagnosis-delete-confirm-dialog.component';
import { DiagnosisCreateDialogComponent } from './pages/user/user-dashboard/diagnosis-create-dialog/diagnosis-create-dialog.component';
import { DiagnosisUpdateDialogComponent } from './pages/user/user-dashboard/diagnosis-list-update/diagnosis-update-dialog/diagnosis-update-dialog.component';
import { PatientDeleteConfirmDialogComponent } from './pages/receptionist/receptionist-dashboard/patient-delete-confirm-dialog/patient-delete-confirm-dialog.component';
import { PatientUpdateDialogComponent } from './pages/receptionist/receptionist-dashboard/patient-update-dialog/patient-update-dialog.component';
import { AppointmentsComponent } from './pages/user/user-dashboard/appointments/appointments.component';
import { AppointmentAccordionComponent } from './common/components/appointment-accordion/appointment-accordion.component';
import { PatientDocumentReceptionistDialogComponent } from './pages/receptionist/receptionist-dashboard/patient-document-list-dialog/patient-document-list-dialog.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
@NgModule({
  declarations: [
    AppComponent,
    AppointmentsComponent,
    AppointmentAccordionComponent,
    LoginComponent,
    AdminDashboardComponent,
    AdminDashboardComponent,
    UserCreateComponent,
    ClinicClassCreateComponent,
    ClinicClassConfirmDeleteDialogComponent,
    DiagnosisUpdateDialogComponent,
    DiagnosisCreateDialogComponent,
    DiagnosisListUpdateComponent,
    DiagnosisDeleteConfirmDialogComponent,
    ExaminationCreateComponent,
    ExaminationUpdateComponent,
    ExaminationUpdateDialogComponent,
    ExaminationDeleteConfirmDialogComponent,
    UserUpdateComponent,
    UserDeleteConfirmDialogComponent,
    UserUpdateDialogComponent,
    UserDashboardComponent,
    ClinicClassUpdateComponent,
    ClinicClassUpdateDialogComponent,
    ClinicClassConfirmUpdateDialogComponent,
    NavbarComponent,
    PatientFilesDialogComponent,
    PatientAppointmentsListUpdateComponent,
    PatientDocumentReceptionistDialogComponent,
    PatientDeleteConfirmDialogComponent,
    PatientUpdateDialogComponent,
    PatientAppointmentDeleteConfirmDialogComponent,
    ReceptionistDashboardComponent,
    SearchPatientComponent,
    TimetableComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatDividerModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatInputModule,
    MatSelectModule,
    MatSnackBarModule,
    MatStepperModule,
    MatCheckboxModule,
    MatTableModule,
    MatDialogModule,
    MatToolbarModule,
    MatIconModule,
    NgZeeTimeTableModule,
    MatRadioModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
