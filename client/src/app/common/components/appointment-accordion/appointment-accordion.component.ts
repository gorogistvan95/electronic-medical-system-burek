import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { DiagnosisCreateDialogComponent } from 'src/app/pages/user/user-dashboard/diagnosis-create-dialog/diagnosis-create-dialog.component';
import { PatientFilesDialogComponent } from 'src/app/pages/user/user-dashboard/patient-files-dialog/patient-files-dialog.component';
import { PatientDataAndVisitDetails } from 'src/app/pages/user/user-dashboard/user-dashboard.component';
import { AuthService } from 'src/app/services/auth.service';
import { PatientIdPassingService } from 'src/app/services/patient-id-passing-service';

@Component({
  selector: 'app-appointment-accordion',
  templateUrl: 'appointment-accordion.component.html',
  styleUrls: ['appointment-accordion.component.scss'],
})
export class AppointmentAccordionComponent implements OnInit {
  @Input() patientDataAndVisitDetails = [];
  isDiagnosis$: Observable<boolean>;
  dashBoardItem = {
    title: 'Diagnózis listázása',
    link: '/user/diagnosises',
  };
  constructor(
    private authService: AuthService,
    private dialog: MatDialog,
    private patientIdPassingService: PatientIdPassingService
  ) {}

  ngOnInit() {
    this.isDiagnosis$ = this.authService.isDiagnosis$;
  }

  openPatientFilesDialogComponent(
    patientDataAndVisitDetails: PatientDataAndVisitDetails
  ) {
    this.dialog.open(PatientFilesDialogComponent, {
      data: patientDataAndVisitDetails,
    });
  }

  openCreateDiagnosisComponent(patientId: string) {
    this.dialog.open(DiagnosisCreateDialogComponent, {
      data: patientId,
      width: '60%',
    });
  }

  setPatientValue(patientId: string) {
    this.patientIdPassingService.setPatientId = patientId;
  }
}
