import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PatientIdPassingService } from 'src/app/services/patient-id-passing-service';
import { PatientResDto } from '../../api/patient/patient.api.interface';
import { PatientAPIService } from '../../api/patient/patient.api.service';

@Component({
  selector: 'app-search-patient',
  templateUrl: './search-patient.component.html',
  styleUrls: ['./search-patient.component.scss'],
})
export class SearchPatientComponent implements OnInit {
  public form: FormGroup;

  @Output('searchedPatient') searchedPatient: EventEmitter<
    PatientResDto
  > = new EventEmitter();

  constructor(
    private fb: FormBuilder,
    private patientAPIService: PatientAPIService,
    private snackBar: MatSnackBar,
    private readonly patientIdPassingService: PatientIdPassingService
  ) {}

  ngOnInit() {
    this.form = this.fb.group({ tajNumber: ['', Validators.required] });
  }

  async onSubmit() {
    try {
      const response = await this.patientAPIService
        .getPatientByTajNumber(this.form.get('tajNumber').value)
        .toPromise();

      if (response.ok) {
        this.patientIdPassingService.setPatientId = response.body.id;
        this.searchedPatient.emit(response.body);
      }
    } catch (e) {
      this.snackBar.open(
        `Nem található páciens '${
          this.form.get('tajNumber').value
        }' TAJ szám alapján`,
        'Rendben'
      );
    }
  }
}
