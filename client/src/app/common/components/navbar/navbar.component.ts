import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  isAdmin$: Observable<boolean>;
  isReceptionist$: Observable<boolean>;
  isUser$: Observable<boolean>;
  isAuthenticated$: Observable<boolean>;
  userName$: Observable<string>;

  constructor(public authService: AuthService) {}

  ngOnInit(): void {
    this.isAdmin$ = this.authService.isAdmin$;
    this.isReceptionist$ = this.authService.isReceptionist$;
    this.isUser$ = this.authService.isUser$;
    this.isAuthenticated$ = this.authService.isAuthenticated$;
    this.userName$ = this.authService.userName$;
  }
}
