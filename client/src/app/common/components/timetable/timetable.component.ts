import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { NgZeeTimeTableData, NgZeeTimeTableOptions } from 'ng-zee-timetable';
import { BehaviorSubject } from 'rxjs';
import { getSafe } from 'src/app/utils/get-safe';
import { ListTimeTableResDto } from '../../api/timetable/timetable.api.interface';
import * as moment from 'moment';

export interface IntervalChanged {
  startDate: string;
  endDate: string;
}

@Component({
  selector: 'app-timetable',
  templateUrl: './timetable.component.html',
  styleUrls: ['./timetable.component.scss'],
})
export class TimetableComponent implements OnChanges {
  @Input() inputTimetableData: ListTimeTableResDto[] = [];
  @Input() startDate: string;
  @Input() endDate: string;
  @Input() disableIntervalChange = false;
  @Output() intervalChange: EventEmitter<IntervalChanged> = new EventEmitter();
  mappedTimetableData$: BehaviorSubject<
    NgZeeTimeTableData
  > = new BehaviorSubject(undefined);

  options: NgZeeTimeTableOptions = {
    element: {
      background: '#f97c7c',
      titleColor: 'white',
      subTitleColor: '#862424',
    },
    rowLabel: {
      background: '#d83d3d',
      labelColor: 'white',
    },
  };

  days: string[] = [
    'Vasárnap',
    'Hétfő',
    'Kedd',
    'Szerda',
    'Csütörtök',
    'Péntek',
    'Szombat',
  ];

  ngOnChanges(change: SimpleChanges) {
    if (
      change &&
      getSafe(
        () =>
          change.inputTimetableData &&
          0 < change.inputTimetableData.currentValue.length
      )
    ) {
      const inputTimetableData = change.inputTimetableData.currentValue;
      this.mappedTimetableData$.next(
        this.mapInputDataToTimeTable(inputTimetableData)
      );
    }
  }

  mapInputDataToTimeTable(data: ListTimeTableResDto[]) {
    let mappedData = {
      Hétfő: {},
      Kedd: {},
      Szerda: {},
      Csütörtök: {},
      Péntek: {},
      Szombat: {},
      Vasárnap: {},
    };
    data.forEach((session: ListTimeTableResDto) => {
      if (
        new Date(this.startDate).getTime() <
          new Date(session.startOfSession).getTime() &&
        new Date(session.startOfSession).getTime() <
          new Date(this.endDate).getTime()
      ) {
        const dayIndex = moment(session.startOfSession).day();
        const startOfSessionHoursAndMinutes = this.formatHoursAndMinutes(
          session.startOfSession
        );
        const endOfSessionHoursAndMinutes = this.formatHoursAndMinutes(
          session.endOfSession
        );

        mappedData[this.days[dayIndex]] = {
          ...mappedData[this.days[dayIndex]],
          [startOfSessionHoursAndMinutes]: {
            title: `${session.examinationName}`,
            subTitle: `${session.patientName}`,
            endTime: endOfSessionHoursAndMinutes,
          },
        };
      }
    });
    return mappedData;
  }

  formatHoursAndMinutes(time: string) {
    const minutes = moment(time).minutes();
    const hours = moment(time).hours();
    const roundToNearest5 = (x) => Math.round(x / 5) * 5;
    const roundedminutes = roundToNearest5(minutes);
    return `${hours < 10 ? `0${hours}` : hours}:${
      roundedminutes < 10 ? `0${roundedminutes}` : roundedminutes
    }`;
  }

  changeInterval(intervalChange: 'previous' | 'next') {
    this.mappedTimetableData$.next({});
    if ('previous' === intervalChange) {
      this.intervalChange.emit({
        startDate: moment(this.startDate).subtract(7, 'days').toISOString(),
        endDate: moment(this.endDate).subtract(7, 'days').toISOString(),
      });
    } else if ('next' === intervalChange) {
      this.intervalChange.emit({
        startDate: moment(this.startDate).add(7, 'days').toISOString(),
        endDate: moment(this.endDate).add(7, 'days').toISOString(),
      });
    }
  }
}
