import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AdminDashboardComponent } from 'src/app/pages/admin/admin-dashboard/admin-dashboard.component';
import { ClinicClassCreateComponent } from 'src/app/pages/admin/clinic-class-create/clinic-class-create.component';
import { ClinicClassUpdateComponent } from 'src/app/pages/admin/clinic-class-update/clinic-class-update.component';
import { ExaminationCreateComponent } from 'src/app/pages/admin/examination-create/examination-create.component';
import { ExaminationUpdateComponent } from 'src/app/pages/admin/examination-update/examination-update.component';
import { UserCreateComponent } from 'src/app/pages/admin/user-create/user-create.component';
import { UserUpdateComponent } from 'src/app/pages/admin/user-update/user-update.component';
import { LoginComponent } from 'src/app/pages/login/login.component';
import { PatientAppointmentsListUpdateComponent } from 'src/app/pages/receptionist/patient-list-update/patient-list-update.component';
import { PatientDocumentReceptionistDialogComponent } from 'src/app/pages/receptionist/receptionist-dashboard/patient-document-list-dialog/patient-document-list-dialog.component';
import { ReceptionistDashboardComponent } from 'src/app/pages/receptionist/receptionist-dashboard/receptionist-dashboard.component';
import { AppointmentsComponent } from 'src/app/pages/user/user-dashboard/appointments/appointments.component';
import { DiagnosisListUpdateComponent } from 'src/app/pages/user/user-dashboard/diagnosis-list-update/diagnosis-list-update.component';
import { UserDashboardComponent } from 'src/app/pages/user/user-dashboard/user-dashboard.component';
import { AuthService, IParsedJwtToken } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const token: IParsedJwtToken = this.authService.getParsedToken();
    const component = next.component;
    if (!token) {
      if (component !== LoginComponent) {
        this.router.navigate(['login']);
        return false;
      } else {
        return true;
      }
    } else if (this.isAdmin(token)) {
      if (this.isAdminComponent(component)) {
        return true;
      } else {
        this.router.navigate(['admin', 'dashboard']);
        return false;
      }
    } else if (this.isReceptionist(token)) {
      if (this.isReceptionistComponent(component)) {
        return true;
      } else {
        this.router.navigate(['receptionist', 'dashboard']);
        return false;
      }
    } else if (this.isUser(token)) {
      if (this.isUserComponent(component)) {
        return true;
      } else {
        this.router.navigate(['user', 'dashboard']);
        return false;
      }
    } else {
      return false;
    }
  }

  isAdminComponent(component) {
    return (
      AdminDashboardComponent === component ||
      UserCreateComponent === component ||
      UserUpdateComponent === component ||
      ClinicClassCreateComponent === component ||
      ClinicClassUpdateComponent === component ||
      ExaminationCreateComponent === component ||
      ExaminationUpdateComponent === component ||
      LoginComponent === component
    );
  }

  isReceptionistComponent(component) {
    return (
      ReceptionistDashboardComponent === component ||
      PatientAppointmentsListUpdateComponent === component ||
      PatientDocumentReceptionistDialogComponent === component
    );
  }

  isUserComponent(component) {
    return (
      UserDashboardComponent === component ||
      DiagnosisListUpdateComponent === component ||
      AppointmentsComponent === component
    );
  }

  isAdmin(token: IParsedJwtToken) {
    return 'ADMIN' === token.role;
  }

  isReceptionist(token: IParsedJwtToken) {
    return 'RECEPTIONIST' === token.role;
  }

  isUser(token: IParsedJwtToken) {
    return 'USER' === token.role;
  }
}
