export interface CreatePatientDto {
  name: string;
  gender: string;
  address: string;
  idCardNumber: string;
  tajNumber: string;
}

export interface UpdatePatient {
  name?: string;
  address?: string;
  idCardNumber?: string;
  tajNumber?: string;
}

export interface PatientResDto extends CreatePatientDto {
  createdAt: string;
  updatedAt: string;
  id: string;
}

export interface PatientDetails {
  patient: {
    address: string;
    createdAt: string;
    gender: 'male' | 'female';
    id: string;
    idCardNumber: any;
    name: string;
    tajNumber: any;
    updatedAt: string;
  };
  referral: PatientReferralDetails[];
}

export interface PatientReferralDetails {
  referral: {
    createdAt: string;
    description: string;
    examinationId: string;
    id: string;
    updatedAt: string;
  };
  examinationName: string;
  timetable: PatientReferralTimetable[];
}

export interface PatientReferralTimetable {
  createdAt: string;
  doctorId: string;
  endOfSession: string;
  id: string;
  startOfSession: string;
  updatedAt: string;
}
