import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import {
  CreatePatientDto,
  PatientDetails,
  PatientResDto,
  UpdatePatient,
} from './patient.api.interface';

@Injectable({ providedIn: 'root' })
export class PatientAPIService {
  constructor(private http: HttpService) {}

  createPatient(body: CreatePatientDto): Observable<any> {
    return this.http.post('/api/patient/register', body);
  }

  getPatientByTajNumber(
    tajNumber: string
  ): Observable<HttpResponse<PatientResDto>> {
    return this.http.get(`/api/patient/taj/${tajNumber}`);
  }

  updatePatient(
    patientId: string,
    body: UpdatePatient
  ): Observable<HttpResponse<any>> {
    return this.http.put(`api/patient/${patientId}/update-patient`, body);
  }

  deletePatient(patientId: string) {
    return this.http.delete(`/api/patient/${patientId}/delete-patient`);
  }

  listPatientandRelatedObjectsByDoctor(): Observable<
    HttpResponse<PatientDetails[]>
  > {
    return this.http.get(
      `api/patient/list-patient-and-related-stuffs-bydoctor`
    );
  }
}
