export interface CreateExaminationDto {
  name: string;
  clinicClassId: string;
  price?: number;
  description?: string;
}

export interface ListExaminationResponseDto {
  id: string;
  name: string;
  price: number;
  description: string;
}

export interface UpdateExaminationReqDto {
  name?: string;
  price?: number;
  description?: string;
}

export interface ExaminationDto {
  id: string;
  name: string;
  price: number;
  description: string;
}
