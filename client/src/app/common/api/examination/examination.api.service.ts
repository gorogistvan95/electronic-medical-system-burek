import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { RoleType } from '../../enums/role-types.enum';
import {
  CreateExaminationDto,
  ListExaminationResponseDto,
  UpdateExaminationReqDto,
} from './examination.api.interface';

@Injectable({
  providedIn: 'root',
})
export class ExaminationAPIService {
  constructor(private http: HttpService) {}

  createExamination(body: CreateExaminationDto): Observable<HttpResponse<any>> {
    return this.http.post('/api/examination/create-examination', body);
  }

  listExamination(
    clinicId: string
  ): Observable<HttpResponse<ListExaminationResponseDto[]>> {
    return this.http.get(`/api/examination/${clinicId}/list-examination`);
  }

  updateExamination(
    examinationId: string,
    body: UpdateExaminationReqDto
  ): Observable<HttpResponse<any>> {
    return this.http.put(
      `api/examination/${examinationId}/update-examination`,
      body
    );
  }

  deleteExamination(
    examinationId: string
  ): Observable<HttpResponse<ListExaminationResponseDto[]>> {
    return this.http.delete(
      `/api/examination/${examinationId}/delete-examination`
    );
  }
}
