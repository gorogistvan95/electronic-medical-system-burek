import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

import {
  CreateTimeTableReqDto,
  CreateTimeTableResDto,
  ListTimeTableResDto,
  ListTimeTableByPatientResDto,
} from './timetable.api.interface';

@Injectable({
  providedIn: 'root',
})
export class TimetableAPIService {
  constructor(private http: HttpService) {}
  createAppointment(
    body: CreateTimeTableReqDto
  ): Observable<HttpResponse<CreateTimeTableResDto>> {
    return this.http.post('/api/timetable/create-timetable', body);
  }

  getAppointmentsByDoctorId(
    doctorId: string,
    startDate: string,
    endDate: string
  ): Observable<HttpResponse<ListTimeTableResDto[]>> {
    return this.http.get(
      `/api/timetable/list-appointments-by-doctor-id/${doctorId}?startDate=${startDate}&endDate=${endDate}`
    );
  }
  getAppointmentsByPatientId(
    patientId: string
  ): Observable<HttpResponse<ListTimeTableByPatientResDto[]>> {
    return this.http.get(
      `/api/timetable/list-appointments-by-patient-id/${patientId}`
    );
  }

  deleteAppointment(
    appointmentId: string
  ): Observable<HttpResponse<ListTimeTableResDto[]>> {
    return this.http.delete(
      `/api/timetable/${appointmentId}/delete-appointment/`
    );
  }
}
