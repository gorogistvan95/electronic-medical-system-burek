export interface CreateTimeTableReqDto {
  startOfSession: Date;
  endOfSession: Date;
  authId: string;
  referralId: string;
}

export class CreateTimeTableResDto {
  id: string;
  startOfSession: Date;
  endOfSession: Date;
  doctorId: string;
  referralId: string;
}

export interface ListTimeTableResDto {
  id: string;
  examinationName?: string;
  patientName?: string;
  startOfSession: string;
  endOfSession: string;
  doctorId: string;
  referralId: string;
}

export interface ListTimeTableByPatientResDto {
  id: string;
  startOfSession: string;
  endOfSession: string;
  doctorId: string;
  referralId: string;
  examinationName: string;
  patientName: string;
  clinicClassName: string;
}
