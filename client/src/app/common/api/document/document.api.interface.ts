export interface CreateDocumentReqDto {
  name: string;
  userId: string;
  description?: string;
  isAvailableAlways?: string;
}

export interface CreateDocumentResDto {
  name: string;
  id: string;
  documentPath: string;
  isAvailableAlways: boolean;
  uploader: string;
  patient: string;
}

export class ListDocumentResDto {
  id: string;
  documentPath: string;
  description: string;
  isAvailableAlways: boolean;
  uploader: string;
  patient: string;
  createdAt: Date;
  updatedAt: Date;
  mimeType: string;
  size: string;
  name: string;
}
