import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import {
  CreateDocumentResDto,
  ListDocumentResDto,
} from './document.api.interface';

@Injectable({
  providedIn: 'root',
})
export class DocumentAPIService {
  constructor(private http: HttpService) {}

  upload(
    body: FormData,
    patientId: string
  ): Observable<HttpResponse<CreateDocumentResDto>> {
    return this.http.post(`/api/document/document-upload/${patientId}`, body);
  }

  deleteDocument(documentId: string) {
    return this.http.delete(`/api/document/${documentId}/delete-document`);
  }

  listPatientDocumentsByDoctor(
    patientId: string
  ): Observable<HttpResponse<ListDocumentResDto[]>> {
    return this.http.get(`/api/document/${patientId}/list-document-by-doctor`);
  }

  documentDownload(documentId: string) {
    return this.http.get(this.documentDownloadUrl(documentId));
  }

  documentDownloadUrl(documentId: string) {
    return `/api/document/${documentId}/document-download`;
  }
}
