import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpService } from 'src/app/services/http.service';
import { map } from 'rxjs/operators';
import { map as _map } from 'lodash';

import {
  CreateDiagnosisReqDto,
  ListBnoDiagnosisResponseDto,
  ListDiagnosisResponseDto,
  ListMappedBnoDiagnosisResponseDto,
  UpdateDiagnosisReqDto,
} from './diagnosis.api.interface';

@Injectable({
  providedIn: 'root',
})
export class DiagnosisApiService {
  constructor(private http: HttpService) {}

  createDiagnosis(body: CreateDiagnosisReqDto): Observable<HttpResponse<any>> {
    return this.http.post('/api/diagnosis/create-diagnosis', body);
  }

  listDiagnosises(
    patientId: string
  ): Observable<HttpResponse<ListDiagnosisResponseDto[]>> {
    return this.http.get(`/api/diagnosis/${patientId}/list-diagnosis`);
  }

  listBnoDiagnosises(): Observable<
    HttpResponse<ListBnoDiagnosisResponseDto[]>
  > {
    return this.http.get(`/api/diagnosis/list-diagnosis-bno`);
  }

  updateDiagnosis(
    diagnosisId: string,
    body: UpdateDiagnosisReqDto
  ): Observable<HttpResponse<any>> {
    return this.http.put(`api/diagnosis/${diagnosisId}/update-diagnosis`, body);
  }

  deleteDiagnosis(
    diagnosisId: string
  ): Observable<HttpResponse<ListDiagnosisResponseDto[]>> {
    return this.http.delete(`/api/diagnosis/${diagnosisId}/delete-diagnosis`);
  }

  listMappedBnoDiagnosises(): Observable<ListMappedBnoDiagnosisResponseDto[]> {
    return this.listBnoDiagnosises().pipe(
      map((r) => _map(r.body, (item) => ({ code: item.KOD10, name: item.NEV })))
    );
  }
}
