export interface ListDiagnosisResponseDto {
  id: string;
  code: string;
  name: string;
  createdAt: Date;
  updatedAt: Date;
  description: string;
  treatment: string;
}

export interface CreateDiagnosisReqDto {
  code: string;
  patientId: string;
  name: string;
  description?: string;
  treatment?: string;
}

export interface UpdateDiagnosisReqDto {
  code?: string;
  name?: string;
  description?: string;
  treatment?: string;
}
export interface ListBnoDiagnosisResponseDto {
  KOD10: string;
  NEV: string;
  NEM: string;
  KOR_A: string;
  KOR_F: string;
  ERV_KEZD: string;
  ERV_VEGE: string;
}

export interface ListMappedBnoDiagnosisResponseDto {
  code: string;
  name: string;
}
