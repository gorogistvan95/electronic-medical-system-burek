export class CreateClinicClassDto {
  name: string;
  onlyOneUser: boolean;
}

export class UpdateClinicClassDto {
  name?: string;
  onlyOneUser?: boolean;
}
export class UpdateExaminationReqDto {
  readonly name?: string;
  readonly price?: number;
  readonly description?: string;
}

export class ListClinicClassResponseDto {
  id: string;
  name: string;
  onlyOneUser: boolean;
}

export interface ClinicClassMappedToNameAndId {
  id: string;
  name: string;
}
