import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { map as _map } from 'lodash';
import { HttpService } from 'src/app/services/http.service';
import {
  CreateClinicClassDto,
  ListClinicClassResponseDto,
  ClinicClassMappedToNameAndId,
  UpdateClinicClassDto,
  UpdateExaminationReqDto,
} from './clinic-class.api.interface';

@Injectable({
  providedIn: 'root',
})
export class ClinicClassAPIService {
  constructor(private http: HttpService) {}

  createClinicClass(body: CreateClinicClassDto): Observable<HttpResponse<any>> {
    return this.http.post('/api/clinic-class/createclinicclass', body);
  }

  updateClinicClass(
    body: UpdateClinicClassDto,
    id: string
  ): Observable<HttpResponse<any>> {
    return this.http.put(`/api/clinic-class/${id}/update-clinic-class/`, body);
  }

  listClinicClasses(): Observable<HttpResponse<ListClinicClassResponseDto[]>> {
    return this.http.get('/api/clinic-class/listclinicclass');
  }
  deleteClinicClass(
    clinicClassId: string
  ): Observable<HttpResponse<ListClinicClassResponseDto[]>> {
    return this.http.delete(
      `/api/clinic-class/${clinicClassId}/delete-clinic-class`
    );
  }

  updateExamination(
    body: UpdateExaminationReqDto,
    clinicClassId: string
  ): Observable<HttpResponse<any>> {
    return this.http.put(
      `/api/examination/${clinicClassId}/update-clinic-class/`,
      body
    );
  }
  listClinicClassMappedToIdAndName(): Observable<
    ClinicClassMappedToNameAndId[]
  > {
    return this.listClinicClasses().pipe(
      map((r) => _map(r.body, (item) => ({ id: item.id, name: item.name })))
    );
  }
}
