export interface CreateReferralReqDto {
  description: string;
  patientId: string;
  examinationId: string;
}

export interface CreateReferralResponseDto {
  id: string;
  examinationId: string;
  patientId: string;
  description: string;
}
