import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';

import {
  CreateReferralReqDto,
  CreateReferralResponseDto,
} from './referral.api.interface';

@Injectable({
  providedIn: 'root',
})
export class ReferralAPIService {
  constructor(private http: HttpService) {}

  createReferral(
    body: CreateReferralReqDto
  ): Observable<HttpResponse<CreateReferralResponseDto>> {
    return this.http.post('/api/referral/create-referral', body);
  }
}
