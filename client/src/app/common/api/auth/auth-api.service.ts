import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import {
  AuthDto,
  CreateUserDto,
  LoginPayloadDto,
  UpdateUserReqDto,
} from './auth-api.interface';

@Injectable({ providedIn: 'root' })
export class AuthAPIService {
  constructor(private http: HttpService) {}

  login(
    username: string,
    password: string
  ): Observable<HttpResponse<LoginPayloadDto>> {
    console.log(username + ' ' + password);
    return this.http.post('/api/auth/login', { username, password });
  }

  register(createUser: CreateUserDto): Observable<HttpResponse<any>> {
    return this.http.post('/api/auth/register', {
      ...createUser,
      passwordConfirm: undefined,
    });
  }

  listUsers(): Observable<HttpResponse<AuthDto[]>> {
    return this.http.get('/api/auth/list-users');
  }

  deleteUser(userId: string): Observable<HttpResponse<AuthDto[]>> {
    return this.http.delete(`/api/auth/${userId}/delete-user`);
  }

  updateUser(
    body: UpdateUserReqDto,
    userId: string
  ): Observable<HttpResponse<any>> {
    return this.http.put(`api/auth/${userId}/update-user`, body);
  }

  listDoctorsByClinicId(clinicId: string): Observable<HttpResponse<AuthDto[]>> {
    return this.http.get(`/api/auth/${clinicId}/list-doctors`);
  }
}
