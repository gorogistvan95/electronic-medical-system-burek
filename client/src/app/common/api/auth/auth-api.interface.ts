import { RoleType } from '../../enums/role-types.enum';

export interface LoginPayloadDto {
  auth: AuthDto;
  token: TokenPayloadDto;
}

export interface TokenPayloadDto {
  expiresIn: number;
  accessToken: string;
}

export interface AuthDto {
  id: string;
  role: RoleType;
  createdAt: Date;
  updatedAt: Date;
  username: string;
  name: string;
  password: string;
  lastLogin: Date;
  clinicClass: ClinicClassDto;
}

export interface ClinicClassDto {
  name: string;
  onlyOneUser?: boolean;
}

export class UpdateUserReqDto {
  readonly username?: string;
  readonly name?: string;
  readonly password?: string;
}

export interface CreateUserDto {
  username: string;
  name?: string;
  password: string;
  isDiagnosis: boolean;
  role: string;
  clinicClassId: string;
}
