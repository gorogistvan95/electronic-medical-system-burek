#Előfeltételek:
0.Bizonyosodjunk meg, hogy a szakdolgozat dokumentumban szerpelő  technológiák(npm 6.14.4 ,angular 11.2.3 ,Nodejs 12.18.0,Nestjs 7.5.6,MySql  8.0.23) a megfelelő verzió számmal telepítve vannak-e. 
1. Abban az esetben,ha a új adatbázist szeretnénk használni akkor először létre kell hoznunk egy admin felhasználót. A következő parancsal szúrjuk be az adatbázisba a felhasználót:

INSERT INTO `auth` (`id`, `created_at`, `updated_at`, `name`, `username`, `password`, `role`, `is_diagnosis`, `clinic_class_id`, `last_login`) VALUES ('52d9f2e5-1fc7-472b-b42c-34cb0288d06c', '2020-12-12 17:30:24.000000', '2020-12-12 17:30:24.000000', NULL, 'admin1', '$2b$10$BEls/VDjXI3gtDnQ/CwAc.HmMjQaTJg9Sei6URfoFcZYc00aOINRO', 'ADMIN', '0', NULL, '2020-12-12 17:30:24.000');

Bejelentkezés ezzel a felhasználóval:  felhasználónév: admin1  jelszó:123456

A felhasználó beszúrása után ajánlott bejelentkezni vele és megváltoztatni a jelszavat vagy új admint létrehozni és törölni a jelenlegit.

#A rendszer elindítása:
1. client mappába lépve futtatjuk a következő parancsot: npm install
2. server mappába lépve futtatjuk  a következő parancsot: npm install
3. Futtassunk egy MySql adatbázist és annak a kapcsolódáshoz szükséges adatait adjuk a :MYSQL envioroment variables alatti változókba.
4. server mappába lépve futtatjuk a következő parancsot:  npm run-script build
4. server mappába lépve futtatjuk a következő parancsot:  npm start
5. a client mappába lépve futtatjuk a következő paramcsot: npm run start




